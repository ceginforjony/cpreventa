program CPreVentas;

uses
  System.StartUpCopy,
  FMX.Forms,
  Albaran in 'controlador\Albaran.pas',
  Articulo in 'controlador\Articulo.pas',
  Caja in 'controlador\Caja.pas',
  Cliente in 'controlador\Cliente.pas',
  Comunicacion in 'controlador\Comunicacion.pas',
  Controlador in 'controlador\Controlador.pas',
  Factura in 'controlador\Factura.pas',
  Pedido in 'controlador\Pedido.pas',
  Usuario in 'controlador\Usuario.pas',
  Modelo in 'modelo\Modelo.pas',
  ModeloAlbaran in 'modelo\ModeloAlbaran.pas',
  ModeloArticulo in 'modelo\ModeloArticulo.pas',
  ModeloCaja in 'modelo\ModeloCaja.pas',
  ModeloCliente in 'modelo\ModeloCliente.pas',
  ModeloComunicacion in 'modelo\ModeloComunicacion.pas',
  ModeloFactura in 'modelo\ModeloFactura.pas',
  ModeloPedido in 'modelo\ModeloPedido.pas',
  ModeloUsuario in 'modelo\ModeloUsuario.pas',
  vista_articulo in 'vista\Articulo\vista_articulo.pas' {Formarticulo},
  vista_articulos in 'vista\Articulo\vista_articulos.pas' {FormArticulos},
  vista_buscarArticulo in 'vista\Articulo\vista_buscarArticulo.pas' {Formbusquedaart},
  vista_BuscarLote in 'vista\Articulo\vista_BuscarLote.pas' {Formbusquedalote},
  vista_listarArticulos in 'vista\Articulo\vista_listarArticulos.pas' {FormListarArt},
  vista_lotes in 'vista\Articulo\vista_lotes.pas' {FormLotes},
  vista_albaranes in 'vista\Cliente\Albaran\vista_albaranes.pas' {FormAlbaranes},
  vista_busquedaalb in 'vista\Cliente\Albaran\vista_busquedaalb.pas' {Formbusquedaalb},
  vista_busquedaalbclt in 'vista\Cliente\Albaran\vista_busquedaalbclt.pas' {FormBusquedaAlbClt},
  vista_busquedafac in 'vista\Cliente\Factura\vista_busquedafac.pas' {Formbusquedafac},
  vista_busquedafacclt in 'vista\Cliente\Factura\vista_busquedafacclt.pas' {Formbusquedafacclt},
  vista_Facturas in 'vista\Cliente\Factura\vista_Facturas.pas' {FormFacturas},
  vista_busquedalineaventa in 'vista\Cliente\Pedidos\vista_busquedalineaventa.pas' {FormBusquedaLineaVenta},
  vista_busquedaped in 'vista\Cliente\Pedidos\vista_busquedaped.pas' {Formbusquedaped},
  vista_busquedapedclt in 'vista\Cliente\Pedidos\vista_busquedapedclt.pas' {FormBusquedaPedClt},
  vista_historicoped in 'vista\Cliente\Pedidos\vista_historicoped.pas' {Formhistoricoped},
  vista_lineasPed in 'vista\Cliente\Pedidos\vista_lineasPed.pas' {FormLineasPed},
  vista_lineasPedNoEdit in 'vista\Cliente\Pedidos\vista_lineasPedNoEdit.pas' {FormLineasPedNoEdit},
  vista_listarArtPedHis in 'vista\Cliente\Pedidos\vista_listarArtPedHis.pas' {FormListarPedHistorial},
  vista_NuevaLineaPed in 'vista\Cliente\Pedidos\vista_NuevaLineaPed.pas' {FormNuevaLineaPed},
  vista_NuevoPedido in 'vista\Cliente\Pedidos\vista_NuevoPedido.pas' {formnuevoped},
  vista_pedidos in 'vista\Cliente\Pedidos\vista_pedidos.pas' {FormPedidos},
  vista_rectificarped in 'vista\Cliente\Pedidos\vista_rectificarped.pas' {FormRecPed},
  vista_updateLineaPed in 'vista\Cliente\Pedidos\vista_updateLineaPed.pas' {Formupdatelineaped},
  vista_verped in 'vista\Cliente\Pedidos\vista_verped.pas' {Formverped},
  vista_albaranesCliente in 'vista\Cliente\vista_albaranesCliente.pas' {Formalbaranesclt},
  Vista_BuscarCliente in 'vista\Cliente\Vista_BuscarCliente.pas' {Formbusquedaclt},
  vista_clientes in 'vista\Cliente\vista_clientes.pas' {Formclientes},
  vista_facturascliente in 'vista\Cliente\vista_facturascliente.pas' {Formfacturasclt},
  vista_listaclientes in 'vista\Cliente\vista_listaclientes.pas' {Formlistaclientes},
  vista_PedidosCliente in 'vista\Cliente\vista_PedidosCliente.pas' {Formpedidosclt},
  vista_ruta in 'vista\Cliente\vista_ruta.pas' {Formruta},
  vista_cliente in 'vista\Cliente\vista_cliente.pas' {Formcliente},
  vista_busquedacobro in 'vista\cobros\vista_busquedacobro.pas' {Formbusquedacobro},
  vista_busquedacobroclt in 'vista\cobros\vista_busquedacobroclt.pas' {Formbusquedacobroclt},
  vista_caja in 'vista\cobros\vista_caja.pas' {FormCaja},
  vista_cobroalb in 'vista\cobros\vista_cobroalb.pas' {Formcobraralb},
  vista_cobrofac in 'vista\cobros\vista_cobrofac.pas' {FormCobroFac},
  vista_cobrosclt in 'vista\cobros\vista_cobrosclt.pas' {Formcobrosclt},
  vista_nuevocobro in 'vista\cobros\vista_nuevocobro.pas' {Formnuevocobro},
  vista_nuevocobroclt in 'vista\cobros\vista_nuevocobroclt.pas' {Formnuevocobroclt},
  vista_ajustes in 'vista\utilidades\vista_ajustes.pas' {FormAjustes},
  vista_comunicacion in 'vista\utilidades\vista_comunicacion.pas' {FormComunicacion},
  vista_parametros in 'vista\utilidades\vista_parametros.pas' {FormParametros},
  carga_inicial in 'vista\carga_inicial.pas' {Form1},
  mensaje in 'vista\mensaje.pas' {Form17},
  vista_login in 'vista\vista_login.pas' {Form3},
  vista_menu in 'vista\vista_menu.pas' {Form2},
  Android.JNI.Toast in 'librerias\Android.JNI.Toast.pas',
  Androidapi.JNI.BluetoothAdapter in 'librerias\Androidapi.JNI.BluetoothAdapter.pas',
  funciones in 'funciones\funciones.pas ';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TFormarticulo, Formarticulo);
  Application.CreateForm(TFormArticulos, FormArticulos);
  Application.CreateForm(TFormbusquedaart, Formbusquedaart);
  Application.CreateForm(TFormbusquedalote, Formbusquedalote);
  Application.CreateForm(TFormListarArt, FormListarArt);
  Application.CreateForm(TFormLotes, FormLotes);
  Application.CreateForm(TFormAlbaranes, FormAlbaranes);
  Application.CreateForm(TFormbusquedaalb, Formbusquedaalb);
  Application.CreateForm(TFormBusquedaAlbClt, FormBusquedaAlbClt);
  Application.CreateForm(TFormbusquedafac, Formbusquedafac);
  Application.CreateForm(TFormbusquedafacclt, Formbusquedafacclt);
  Application.CreateForm(TFormFacturas, FormFacturas);
  Application.CreateForm(TFormBusquedaLineaVenta, FormBusquedaLineaVenta);
  Application.CreateForm(TFormbusquedaped, Formbusquedaped);
  Application.CreateForm(TFormBusquedaPedClt, FormBusquedaPedClt);
  Application.CreateForm(TFormhistoricoped, Formhistoricoped);
  Application.CreateForm(TFormLineasPed, FormLineasPed);
  Application.CreateForm(TFormLineasPedNoEdit, FormLineasPedNoEdit);
  Application.CreateForm(TFormListarPedHistorial, FormListarPedHistorial);
  Application.CreateForm(TFormNuevaLineaPed, FormNuevaLineaPed);
  Application.CreateForm(Tformnuevoped, formnuevoped);
  Application.CreateForm(TFormPedidos, FormPedidos);
  Application.CreateForm(TFormRecPed, FormRecPed);
  Application.CreateForm(TFormupdatelineaped, Formupdatelineaped);
  Application.CreateForm(TFormverped, Formverped);
  Application.CreateForm(TFormalbaranesclt, Formalbaranesclt);
  Application.CreateForm(TFormbusquedaclt, Formbusquedaclt);
  Application.CreateForm(TFormcliente, Formcliente);
  Application.CreateForm(TFormclientes, Formclientes);
  Application.CreateForm(TFormfacturasclt, Formfacturasclt);
  Application.CreateForm(TFormlistaclientes, Formlistaclientes);
  Application.CreateForm(TFormpedidosclt, Formpedidosclt);
  Application.CreateForm(TFormruta, Formruta);
  Application.CreateForm(TFormcliente, Formcliente);
  Application.CreateForm(TFormbusquedacobro, Formbusquedacobro);
  Application.CreateForm(TFormbusquedacobroclt, Formbusquedacobroclt);
  Application.CreateForm(TFormCaja, FormCaja);
  Application.CreateForm(TFormcobraralb, Formcobraralb);
  Application.CreateForm(TFormCobroFac, FormCobroFac);
  Application.CreateForm(TFormcobrosclt, Formcobrosclt);
  Application.CreateForm(TFormnuevocobro, Formnuevocobro);
  Application.CreateForm(TFormnuevocobroclt, Formnuevocobroclt);
  Application.CreateForm(TFormAjustes, FormAjustes);
  Application.CreateForm(TFormComunicacion, FormComunicacion);
  Application.CreateForm(TFormParametros, FormParametros);
  Application.CreateForm(TForm17, Form17);
  Application.CreateForm(TForm3, Form3);
  Application.Run;
end.
