unit Usuario;

interface

{$ZEROBASEDSTRINGS ON}

uses System.Classes, FMX.Forms, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,modelousuario,controlador,vista_menu,Data.FMTBcd;


type
  TUsuario = class(TControlador) // Tambi�n puede ser solo class si derivas directamente de TObject

  public
    procedure cargar_menu(vista: tform2);
    function validacion(user,pass:string):boolean;
    function nombreruta(codigo:integer):string;
    function obtenerajustes():tdictionary<string,string>;
    procedure actualizarajustes(ajustes:tdictionary<string,string>);

  end;


implementation

procedure TUsuario.cargar_menu(vista: tform2);
var modeloUsuario: TModeloUsuario;
    usuario: TSQLQuery;
    depen: string;
    empresa:string;
begin
  modeloUsuario:= TModeloUsuario.Create;




  depen :='Dep'+ modelousuario.ObtenerDependiente();


  usuario:= modelousuario.ObtenerConfiguracion();
  usuario.First;
  empresa:=usuario.Fieldbyname('empresa').AsString;
  vista.LabelDependiente.Text:=depen;
  vista.Labelempresa1.Text:=empresa;
  vista.Labelempresa2.Text:=empresa;
  vista.Labelempresa3.Text:=empresa;
    //vista.edit1.text:=depen;
  modelousuario.liberarsql;
    //modeloUsuario.Free;
end;
  function tusuario.validacion(user,pass:string):boolean;
  var modelo:tmodelousuario;
  begin
    modelo:= TModeloUsuario.Create;
    result:=modelo.validacion(user,pass);
    modelo.liberarsql;
  end;
  function tusuario.nombreruta(codigo:integer):string;
  var modelo:tmodelousuario;
  begin
   modelo:= TModeloUsuario.Create;
   result:=modelo.nombreruta(codigo);
   modelo.liberarsql;
  end;
  //SQL:=modeloArticulo.ListarArticulos(filtro,columnas);

  function tusuario.ObtenerAjustes():tdictionary<string,string>;
  var modelo:tmodelousuario;
  query:TSQLQuery;
  ajustes:tdictionary<string,string>;
  begin
   modelo:= TModeloUsuario.Create;
   ajustes:=tdictionary<string,string>.create;
   query:=modelo.obtenerajustes();
   query.First;

   ajustes.Add('escala',query.FieldByName('escala').asstring);
   result:=ajustes;
   modelo.liberarsql;
  end;
   procedure tusuario.actualizarajustes(ajustes:tdictionary<string,string>);
  var modelo:tmodelousuario;


  begin
   modelo:= TModeloUsuario.Create;
   modelo.actualizarajustes(ajustes);
   modelo.liberarsql;
  end;



end.