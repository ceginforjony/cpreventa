unit Albaran;

interface
{$ZEROBASEDSTRINGS ON}
  uses controlador,System.Classes, FMX.Forms, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,modeloCliente,modeloAlbaran,FMX.Dialogs, System.SysUtils,modeloUsuario,funciones,system.Math,Androidapi.JNI.BluetoothAdapter,Androidapi.JNI.JavaTypes,
  Androidapi.JNIBridge,Android.JNI.Toast,Androidapi.Helpers,modelocomunicacion,
  Data.FMTBcd;

type TIntArray = array of Integer;

type
  TAlbaran = class(TControlador) // Tambi�n puede ser solo class si derivas directamente de TObject

  public

    procedure ListarAlbaranesCliente(codigoclt:string;var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
    procedure ListarAlbaranes(var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
    procedure MostrarcobrarAlbaran(idalb:integer;padre:tform);
    function  obtenerConfiguracionAlb(): Tdictionary<string,variant>;
    function  obtenerDatosAlb(idalb:integer):Tdictionary <string,string>;
    procedure realizarcobro(idalb:integer;datoscobro:tdictionary<string,string>);
    function  albarancobrado(idalb:integer):boolean;



  end;

implementation
uses {vista_lineasalb, vista_nuevoAlbaran,vista_nuevaLineaAlb, vista_asignarlotealb,} vista_albaranescliente{,vista_updatelineaalb,vista_asiglotesalb,vista_rectificaralb,vista_lineasalbnoedit,vista_veralb},vista_cobroalb;

procedure TAlbaran.ListarAlbaranescliente (codigoclt:string; var SQL: TSQLQuery; filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modeloAlbaran: TModeloAlbaran;

begin
  modeloAlbaran:=TModeloAlbaran.Create;
  if(Assigned(sql)) then
    freeandnil(sql);
  SQL:=modeloAlbaran.ListarAlbaranescliente(codigoclt,filtro,orden);

end;

procedure TAlbaran.ListarAlbaranes (var SQL: TSQLQuery; filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modeloAlbaran: TModeloAlbaran;


begin
  modeloAlbaran:=TModeloAlbaran.Create;
  if(Assigned(sql)) then
   freeandnil(sql);
  SQL:=modeloAlbaran.ListarAlbaranes(filtro,orden);

end;

procedure talbaran.MostrarcobrarAlbaran(idalb:integer;padre:tform);
begin

    formcobraralb.padre:=padre;
    formcobraralb.idAlb:=idalb;
    cargarVistaconvk(formcobraralb);
end;

function  Talbaran.obtenerConfiguracionAlb(): Tdictionary<string,variant>;

Var modeloAlbaran: tmodeloAlbaran;
    configuracion: Tdictionary<string,variant>;
    query:TSQLQuery;
begin


       modeloalbaran:= tmodeloAlbaran.create;
       configuracion:= tdictionary<string,variant>.create;
       query:=modeloalbaran.obtenerconfiguracionAlb();
       query.first;
       configuracion.add('IvaPrecios',query.fieldbyname('IVAPrecios').asstring);
       configuracion.add('ManejaBultos',query.fieldbyname('ManejaBultos').asstring);
       configuracion.add('DtoLineas',query.fieldbyname('DtoLineas').asstring);
       configuracion.add('PCambioDtoLinea',query.fieldbyname('PcambioDtoL').asstring);
       configuracion.add('PCambioDePrecio',query.fieldbyname('PCambioDeP').asstring);
       configuracion.add('PCambioTarifa',query.fieldbyname('PCambioTarifa').asstring);
       configuracion.add('PermitirSinCodigo',query.fieldbyname('PermitirSinCo').asstring);
       configuracion.add('VerTarifa1',query.fieldbyname('VerTarifa1').asstring);
       configuracion.add('VerTarifa2',query.fieldbyname('VerTarifa2').asstring);
       configuracion.add('VerTarifa3',query.fieldbyname('VerTarifa3').asstring);
       configuracion.add('VerTarifa4',query.fieldbyname('VerTarifa4').asstring);
       configuracion.add('VerTarifa5',query.fieldbyname('VerTarifa5').asstring);
       configuracion.add('VerTarifa6',query.fieldbyname('VerTarifa6').asstring);
       configuracion.add('iva1',query.fieldbyname('IVA1').asstring);
       configuracion.add('iva2',query.fieldbyname('IVA2').asstring);
       configuracion.add('iva3',query.fieldbyname('IVA3').asstring);
       configuracion.add('rec1',query.fieldbyname('REC1').asstring);
       configuracion.add('rec2',query.fieldbyname('REC2').asstring);
       configuracion.add('rec3',query.fieldbyname('REC3').asstring);
       configuracion.add('NumDecCan',query.fieldbyname('NumDecCan').asinteger);
       configuracion.add('NumDecPre',query.fieldbyname('NumDecPre').asinteger);
       configuracion.add('NumDecPorDes',query.fieldbyname('NumDecPor').asinteger);
       configuracion.add('NumDecTot',query.fieldbyname('NumDecTot').asinteger);
       configuracion.add('TamCodArt',query.fieldbyname('TamCodArt').asinteger);
       //configuracion.add('NumDecPorDes',query.fieldbyname('NumDecPor').asstring);
       query.Free;
       query:=nil;

       result:=configuracion;



end;

function  talbaran.obtenerDatosAlb(idalb:integer):Tdictionary <string,string>;
       var modeloalbaran:tmodeloalbaran;
        query:TSQLQuery;
        datosalb: Tdictionary <string,string>;
begin

      modeloalbaran:=tmodeloalbaran.create;
      datosalb:=Tdictionary <string,string>.create;
      query:=modeloalbaran.obtenerdatosalb(idalb);
      query.first;
      datosalb.add('serie',query.fieldbyname('serie').asstring);
      datosalb.add('numero',query.fieldbyname('numero').asstring);
      datosalb.add('fecha',FormatDateTime('dd/mm/yyyy',query.fieldbyname('fecha').AsDateTime));
      datosalb.add('cliente',query.fieldbyname('cliente').asstring);
      datosalb.add('total',query.fieldbyname('total').asstring);
      datosalb.add('cobrado',query.fieldbyname('cobrado').asstring);
      result:=datosalb;
      if(Assigned(query)) then
        freeandnil(query);

end;

procedure talbaran.realizarcobro(idalb:integer;datoscobro:tdictionary<string,string>);
var modeloalbaran:tmodeloalbaran;

begin

    modeloalbaran:=tmodeloalbaran.create;
    modeloalbaran.actualizarcobro(idalb,datoscobro);
    modeloalbaran.addAlblineacaja(idalb,datoscobro);
    modeloalbaran.liberarsql;




end;

function talbaran.albarancobrado(idalb:integer):boolean;
var datosalb:tdictionary<string,string>;
begin
  datosalb:=obtenerdatosalb(idalb);
  if (strtofloat(datosalb.items['total']) - strtofloat(datosalb.items['cobrado']))<= 0  then
      result:=true
  else
      result:=false;


end;


end.
