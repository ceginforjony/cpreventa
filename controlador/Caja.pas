unit Caja;

interface

{$ZEROBASEDSTRINGS ON}

uses System.Classes, FMX.Forms, Generics.Collections,Data.DbxSqlite,System.SysUtils, Data.DB, Data.SqlExpr,modelousuario,controlador,modelocaja,Data.FMTBcd;


type
  TCaja = class(TControlador) // Tambi�n puede ser solo class si derivas directamente de TObject

  public
    //rocedure cargar_menu(vista: tform2);
   procedure ListarCobrosCaja(var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
   procedure ListarCobrosCajaClt(codclt:string;var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
   function obtenersaldo():string;
   function obtenersaldoclt(codclt:string):string;
   function obtenertiposconcepto():TSQLQuery;
   function obtenerimpagoalb(codclt:string):TSQLQuery;
   function obtenerimpagofac(codclt:string):TSQLQuery;
   procedure registrarcobroentrada(datoscobro:tdictionary<string,variant>);
   procedure registrarcobrosalida(datoscobro:tdictionary<string,variant>);
   procedure mostrarnuevocobroclt(codclt:string);

  end;


implementation
uses vista_nuevocobroclt;
procedure tcaja.ListarCobrosCaja(var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modelocaja: TModeloCaja;

begin
  modelocaja:=TModeloCaja.Create;
 if(Assigned(sql)) then
  freeandnil(sql);
  SQL:= modeloCaja.ListarCobroscaja(filtro,orden);

  //showmessage(inttostr(SQL.RecordCount)+'segundo');
end;
procedure tcaja.ListarCobrosCajaClt(codclt:string;var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modelocaja: TModeloCaja;

begin
  modelocaja:=TModeloCaja.Create;
  if(Assigned(sql)) then
  freeandnil(sql);
  SQL:= modeloCaja.ListarCobroscajaClt(codclt,filtro,orden);

  //showmessage(inttostr(SQL.RecordCount)+'segundo');
end;

function tcaja.obtenersaldo():string;
var modelocaja:tmodelocaja;
begin
  modelocaja:=TModeloCaja.Create;
  result:= modeloCaja.obtenersaldo();
  modelocaja.liberarsql;
end;
function tcaja.obtenersaldoclt(codclt:string):string;
var modelocaja:tmodelocaja;
begin
  modelocaja:=TModeloCaja.Create;
  result:= modeloCaja.obtenersaldoclt(codclt);
  modelocaja.liberarsql;
end;



function tcaja.obtenertiposconcepto():TSQLQuery;
var modelocaja:tmodelocaja;
    tipos:tdictionary<integer,string>;
    query:TSQLQuery;
begin
 tipos:=tdictionary<integer,string>.create;
 modelocaja:=tmodelocaja.create;
 result:=modelocaja.obtenertiposconceptos();




end;
function tcaja.obtenerimpagoalb(codclt:string):TSQLQuery;
var modelocaja:tmodelocaja;
begin
  modelocaja:=tmodelocaja.Create;
  result:=modelocaja.obtenerimpagoalb(codclt);

end;
function tcaja.obtenerimpagofac(codclt:string):TSQLQuery;
var modelocaja:tmodelocaja;
begin
  modelocaja:=tmodelocaja.Create;
  result:=modelocaja.obtenerimpagofac(codclt);

end;
procedure tcaja.registrarcobroentrada(datoscobro:tdictionary<string,variant>);
var  modelocaja:tmodelocaja;
begin
modelocaja:=tmodelocaja.Create;
modelocaja.registrarcobroentrada(datoscobro);
modelocaja.liberarsql;



end;
procedure tcaja.registrarcobrosalida(datoscobro:tdictionary<string,variant>);
var  modelocaja:tmodelocaja;
begin
modelocaja:=tmodelocaja.Create;
modelocaja.registrarcobrosalida(datoscobro);
modelocaja.liberarsql;


end;
procedure tcaja.mostrarnuevocobroclt(codclt:string);
begin
formnuevocobroclt.codclt:=codclt;
cargarvista(formnuevocobroclt);


end;


  //SQL:=modeloArticulo.ListarArticulos(filtro,columnas);





end.
