unit Factura;

interface

{$ZEROBASEDSTRINGS ON}

  uses controlador,System.Classes, FMX.Forms, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,modeloCliente,modeloFactura,FMX.Dialogs, System.SysUtils,modeloUsuario,funciones,system.Math,Androidapi.JNI.BluetoothAdapter,Androidapi.JNI.JavaTypes,
  Androidapi.JNIBridge,Android.JNI.Toast,Androidapi.Helpers,modelocomunicacion,Data.FMTBcd;


type TIntArray = array of Integer;

type
  TFactura = class(TControlador) // Tambi�n puede ser solo class si derivas directamente de TObject

  public

    procedure ListarFacturasCliente(codigoclt:string;var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
    procedure ListarFacturas(var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
    procedure MostrarcobrarFactura(idfac:integer;padre:tform);
    function  obtenerConfiguracionFac(): Tdictionary<string,variant>;
    function  obtenerDatosFac(idfac:integer):Tdictionary <string,string>;
    procedure realizarcobro(idfac:integer;datoscobro:tdictionary<string,string>);
    function  facturacobrado(idfac:integer):boolean;



  end;

implementation
uses {vista_lineasfac, vista_nuevoFactura,vista_nuevaLineaFac, vista_asignarlotefac,} vista_facturascliente{,vista_updatelineafac,vista_asiglotesfac,vista_rectificarfac,vista_lineasfacnoedit,vista_verfac},vista_cobrofac;

procedure TFactura.ListarFacturascliente (codigoclt:string; var SQL: TSQLQuery; filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modeloFactura: TModeloFactura;

begin
  modeloFactura:=TModeloFactura.Create;
  freeandnil(sql);
  SQL:=modeloFactura.ListarFacturascliente(codigoclt,filtro,orden);

end;

procedure TFactura.ListarFacturas (var SQL: TSQLQuery; filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modeloFactura: TModeloFactura;


begin
  modeloFactura:=TModeloFactura.Create;
   freeandnil(sql);
  SQL:=modeloFactura.ListarFacturas(filtro,orden);

end;

procedure tfactura.MostrarcobrarFactura(idfac:integer;padre:tform);
begin

    formcobrofac.padre:=padre;
    formcobrofac.idFac:=idfac;
    cargarVistaconvk(formcobrofac);
end;

function  Tfactura.obtenerConfiguracionFac(): Tdictionary<string,variant>;

Var modeloFactura: tmodeloFactura;
    configuracion: Tdictionary<string,variant>;
    query:TSQLQuery;
begin


       modelofactura:= tmodeloFactura.create;
       configuracion:= tdictionary<string,variant>.create;
       query:=modelofactura.obtenerconfiguracionFac();
       query.first;
       configuracion.add('IvaPrecios',query.fieldbyname('IVAPrecios').asstring);
       configuracion.add('ManejaBultos',query.fieldbyname('ManejaBultos').asstring);
       configuracion.add('DtoLineas',query.fieldbyname('DtoLineas').asstring);
       configuracion.add('PCambioDtoLinea',query.fieldbyname('PcambioDtoL').asstring);
       configuracion.add('PCambioDePrecio',query.fieldbyname('PCambioDeP').asstring);
       configuracion.add('PCambioTarifa',query.fieldbyname('PCambioTarifa').asstring);
       configuracion.add('PermitirSinCodigo',query.fieldbyname('PermitirSinCo').asstring);
       configuracion.add('VerTarifa1',query.fieldbyname('VerTarifa1').asstring);
       configuracion.add('VerTarifa2',query.fieldbyname('VerTarifa2').asstring);
       configuracion.add('VerTarifa3',query.fieldbyname('VerTarifa3').asstring);
       configuracion.add('VerTarifa4',query.fieldbyname('VerTarifa4').asstring);
       configuracion.add('VerTarifa5',query.fieldbyname('VerTarifa5').asstring);
       configuracion.add('VerTarifa6',query.fieldbyname('VerTarifa6').asstring);
       configuracion.add('iva1',query.fieldbyname('IVA1').asstring);
       configuracion.add('iva2',query.fieldbyname('IVA2').asstring);
       configuracion.add('iva3',query.fieldbyname('IVA3').asstring);
       configuracion.add('rec1',query.fieldbyname('REC1').asstring);
       configuracion.add('rec2',query.fieldbyname('REC2').asstring);
       configuracion.add('rec3',query.fieldbyname('REC3').asstring);
       configuracion.add('NumDecCan',query.fieldbyname('NumDecCan').asinteger);
       configuracion.add('NumDecPre',query.fieldbyname('NumDecPre').asinteger);
       configuracion.add('NumDecPorDes',query.fieldbyname('NumDecPor').asinteger);
       configuracion.add('NumDecTot',query.fieldbyname('NumDecTot').asinteger);
       configuracion.add('TamCodArt',query.fieldbyname('TamCodArt').asinteger);
       //configuracion.add('NumDecPorDes',query.fieldbyname('NumDecPor').asstring);
       query.Free;
       query:=nil;

       result:=configuracion;



end;

function  tfactura.obtenerDatosFac(idfac:integer):Tdictionary <string,string>;
       var modelofactura:tmodelofactura;
        query:TSQLQuery;
        datosfac: Tdictionary <string,string>;
begin

      modelofactura:=tmodelofactura.create;
      datosfac:=Tdictionary <string,string>.create;
      query:=modelofactura.obtenerdatosfac(idfac);
      query.first;
      datosfac.add('serie',query.fieldbyname('serie').asstring);
      datosfac.add('numero',query.fieldbyname('numero').asstring);
      datosfac.add('fecha',FormatDateTime('dd/mm/yyyy',query.fieldbyname('fecha').AsDateTime));
      datosfac.add('cliente',query.fieldbyname('cliente').asstring);
      datosfac.add('total',query.fieldbyname('total').asstring);
      datosfac.add('cobrado',query.fieldbyname('cobrado').asstring);
      result:=datosfac;
      freeandnil(query);

end;

procedure tfactura.realizarcobro(idfac:integer;datoscobro:tdictionary<string,string>);
var modelofactura:tmodelofactura;

begin

    modelofactura:=tmodelofactura.create;
    modelofactura.actualizarcobro(idfac,datoscobro);
    modelofactura.addFaclineacaja(idfac,datoscobro);
    modelofactura.liberarsql;




end;

function tfactura.facturacobrado(idfac:integer):boolean;
var datosfac:tdictionary<string,string>;
begin
  datosfac:=obtenerdatosfac(idfac);
  if (strtofloat(datosfac.items['total']) - strtofloat(datosfac.items['cobrado']))<= 0  then
      result:=true
  else
      result:=false;


end;


end.
