unit Cliente;

interface

{$ZEROBASEDSTRINGS ON}
uses controlador,System.Classes, FMX.Forms, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,modeloCliente,FMX.Dialogs, System.SysUtils,Data.FMTBcd;


type
  TCliente = class(TControlador) // Tambi�n puede ser solo class si derivas directamente de TObject

  public

    procedure ListarClientes(var SQL: TSQLQuery;filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
    procedure MostrarCliente(codigo: string);
    procedure MostrarListaCLientes();
    Function ConsultarCliente(codigo:string):TDictionary<string,string>;
    procedure MostrarFormAlbaranesClt(codigoClt:string);
    procedure MostrarFormPedidosClt(codigoClt:string);
    procedure MostrarFormfacturasClt(codigoClt:string);
    procedure MostrarFormcobrosClt(codigoClt:string);
    function obtenerrutas():TSQLQuery;
    function obtenerdto(codigo:string):string;
    function  datoscltcompletable(codigo:string):tdictionary<string,string>;


  end;

implementation
uses
//vista_cliente, vista_albaranesCliente,vista_clientes,vista_cobrosclt,vista_facturascliente;
  vista_cliente,vista_clientes,vista_cobrosclt,vista_pedidoscliente,vista_albaranescliente,vista_facturascliente;
  procedure TCliente.ListarClientes (var SQL: TSQLQuery; filtro: TDictionary<String,String>;orden: TDictionary<String,String>);
var modelocliente: TModeloCliente;


begin
  modelocliente:=TModeloCliente.Create;
  if(Assigned(sql)) then
  freeandnil(sql);
  SQL:= modeloCliente.ListarClientes(filtro,orden);
  //showmessage(sql.Text);



end;

  Function TCliente.ConsultarCliente(codigo: string):TDictionary<string,string>;

  var modelocliente: TModeloCliente;
      DatosCliente: TDictionary<string,string>;
      QueryCliente: TSQLQuery;

begin
  datosCliente:= TDictionary<string,string>.create;
  modelocliente:=TModeloCliente.Create;
  QueryCliente:= modeloCliente.ConsultarCliente(codigo);
  QueryCliente.first;
  datosCliente.add('codigo',querycliente.fieldbyname('codigo').AsString);
  datosCliente.add('cliente',querycliente.fieldbyname('nombre').AsString);
  datosCliente.add('ncomercial',querycliente.fieldbyname('ncomercial').AsString);
  datosCliente.add('direccion',querycliente.fieldbyname('direccion').AsString);
  datosCliente.add('provincia',querycliente.fieldbyname('provincia').AsString);
  datosCliente.add('poblacion',querycliente.fieldbyname('poblacion').AsString);
  datosCliente.add('codpostal',querycliente.fieldbyname('cp').AsString);
  datosCliente.add('telefono',querycliente.fieldbyname('telefono').AsString);
  datosCliente.add('contacto',querycliente.fieldbyname('contacto').AsString);

  datosCliente.add('pen_alb',modelocliente.obtenerpendientealb(codigo));
  datosCliente.add('pen_fac',modelocliente.obtenerpendientefac(codigo));



  if(Assigned(querycliente)) then
    freeandnil(querycliente);
  result:=DatosCliente;
  //showmessage(inttostr(SQL.RecordCount)+'segundo');
end;

procedure Tcliente.MostrarFormAlbaranesClt( codigoClt: string);
 begin

  formalbaranesClt.codigoClt:=codigoClt;
  CargarVista(formalbaranesClt);
 end;

 procedure Tcliente.MostrarFormPedidosClt( codigoClt: string);
 begin

 formpedidosClt.codigoClt:=codigoClt;
 CargarVista(formpedidosClt);
 end;


procedure tcliente.MostrarFormfacturasClt(codigoClt:string);
begin
 formfacturasClt.codigoClt:=codigoClt;
 CargarVista(formfacturasClt);
end;


procedure tcliente.MostrarFormcobrosClt(codigoClt:string);
 begin

 formcobrosclt.codClt:=codigoClt;
 CargarVista(formcobrosclt);
 end;

procedure TCliente.MostrarCliente (codigo:string);

  begin
     formcliente.codigoClt:=codigo;
     cargarvista(formcliente);
     //formcliente.Show;
  end;
 procedure tcliente.MostrarListaCLientes();
 begin
        cargarvista(formclientes);
 end;
 function tcliente.datoscltcompletable(codigo:string):tdictionary<string,string>;
    var modelocliente:tmodelocliente;
       Datosclt: TDictionary<String,string>;
       queryClt:TSQLQuery;
   begin
   modelocliente:=tmodelocliente.create;
   datosclt:= tdictionary<string,string>.create;
   queryclt:= TSQLQuery.create(nil);
   if codigo <> '' then
   begin
   queryClt:= modelocliente.ConsultarClientesCompletable(codigo);
   if(queryclt.recordcount<>0) then
   begin
        queryclt.first;
        datosclt.add('codclt',queryclt.fieldbyname('codigo').AsString);
        datosclt.add('nombre',queryclt.fieldbyname('nombre').AsString);


   end;
   end;

   //modelocliente.free;
     if(Assigned(queryclt)) then
   freeandnil(queryclt);
   result:=datosclt;










 end;
 function tcliente.obtenerrutas():TSQLQuery;
  var modelocliente:tmodelocliente;
 begin

  modelocliente:=tmodelocliente.create;
  result:=modelocliente.obtenerrutas();







 end;
 function tcliente.obtenerdto(codigo:string):string;

 var modelocliente:tmodelocliente;
 begin

  modelocliente:=tmodelocliente.create;
  result:=modelocliente.obtenerdto(codigo);
 end;


end.
