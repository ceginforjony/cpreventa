unit ModeloCliente;

interface

{$ZEROBASEDSTRINGS ON}

 uses Modelo, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,FMX.Dialogs, System.SysUtils,funciones,Data.FMTBcd;


type
  TModeloCliente = class(TModelo) // Tambi�n puede ser solo class si derivas directamente de TObject
  private
  {}
  protected
    { Declaraciones protegidas de la clase }
  public
    function ListarClientes( filtro:TDictionary<String,String>;orden:TDictionary<String,String> ):TSQLQuery;
    function ConsultarCliente( codigo: string ): TSQLQuery;
    function  ObtenerRecargo(codigo:string):string;
    function  ConsultarClientesCompletable(codigo:string): TSQLQuery;
    function  Obtenerrutas():TSQLQuery;
    function obtenerpendientealb(codigo:string):string;
    function obtenerpendientefac(codigo:string):string;
    function obtenercodtabdesclt(codigo:string):integer;
    function ofertadisponible(codigo:string):boolean;
    function  Obtenerdto(codigo:string):string;
  published
    { Declaraciones publicadas de la clase }
  end;






implementation

 function TModeloCliente.ListarClientes( filtro:TDictionary<String,String>;orden:TDictionary<String,String>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;
     rut:string;
     rutwhere:string;


 begin
 //Utiliza ITERADORESSSSSSSSS for(i=0;i<filtro.Count;i++)

   if ruta<>'-1' then
      begin
      rut:=' and ruta="'+ruta+'" ';
      rutwhere:=' where ruta="'+ruta+'" ';
      end
   else
      begin
       rut:='';
       rutwhere:='';
      end;






    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

 if(filtro.Count = 0) then
    begin
      //showmessage('no tiene elementos');
      EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal   from v_clientes '+rutwhere+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
    end
 else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if (caso <> 0) and (caso <> 1) then
        begin
        filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
        end;


      case caso of
      0: EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal from v_clientes where UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '")'+rut+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
      1: EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal  from v_clientes where '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%"'+rut+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
      2: EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal  from v_clientes where cast('+ filtro.items['col'] +' as numeric) = '+ filtro.items['texto']+rut+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
      3: EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal  from v_clientes where '+ filtro.items['col'] +' < '+ filtro.items['texto']+rut+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
      4: EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal  from v_clientes where '+ filtro.items['col'] +' > '+ filtro.items['texto']+rut+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
      5: EjecutarInstruccion('select C�digo,Cliente,NComercial,Direcci�n,Poblaci�n,C�dPostal  from v_clientes where cast('+ filtro.items['col'] +' as numeric) <> '+ filtro.items['texto']+rut+ ' order by v_clientes.'+orden.items['column']+' '+orden.items['tipo']  );
      end;

    end;

  //showmessage(inttostr(DevolverBD().RecordCount)+ 'primero');
 Result:=Self.DevolverBD();
end;

function TModeloCliente.ConsultarCliente( codigo: string ): TSQLQuery;

begin
      EjecutarInstruccion('select * from clientes where codigo = '+codigo);
      Result:=Self.DevolverBD();
end;


function TmodeloCliente.ObtenerRecargo(codigo:string):string;
begin
   Ejecutarinstruccion('select recargo from clientes where codigo='+codigo);
   result:=devolverbd().FieldByName('recargo').AsString;

end;
function TmodeloCliente.Obtenerdto(codigo:string):string;
begin
   Ejecutarinstruccion('select pordto from clientes where codigo='+codigo);
   result:=devolverbd().FieldByName('pordto').AsString;

end;
function TmodeloCliente.obtenerpendientealb(codigo:string):string;

var query:TSQLQuery;
begin


   Ejecutarinstruccion('SELECT sum(importe-cobrado) as pendientealb FROM v_cobrosalb where cliente='+codigo);
   query:=Self.DevolverBD();
  if(query.RecordCount=0)then
  begin
    result:='0';
  end
  else
      result:=query.FieldByName('pendientealb').AsString;

  //freeandnil(query);

end;
function TmodeloCliente.obtenerpendientefac(codigo:string):string;

var query:TSQLQuery;
begin


   Ejecutarinstruccion('SELECT sum(importe-cobrado) as pendientefac FROM v_cobrosfac where cliente='+codigo);
   query:=Self.DevolverBD();
  if(query.RecordCount=0)then
  begin
    result:='0';
  end
  else
      result:=query.FieldByName('pendientefac').AsString;

  //freeandnil(query);

end;

 function TmodeloCliente.obtenercodtabdesclt(codigo:string):integer;

var query:TSQLQuery;
begin


   Ejecutarinstruccion('SELECT codtabladto FROM clientes where codigo='+codigo);
   query:=Self.DevolverBD();
   query.First;
   result:=query.FieldByName('codtabladto').AsInteger;

  //freeandnil(query);

end;
 function TmodeloCliente.ofertadisponible(codigo:string):boolean;

var query:TSQLQuery;
begin


   Ejecutarinstruccion('SELECT codigo FROM mdescuentos where codigo='+codigo+' and fecha_ini <= "'+formatdatetime('YYYY-MM-DD',DATE)+'" and fecha_hasta >= "'+formatdatetime('YYYY-MM-DD',DATE)+'"');
   query:=Self.DevolverBD();
   if query.RecordCount=0 then
      result:=false
   else
      result:=true;


  //freeandnil(query);

end;



 function  tmodelocliente.ConsultarClientesCompletable(codigo:string): TSQLQuery;
 var query:TSQLQuery;
begin
  EjecutarInstruccion('select * from clientes where codigo = '+codigo);
  query:=Self.DevolverBD();

  result:=query;



 end;
  function  tmodelocliente.Obtenerrutas():TSQLQuery;
  begin

    EjecutarInstruccion('select * from rutas');
    Result:=Self.DevolverBD();

  end;

end.
