unit ModeloFactura;

interface

      {$ZEROBASEDSTRINGS ON}

  uses Modelo, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,FMX.Dialogs, System.SysUtils,funciones,System.Variants,Data.FMTBcd;


type
  TModeloFactura = class(TModelo) // Tambi�n puede ser solo class si derivas directamente de TObject
  private
  //function ObtenerUltimaLineaFac(serie:string;numero:integer;ano:integer):integer;
  {}
  protected
    { Declaraciones protegidas de la clase }
  public
    function ListarFacturasCliente(codigoClt:string; filtro:TDictionary<String,String>;orden: TDictionary<String,String> ):TSQLQuery;
    function ListarFacturas(filtro:TDictionary<String,String>;orden: TDictionary<String,String> ):TSQLQuery;
    function obtenerconfiguracionFac():TSQLQuery;
    function  ObtenerDatosFac(idfac:integer):TSQLQuery;
    procedure actualizarcobro(idfac:integer;datoscobro:tdictionary<string,string>);
    procedure addFaclineacaja(idfac:integer;datoscobro:tdictionary<string,string>);

  published
    { Declaraciones publicadas de la clase }
  end;






implementation

 function TModeloFactura.ListarFacturasCliente(codigoClt:string; filtro:TDictionary<String,String>;orden: TDictionary<String,String>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;


 begin


    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

 if(filtro.Count = 0) then
    begin

      EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado  from v_facturas where  C�dCliente =' +codigoclt + ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
    end
 else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if filtro.Items['fecha']<>'S' then
      begin
          if (caso <> 0) and (caso <> 1) then
            begin
            filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
            end;


          case caso of
          0: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where C�dCliente =' +codigoclt+' and   UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '")'+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          1: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where C�dCliente =' +codigoclt+' and  '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%"'+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          2: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where C�dCliente =' +codigoclt+' and   cast('+ filtro.items['col'] +' as numeric) = '+ filtro.items['texto']+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          3: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where C�dCliente =' +codigoclt+' and    '+ filtro.items['col'] +' < '+ filtro.items['texto']+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          4: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where C�dCliente =' +codigoclt+' and   '+ filtro.items['col'] +' > '+ filtro.items['texto']+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          5: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado  from v_facturas where C�dCliente =' +codigoclt+' and  cast('+ filtro.items['col'] +' as numeric) <> '+ filtro.items['texto']+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          end;

      end
      else
      begin
           case caso of

            2: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado  from v_facturas where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' = "'+ filtro.items['texto']+'"'+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            3: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' < "'+ filtro.items['texto']+'"'+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            4: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' > "'+ filtro.items['texto']+'"'+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            5: EjecutarInstruccion('select id, Serie, N�mero,Fecha,Total, Cobrado   from v_facturas where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' <> "'+ filtro.items['texto']+'"'+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            end;
      end;
    end;

  //showmessage(inttostr(DevolverBD().RecordCount)+ 'primero');
 Result:=Self.DevolverBD();
end;
function TModeloFactura.ListarFacturas(filtro:TDictionary<String,String>;orden: TDictionary<String,String>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;
      rut1,rut:string;


 begin
 //Utiliza ITERADORESSSSSSSSS for(i=0;i<filtro.Count;i++)

     if ruta<>'-1' then
      begin
        rut1:=' where ruta="'+ruta+'" ';
        rut:=' ruta="'+ruta+'" ';
      end
     else
      begin
        rut:='';
        rut1:='';

      end;



    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

    if(filtro.Count = 0) then
    begin
      //showmessage('no tiene elementos');
      EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado  from v_facturas '+ rut1 +' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
    end
    else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if filtro.Items['fecha']<>'S' then
      begin
          if (caso <> 0) and (caso <> 1) then
            begin
            filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
            end;


          case caso of
          0: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where    UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '")'+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          1: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%"'+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          2: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where    cast('+ filtro.items['col'] +' as numeric) = '+ filtro.items['texto']+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          3: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where     '+ filtro.items['col'] +' < '+ filtro.items['texto']+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          4: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where    '+ filtro.items['col'] +' > '+ filtro.items['texto']+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          5: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where   cast('+ filtro.items['col'] +' as numeric) <> '+ filtro.items['texto']+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
          end;

      end
      else
      begin
           case caso of

            2: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where  '+ filtro.items['col'] +' = "'+ filtro.items['texto']+'"'+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            3: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where  '+ filtro.items['col'] +' < "'+ filtro.items['texto']+'"'+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            4: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where '+ filtro.items['col'] +' > "'+ filtro.items['texto']+'"'+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            5: EjecutarInstruccion('select id, Serie, N�mero,Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_facturas where '+ filtro.items['col'] +' <> "'+ filtro.items['texto']+'"'+rut+ ' order by v_facturas.'+orden.items['column']+' '+orden.items['tipo']  );
            end;
      end;
    end;


    Result:=Self.DevolverBD();
end;

function TModeloFactura.obtenerconfiguracionFac():TSQLQuery;
begin
    EjecutarInstruccion('select * from configuracion');
    Result:=Self.DevolverBD();
end;

function tmodelofactura.ObtenerDatosFac(idfac:integer):TSQLQuery;
begin
    EjecutarInstruccion('select numero,serie,fecha,cliente,importe as total,cobrado,delegacion,dependiente,observaciones,ano,enviado from facturas where id='+inttostr(idfac));
    result:=Self.DevolverBD();
end;

procedure tmodelofactura.actualizarcobro(idfac:integer;datoscobro:tdictionary<string,string>);
var cobro:string;
          cobrado:double;
          cobrar:string;
          queryconf:TSQLQuery;
          DecTotal:integer;
begin
    queryconf:=obtenerconfiguracionFac();
    queryconf.first;
    DecTotal:=queryconf.fieldbyname('NumDecTot').asinteger;
    cobro:=stringreplace(datoscobro.Items['cobro'],'.',',',[rfReplaceAll]);
    ejecutarinstruccion('select *  from Facturas where id = '+inttostr(idfac));
    cobrado:=redondear(Devolverbd().FieldByName('cobrado').AsFloat+strtofloat(cobro),DecTotal);
    cobrar:=formatearnumero(DecTotal,floattostr(cobrado));
    cobrar:=stringreplace(cobrar,',','.',[rfReplaceAll]);
    Ejecutarmodificacion('update Facturas set cobrado =   '+cobrar+' where id = '+inttostr(idfac));

    //cobro:=stringreplace(datoscobro.Items['cobro'],',','.',[rfReplaceAll]);
    //Ejecutarmodificacion('update Facturas set cobrado = cobrado + '+cobro+' where id = '+inttostr(idfac));
end;

procedure tmodelofactura.addFaclineacaja(idfac:integer;datoscobro:tdictionary<string,string>);
var query: TSQLQuery;
    columnas:array[0..11] of string;
    valores:array[0..11] of variant;
begin
     columnas[0]:='numero';
     columnas[1]:='serie';
     columnas[2]:='fecha';
     columnas[3]:='ano';
     columnas[4]:='cliente';
     columnas[5]:='dependiente';
     columnas[6]:='importe';
     columnas[7]:='cobrado';
     columnas[8]:='marca';
     columnas[9]:='tipoC';
     columnas[10]:='Enviado';
     columnas[11]:='Comentarios';

     query:=obtenerdatosfac(idfac);
     query.First;
     valores[0]:= query.FieldByName('numero').asinteger;
     valores[1]:= query.FieldByName('serie').asstring;
     valores[2]:= formatdatetime('YYYY-MM-DD',DATE);
     valores[3]:= anoamericano(formatdatetime('YYYY-MM-DD',query.FieldByName('fecha').AsDateTime)) ;
     valores[4]:= query.FieldByName('cliente').asstring;


     valores[6]:= query.FieldByName('total').asfloat;

     valores[7]:=strtofloat(datoscobro.items['cobro']);
     valores[8]:='N';
     valores[9]:=3;
     valores[10]:='NULL';
     valores[11]:=datoscobro.items['comentarios'];




      EjecutarInstruccion('select dependiente from configuracion');
      valores[5]:=self.DevolverBD().FieldByName('dependiente').AsString;
      EjecutarInsert('Caja',columnas,valores);

end;


end.
