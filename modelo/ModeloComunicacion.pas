unit ModeloComunicacion;

interface

     {$ZEROBASEDSTRINGS ON}

 uses Modelo,System.Classes,XmlIntf, XmlDoc,Xml.xmldom, Xml.adomxmldom, System.Variants, Generics.Collections,DATA.DBXCOMMON,Data.DbxSqlite,Datasnap.DBClient, Data.DB, Data.SqlExpr,FMX.Dialogs, System.SysUtils,funciones,system.IOUtils,Data.FMTBcd,ModeloPedido;


type
  TModeloComunicacion = class(TModelo) // Tambi�n puede ser solo class si derivas directamente de TObject
  private
  {}
  protected
    { Declaraciones protegidas de la clase }
  public

    procedure ReiniciarBaseDeDatos();
    procedure ReiniciarPaquete(idped:integer);
    procedure ProcesarArticulos(var tabla:tstrings);
    procedure Procesarclientes(var tabla:tstrings);
    procedure ProcesarAlmacenes(var tabla:tstrings);
    procedure ProcesarCBarras(var tabla:tstrings);
    procedure ProcesarConceptosC(var tabla:tstrings);
    procedure ProcesarConfiguracion( var tabla:tstrings);
    procedure ProcesarContadores(var tabla:tstrings);
    procedure ProcesarMDescuentos( var tabla:tstrings);
    procedure ProcesarDDescuentos(var tabla:tstrings);

    procedure ProcesarMPedidos(var tabla:tstrings);


     procedure ProcesarDPedidos(var tabla:tstrings);
    procedure ProcesarFacturas(var tabla:tstrings);
    procedure ProcesarAlbaranes(var tabla:tstrings);
    procedure ProcesarFormasPago(var tabla:tstrings);

    procedure ProcesarRutas(var tabla:tstrings);
    procedure ProcesarTXTBultos(var tabla:tstrings);

    procedure escribircontadores();
    procedure escribircaja(id_pedido:integer);
    procedure escribirDPedidos(idped:integer);
    procedure escribirMPedidos(idped:integer);

    function ComprobarPaquete():boolean;
    function obtenerdispositivo():TSQLQuery;
    procedure establecerdispositivo(nombre,mac:string);





  published
    { Declaraciones publicadas de la clase }
  end;






implementation

 procedure TModeloComunicacion.ReiniciarBaseDeDatos() ;

begin


   self.ejecutarmodificacion('delete from sqlite_sequence where name="Articulos"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="Clientes"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="Almacenes"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="CBarras"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="Contadores"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="MDescuentos"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="DDescuentos"');

   self.ejecutarmodificacion('delete from sqlite_sequence where name="MPedidos"');

    self.ejecutarmodificacion('delete from sqlite_sequence where name="DPedidos"');

   self.ejecutarmodificacion('delete from sqlite_sequence where name="Facturas"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="Albaranes"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="FormasPago"');

   self.ejecutarmodificacion('delete from sqlite_sequence where name="Rutas"');
   self.ejecutarmodificacion('delete from sqlite_sequence where name="Caja"');

   self.Ejecutarmodificacion('delete from articulos');
   self.Ejecutarmodificacion('delete from clientes');
   self.Ejecutarmodificacion('delete from almacenes');
   self.Ejecutarmodificacion('delete from CBarras');
   self.Ejecutarmodificacion('delete from ConceptosC');
   self.Ejecutarmodificacion('delete from Configuracion');
   self.Ejecutarmodificacion('delete from Contadores');
   self.Ejecutarmodificacion('delete from MDescuentos');
   self.Ejecutarmodificacion('delete from DDescuentos');


   self.Ejecutarmodificacion('delete from MPedidos');
   self.Ejecutarmodificacion('delete from DPedidos');

   self.Ejecutarmodificacion('delete from Facturas');
   self.Ejecutarmodificacion('delete from Albaranes');
   self.Ejecutarmodificacion('delete from FormasPago');

   self.Ejecutarmodificacion('delete from Rutas');
   self.Ejecutarmodificacion('delete from Caja');
   self.Ejecutarmodificacion('delete from TXTBultos');

end;
            function TModelocomunicacion.Obtenerdispositivo():TSQLQuery;
begin
         EjecutarInstruccion('select * from impresora');
         Result:=Self.DevolverBD();
end;
function TModelocomunicacion.ComprobarPaquete():boolean;
var b:boolean;
begin

         EjecutarInstruccion('select * from caja');
         self.DevolverBD.First;
         if self.DevolverBD.RecordCount=0 then
         begin
             Ejecutarinstruccion('select * from MPedidos where Marca = "N"');
             self.DevolverBD.First;
             if self.DevolverBD.RecordCount=0 then
             begin
                    Result:=false;
             end
             else
                    Result:=true;
         end
         else
              Result:=true;
end;
procedure TModelocomunicacion.establecerdispositivo(nombre,mac:string);
  var
      columnas:array[0..1] of string;
      valores:array[0..1] of variant;

  begin

  columnas[0]:='nombre';
  columnas[1]:='mac';

  valores[0]:= nombre;
  valores[1]:= mac;

  Ejecutarmodificacion('delete from impresora');
  EjecutarInsert('impresora',columnas,valores);


  end;
procedure TModeloComunicacion.ReiniciarPaquete(idped:integer) ;
       var query: TSQLQuery;
           modelopedido:TModeloPedido;
begin

   if(idped = 0) then
   begin
      self.ejecutarmodificacion('delete from sqlite_sequence where name="MPedidos"');

      self.ejecutarmodificacion('delete from sqlite_sequence where name="DPedidos"');
      self.ejecutarmodificacion('delete from sqlite_sequence where name="Caja"');

      self.Ejecutarmodificacion('delete from MPedidos');
      self.Ejecutarmodificacion('delete from DPedidos');
      self.Ejecutarmodificacion('delete from Caja');
   end
   else
   begin
      modelopedido := TModeloPedido.Create;
      query:=modelopedido.obtenerSerie(idped);
      self.Ejecutarmodificacion('update MPedidos set procesada = 1 where id = '+idped.ToString());
      self.Ejecutarmodificacion('update DPedidos set procesada = 1 where numero = '+query.fieldbyname('numero').AsString+' and ano ='+copy(FechaEuropea(query.FieldByName('fecha').AsString),9,10)+' and serie="'+query.fieldbyname('serie').AsString+'"'  );
      modelopedido.liberarsql;
   end;


end;


 procedure TModeloComunicacion.ProcesarArticulos(var tabla:tstrings) ;

var
         columnas:array[0..16] of string;
         valores:array[0..16] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;

     begin
     columnas[0]:='codigo';
     columnas[1]:='descripcion';
     columnas[2]:='tarifa1';
     columnas[3]:='tarifa2';
     columnas[4]:='tarifa3';
     columnas[5]:='tarifa4';
     columnas[6]:='tarifa5';
     columnas[7]:='tarifa6';
     columnas[8]:='stock';
     columnas[9]:='iva';
     columnas[10]:='unicaj';
     columnas[11]:='clase';
      columnas[12]:='temporada';
     columnas[13]:='enviado';
     columnas[14]:='unidad';
     //columnas[15]:='serie';
     columnas[15]:='familia';
     columnas[16]:='subfamilia';
     //columnas[18]:='similaux';

     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'ARTICULOS',registro);
     tr:= cdatos.BeginTransaction;
    While registro.Text<>'' Do
    Begin
     valor:=devuelve_valores(registro);

     valores[0]:= valor[0];
     valores[1]:= valor[1];
     valores[2]:= convertreal(valor[2]);
     valores[3]:= convertreal(valor[3]);
     valores[4]:= convertreal(valor[4]);
     valores[5]:= convertreal(valor[5]);
     valores[6]:= convertreal(valor[6]);
     valores[7]:= convertreal(valor[7]);
     valores[8]:= convertreal(valor[8]);
     valores[9]:= convertreal(valor[9]);

     valores[10]:=convertreal(valor[10]);
     valores[11]:=valor[11];
     valores[12]:=valor[12];
     valores[13]:=valor[13];
     valores[14]:=valor[14];
     //valores[15]:=valor[15];
     valores[15]:=valor[15];
     valores[16]:=valor[16];
     //valores[18]:=valor[18];

     EjecutarInsert('Articulos',columnas,valores);

     devuelve_registro(archivo,'ARTICULOS',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);

end;

procedure TModeloComunicacion.ProcesarAlmacenes(var tabla:tstrings) ;

var
         columnas:array[0..1] of string;
         valores:array[0..1] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;

     begin
     columnas[0]:='codigo';
     columnas[1]:='descripcion';


     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'ALMACENES',registro);

     tr:= cdatos.BeginTransaction;


    While registro.Text<>'' Do
    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     EjecutarInsert('Almacenes',columnas,valores);

     devuelve_registro(archivo,'ALMACENES',registro);

    End;


    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);

end;
procedure TModeloComunicacion.ProcesarConceptosC(var tabla:tstrings) ;

var
         columnas:array[0..3] of string;
         valores:array[0..3] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;

     begin
     columnas[0]:='codigo';
     columnas[1]:='nombre';
     columnas[2]:='tipo';
     columnas[3]:='enviado';


     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'CONCEPTOSC',registro);

         tr:= cdatos.BeginTransaction;

      //showmessage('aqui entra2');
    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];

     valores[2]:= valor[2];
     valores[3]:= valor[3];
     EjecutarInsert('ConceptosC',columnas,valores);
     devuelve_registro(archivo,'CONCEPTOSC',registro);

    End;


    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;








procedure TModeloComunicacion.ProcesarFormasPago(var tabla:tstrings) ;

var
         columnas:array[0..2] of string;
         valores:array[0..2] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;

     begin
     columnas[0]:='codigo';
     columnas[1]:='nombre';
     columnas[2]:='enviado';

     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'FORMASPAGO',registro);
     tr:= cdatos.BeginTransaction;

      //showmessage('aqui entra2');
    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];

     valores[2]:= valor[2];
     EjecutarInsert('FormasPago',columnas,valores);

     devuelve_registro(archivo,'FORMASPAGO',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
     freeandnil(registro);
    freeandnil(valor);

end;




procedure TModeloComunicacion.ProcesarMDescuentos(var tabla:tstrings) ;

var
         columnas:array[0..3] of string;
         valores:array[0..3] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='codigo';
     columnas[1]:='fecha_ini';
     columnas[2]:='fecha_hasta';
     columnas[3]:='enviado';
     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'MDESCUENTOS',registro);
     tr:= cdatos.BeginTransaction;

    While registro.Text<>'' Do
    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= fechaamericana(valor[1]);

     valores[2]:= fechaamericana(valor[2]);
     valores[3]:= valor[3];
     EjecutarInsert('MDescuentos',columnas,valores);

     devuelve_registro(archivo,'MDESCUENTOS',registro);

    End;

    cdatos.CommitFreeAndNil(tr);
     freeandnil(registro);
    freeandnil(valor);
end;

procedure TModeloComunicacion.ProcesarDDescuentos(var tabla:tstrings) ;

var
         columnas:array[0..11] of string;
         valores:array[0..11] of variant;
          registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='codigoM';
     columnas[1]:='cod_ini';
     columnas[2]:='cod_fin';
     columnas[3]:='fam_ini';
     columnas[4]:='fam_fin';
     columnas[5]:='sub_ini';
     columnas[6]:='sub_fin';
     columnas[7]:='dto';
     columnas[8]:='clase';
     columnas[9]:='tipo';
     columnas[10]:='precio';
     columnas[11]:='enviado';

     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'DDESCUENTOS',registro);
     tr:= cdatos.BeginTransaction;
    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];

     valores[2]:= valor[2];
     valores[3]:= valor[3];
     valores[4]:= valor[4];
     valores[5]:= valor[5];

     valores[6]:= valor[6];
     valores[7]:= CONVERTREAL(valor[7]);
     valores[8]:= valor[8];
     valores[9]:= valor[9];

     valores[10]:= CONVERTREAL(valor[10]);
     valores[11]:= valor[11];



     EjecutarInsert('DDescuentos',columnas,valores);

     devuelve_registro(archivo,'DDESCUENTOS',registro);

    End;

    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;

 procedure TModeloComunicacion.ProcesarContadores(var tabla:tstrings) ;

var
     columnas:array[0..8] of string;
     valores:array[0..8] of variant;
     registro:tstrings;
     valor:tstrings;
     archivo:STRING;
     tr : TDBXTransaction;
     begin

     columnas[0]:='serie';
     columnas[1]:='actual';
     columnas[2]:='tipodoc';
     columnas[3]:='contadorfin';
     columnas[4]:='contadorini';
     columnas[5]:='ano';
     columnas[6]:='enviado';
     columnas[7]:='iva';
     columnas[8]:='almacen';

     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;

     devuelve_registro(archivo,'CONTADORES',registro);
     tr:= cdatos.BeginTransaction;

    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];

     valores[2]:= valor[2];
     valores[3]:= valor[3];
     valores[4]:= valor[4];
     valores[5]:= valor[5];

     valores[6]:= valor[6];
     valores[7]:= valor[7];
     valores[8]:= valor[8];

      //ejecutarinstruccion('Select name,sql from sqlite_master where type='+quotedstr('table')+' order by name');

    {
     while not self.DevolverBD.Eof do
     begin
       //showmessage(self.DevolverBD.FieldByName('sql').AsString);
       self.DevolverBD.Next;
     end;
     }
     //SHOWMESSAGE('ANTES DE INSERT');
     EjecutarInsert('Contadores',columnas,valores);
     //SHOWMESSAGE('Depues DE INSERT');
     devuelve_registro(archivo,'CONTADORES',registro);

    End;
    cdatos.CommitFreeAndNil(tr);

    freeandnil(registro);
    freeandnil(valor);

end;



procedure TModeloComunicacion.ProcesarCBarras(var tabla:tstrings) ;

var
         columnas:array[0..1] of string;
         valores:array[0..1] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:string;
         tr : TDBXTransaction;
     begin
     columnas[0]:='codigo';
     columnas[1]:='barra';

    // showmessage('aqui entra');
     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'CBARRAS',registro);
     tr:= cdatos.BeginTransaction;

      //showmessage('aqui entra2');
    While registro.Text<>'' Do
    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     EjecutarInsert('CBarras',columnas,valores);

     devuelve_registro(archivo,'CBARRAS',registro);

    End;
    cdatos.CommitFreeAndNil(tr);

    freeandnil(registro);
    freeandnil(valor);

end;

procedure TModeloComunicacion.ProcesarRutas(var tabla:tstrings) ;

var
         columnas:array[0..1] of string;
         valores:array[0..1] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='codigo';
     columnas[1]:='nombre';
       Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'RUTAS',registro);
     tr:= cdatos.BeginTransaction;

      //showmessage('aqui entra2');
    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     EjecutarInsert('Rutas',columnas,valores);
     devuelve_registro(archivo,'RUTAS',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
     freeandnil(registro);
    freeandnil(valor);


end;

procedure TModeloComunicacion.ProcesarTXTBultos(var tabla:tstrings) ;

var
         columnas:array[0..1] of string;
         valores:array[0..1] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='texto';
     columnas[1]:='enviado';

       Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'TXTBULTOS',registro);
     tr:= cdatos.BeginTransaction;
      //showmessage('aqui entra2');
    While registro.Text<>'' Do
    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     EjecutarInsert('txtbultos',columnas,valores);

     devuelve_registro(archivo,'TXTBULTOS',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);

end;




procedure TModeloComunicacion.Procesarclientes(var tabla:tstrings) ;
 var
         columnas:array[0..28] of string;
         valores:array[0..28] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='codigo';
     columnas[1]:='nombre';
     columnas[2]:='cif';
     columnas[3]:='direccion';
     columnas[4]:='poblacion';
     columnas[5]:='cp';
     columnas[6]:='provincia';
     columnas[7]:='contacto';
     columnas[8]:='telefono';
     columnas[9]:='fax';
     columnas[10]:='riesgo';
     columnas[11]:='ruta';
      columnas[12]:='recargo';
     columnas[13]:='delegacion';
     columnas[14]:='nombredlg';
     columnas[15]:='marca';
     columnas[16]:='CodTablaDto';
     columnas[17]:='PorDto';
     columnas[18]:='FormaPago';

      columnas[19]:='email';
     columnas[20]:='comentarios';
     columnas[21]:='orden';
     columnas[22]:='tarifa';
     columnas[23]:='enviado';
     columnas[24]:='Tot_ven';
     columnas[25]:='Pen_fac';
     columnas[26]:='Pen_cob';
     columnas[27]:='cc';
     columnas[28]:='ncomercial';
    // showmessage('aqui entra');
       Registro:=TStringList.Create;
     valor:=TStringList.Create;
      archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'CLIENTES',registro);
     tr:= cdatos.BeginTransaction;
      //showmessage('aqui entra2');
    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);

     valores[0]:= Strtoint(valor[0]);
     valores[1]:= valor[1];
     //showmessage('aqui entra 2');
     valores[2]:= valor[2];
     valores[3]:= valor[3];
     valores[4]:= valor[4];
     valores[5]:= valor[5];
     valores[6]:= valor[6];
     valores[7]:= valor[7];
     valores[8]:= valor[8];
     valores[9]:= valor[9];
     //showmessage('aqui entra 3');
     valores[10]:=valor[10];
     valores[11]:=valor[11];
     valores[12]:=valor[12];
     valores[13]:=valor[13];
     valores[14]:=valor[14];
     valores[15]:=valor[15];
     valores[16]:=valor[16];
     valores[17]:=CONVERTREAL(valor[17]);
     valores[18]:=valor[18];
     valores[19]:=valor[19];
     valores[20]:=valor[20];
     valores[21]:=valor[21];
     valores[22]:=valor[22];
     valores[23]:=valor[23];
     valores[24]:=convertreal(valor[24]);
     valores[25]:=convertreal(valor[25]);
     valores[26]:=convertreal(valor[26]);
     valores[27]:=valor[27];
     valores[28]:=valor[28];

     EjecutarInsert('Clientes',columnas,valores);

     devuelve_registro(archivo,'CLIENTES',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;

procedure TModeloComunicacion.ProcesarMpedidos(var tabla:tstrings) ;
 var
         columnas:array[0..31] of string;
         valores:array[0..31] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='serie';
     columnas[1]:='numero';
     columnas[2]:='tipodocumento';
     columnas[3]:='fecha';
     columnas[4]:='cliente';
     columnas[5]:='delegacion';
     columnas[6]:='dependiente';
     columnas[7]:='observaciones';
     columnas[8]:='base1';
     columnas[9]:='base2';
     columnas[10]:='base3';
     columnas[11]:='rec1';
      columnas[12]:='rec2';
     columnas[13]:='rec3';
     columnas[14]:='total';
     columnas[15]:='bruto';
     //columnas[16]:='cobrado';
     columnas[16]:='p_descuento';
     columnas[17]:='estado';

      columnas[18]:='c_descuento';
     columnas[19]:='iva1';
     columnas[20]:='iva2';
     columnas[21]:='iva3';
     columnas[22]:='impiva1';
     columnas[23]:='impiva2';
     columnas[24]:='impiva3';
     columnas[25]:='imprec1';
     columnas[26]:='imprec2';
     columnas[27]:='imprec3';
     columnas[28]:='marca';
     columnas[29]:='enviado';
     columnas[30]:='fentrega';
     columnas[31]:='hora';
     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'MPEDIDOS',registro);
     tr:= cdatos.BeginTransaction;
    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     valores[2]:= valor[2];
     valores[3]:= fechaamericana(valor[3]);
     valores[4]:= valor[4];
     valores[5]:= valor[5];
     valores[6]:= valor[6];
     valores[7]:= valor[7];
     valores[8]:= CONVERTREAL(valor[8]);
     valores[9]:= CONVERTREAL(valor[9]);
     valores[10]:=CONVERTREAL(valor[10]);
     valores[11]:=CONVERTREAL(valor[11]);
     valores[12]:=CONVERTREAL(valor[12]);
     valores[13]:=CONVERTREAL(valor[13]);
     valores[14]:=CONVERTREAL(valor[14]);
     valores[15]:=CONVERTREAL(valor[15]);
     //valores[16]:=CONVERTREAL(valor[16]);
     valores[16]:=CONVERTREAL(valor[16]);
     valores[17]:=valor[17];
     valores[18]:=CONVERTREAL(valor[18]);
     valores[19]:=CONVERTREAL(valor[19]);
     valores[20]:=CONVERTREAL(valor[20]);
     valores[21]:=CONVERTREAL(valor[21]);
     valores[22]:=CONVERTREAL(valor[22]);
     valores[23]:=convertreal(valor[23]);
     valores[24]:=convertreal(valor[24]);
     valores[25]:=convertreal(valor[25]);
     valores[26]:=CONVERTREAL(valor[26]);
     valores[27]:=CONVERTREAL(valor[27]);
     valores[28]:=valor[28];
     valores[29]:=valor[29];
     valores[30]:=fechaamericana(valor[30]);
     valores[31]:=valor[31];
     EjecutarInsert('MPEDIDOS',columnas,valores);

     devuelve_registro(archivo,'MPEDIDOS',registro);

    End;
   cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;

 procedure TModeloComunicacion.ProcesarFacturas(var tabla:tstrings) ;
 var
         columnas:array[0..11] of string;
         valores:array[0..11] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='serie';
     columnas[1]:='numero';
     columnas[2]:='fecha';
     columnas[3]:='cliente';
     columnas[4]:='delegacion';
     columnas[5]:='dependiente';
     columnas[6]:='observaciones';
     columnas[7]:='ano';
     columnas[8]:='importe';
     columnas[9]:='cobrado';
     columnas[10]:='enviado';
     columnas[11]:='fpago';



     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'FACTURAS',registro);
     tr:= cdatos.BeginTransaction;



    While registro.Text<>''  Do
    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     valores[2]:= fechaamericana(valor[2]);
     valores[3]:= valor[3];
     valores[4]:= valor[4];
     valores[5]:= valor[5];
     valores[6]:= valor[6];
     valores[7]:= valor[7];
     valores[8]:= convertreal(valor[8]);
     valores[9]:=CONVERTREAL(valor[9]);
     valores[10]:=valor[10];
     valores[11]:=valor[11];



     EjecutarInsert('Facturas',columnas,valores);

     devuelve_registro(archivo,'FACTURAS',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;

 procedure TModeloComunicacion.ProcesarAlbaranes(var tabla:tstrings) ;
 var
         columnas:array[0..11] of string;
         valores:array[0..11] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='serie';
     columnas[1]:='numero';
     columnas[2]:='fecha';
     columnas[3]:='cliente';
     columnas[4]:='delegacion';
     columnas[5]:='dependiente';
     columnas[6]:='observaciones';
     columnas[7]:='ano';
     columnas[8]:='importe';
     columnas[9]:='cobrado';
     columnas[10]:='enviado';
     columnas[11]:='fpago';



     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'ALBARANES',registro);
     tr:= cdatos.BeginTransaction;



    While registro.Text<>''  Do
    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     valores[2]:= fechaamericana(valor[2]);
     valores[3]:= valor[3];
     valores[4]:= valor[4];
     valores[5]:= valor[5];
     valores[6]:= valor[6];
     valores[7]:= valor[7];
     valores[8]:= convertreal(valor[8]);
     valores[9]:=CONVERTREAL(valor[9]);
     valores[10]:=valor[10];
     valores[11]:=valor[11];



     EjecutarInsert('Albaranes',columnas,valores);

     devuelve_registro(archivo,'ALBARANES',registro);

    End;
   cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;





procedure TModeloComunicacion.ProcesarDPedidos(var tabla:tstrings) ;
 var
         columnas:array[0..19] of string;
         valores:array[0..19] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin
     columnas[0]:='serie';
     columnas[1]:='numero';
     columnas[2]:='tipodocumento';
     columnas[3]:='linea';
     columnas[4]:='articulo';
     columnas[5]:='descripcion';
     columnas[6]:='cantidad';
     columnas[7]:='precio';
     columnas[8]:='importe';
     columnas[9]:='estado';
     columnas[10]:='pdto';
     columnas[11]:='ano';
      columnas[12]:='servidas';
     columnas[13]:='bultos';
     columnas[14]:='iva';
     columnas[15]:='pdtocab';
     //columnas[16]:='marca';
     columnas[16]:='literalbultos';
     columnas[17]:='unidades';
     columnas[18]:='enviado';
     columnas[19]:='marca';

     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'DPEDIDOS',registro);

    tr:= cdatos.BeginTransaction;

    While registro.Text<>'' Do

    Begin
     valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= strtoint(valor[1]);
     valores[2]:= valor[2];
     valores[3]:= valor[3];
     valores[4]:= valor[4];
     valores[5]:= valor[5];
     valores[6]:=  CONVERTREAL(valor[6]);
     valores[7]:=  CONVERTREAL(valor[7]);
     valores[8]:= CONVERTREAL(valor[8]);
     valores[9]:= valor[9];
     valores[10]:=CONVERTREAL(valor[10]);
     valores[11]:=valor[11];
     valores[12]:=valor[12];
     valores[13]:=valor[13];
     valores[14]:=CONVERTREAL(valor[14]);
     valores[15]:=CONVERTREAL(valor[15]);
     valores[16]:=valor[16];
     valores[17]:=valor[17];
     valores[18]:=valor[18];
     valores[19]:=valor[19];
     //valores[20]:=valor[20];

     EjecutarInsert('DPEDIDOS',columnas,valores);

     devuelve_registro(archivo,'DPEDIDOS',registro);


    End;


   cdatos.CommitFreeAndNil(tr);

    freeandnil(registro);
    freeandnil(valor);
end;
procedure TModeloComunicacion.ProcesarConfiguracion(var tabla:tstrings) ;
 var
         columnas:array[0..55] of string;
         valores:array[0..55] of variant;
         registro:tstrings;
         valor:tstrings;
         archivo:STRING;
         tr : TDBXTransaction;
     begin


  columnas[0]:='DEPENDIENTE';
  columnas[1]:='OBSERVACIONES';
  columnas[2]:='DELEGACIONES';
  columnas[3]:='FECHA_CARGA';
  columnas[4]:='IVAPRECIOS';
  columnas[5]:='EDITARCLIENTES';
  columnas[6]:='CREARCLIENTES';
  columnas[7]:='MANEJABULTOS';
  columnas[8]:='DTOLINEAS';
  columnas[9]:='DTOTOTALES';
  columnas[10]:='IVA1';
  columnas[11]:='IVA2';
  columnas[12]:='IVA3';
  columnas[13]:='REC1';
  columnas[14]:='REC2';
  columnas[15]:='REC3';
  columnas[16]:='IMPRC1';
  columnas[17]:='IMPRC2';
  columnas[18]:='IMPRC3';
  columnas[19]:='IMPRC4';
  columnas[20]:='IMPRC5';
  columnas[21]:='IMPRP1';
  columnas[22]:='IMPRP2';
  columnas[23]:='IMPRP3';
  columnas[24]:='DIGFAM';
  columnas[25]:='DIGSUBFAM';
  columnas[26]:='TAMCODART';
  columnas[27]:='NUMDECCAN';
  columnas[28]:='NUMDECTOT';
  columnas[29]:='NUMDECPRE';
  columnas[30]:='NUMDECPOR';
  columnas[31]:='URL';
  columnas[32]:='USUARIO';
  columnas[33]:='CLAVE';
  columnas[34]:='ENVIADO';
  columnas[35]:='PUERTOCOM';
  columnas[36]:='EMPRESA';
  columnas[37]:='URL2';
  columnas[38]:='CBARRAS';
  //columnas[39]:='SERIEencab';
  //columnas[40]:='SERIEendeta';
  columnas[39]:='IMPRESION';
  //columnas[42]:='VCAJA';
  columnas[40]:='LOGINARRANQUE';
  columnas[41]:='VERALMACENES';
  columnas[42]:='PERALMACENES';
  columnas[43]:='VERTARIFA1';
  columnas[44]:='VERTARIFA2';
  columnas[45]:='VERTARIFA3';
  columnas[46]:='VERTARIFA4';
  columnas[47]:='VERTARIFA5';
  columnas[48]:='VERTARIFA6';
  columnas[49]:='PCAMBIOTarifa';
  columnas[50]:='PCAMBIODEP';
  columnas[51]:='PCAMBIODTOL';
  columnas[52]:='PCAMBIODTOC';
  columnas[53]:='TIMEESPERAl';
  columnas[54]:='GESCOBALB';
  columnas[55]:='PERMITIRSINCO';

     Registro:=TStringList.Create;
     valor:=TStringList.Create;
     archivo:=TABLA.TEXT;
     devuelve_registro(archivo,'CONFIGURACION',registro);
     tr:= cdatos.BeginTransaction;

    While registro.Text<>'' Do
    Begin
      valor:=devuelve_valores(registro);
     valores[0]:= valor[0];
     valores[1]:= valor[1];
     valores[2]:= valor[2];
     valores[3]:= fechaamericana(valor[3]);
     valores[4]:= valor[4];
     valores[5]:= valor[5];
     valores[6]:= valor[6];
     valores[7]:= valor[7];
     valores[8]:= valor[8];
     valores[9]:= valor[9];
     valores[10]:= convertreal(valor[10]);
     valores[11]:= convertreal(valor[11]);
     valores[12]:= convertreal(valor[12]);
     valores[13]:= convertreal(valor[13]);
     valores[14]:= convertreal(valor[14]);
     valores[15]:= convertreal(valor[15]);
     valores[16]:=valor[16];
     valores[17]:=valor[17];
     valores[18]:=valor[18];
     valores[19]:=valor[19];
     valores[20]:=valor[20];
     valores[21]:=valor[21];
     valores[22]:=valor[22];
     valores[23]:=valor[23];
     valores[24]:=valor[24];
     valores[25]:=valor[25];
     valores[26]:=valor[26];
     valores[27]:=valor[27];
     valores[28]:=valor[28];
     valores[29]:= valor[29];
     valores[30]:= valor[30];
     valores[31]:= valor[31];
     valores[32]:= valor[32];
     valores[33]:= valor[33];
     valores[34]:= valor[34];
     valores[35]:= valor[35];
     valores[36]:= valor[36];
     valores[37]:= valor[37];
     valores[38]:= valor[38];
     //valores[39]:=valor[39];
     //valores[40]:=valor[40];
     valores[39]:=valor[39];
     //valores[42]:=valor[42];
     valores[40]:=valor[40];
     valores[41]:=valor[41];
     valores[42]:=valor[42];
     valores[43]:=valor[43];
     valores[44]:=valor[44];
     valores[45]:=valor[45];
     valores[46]:=valor[46];
     valores[47]:=valor[47];
     valores[48]:=valor[48];
     valores[49]:=valor[49];
     valores[50]:= valor[50];
     valores[51]:= valor[51];
     valores[52]:= valor[52];
     valores[53]:=valor[53];
     valores[54]:=valor[54];
     valores[55]:=valor[55];
     EjecutarInsert('Configuracion',columnas,valores);

     devuelve_registro(archivo,'CONFIGURACION',registro);

    End;
    cdatos.CommitFreeAndNil(tr);
    freeandnil(registro);
    freeandnil(valor);
end;
procedure TModeloComunicacion.escribircontadores();

var
  query:TSQLQuery;
  xml:tstrings;

    begin




    xml:=tstringlist.Create;
    xml.add ('<?xml version="1.0" standalone="yes"?>');
    xml.Add('<NewDataSet>');

    Ejecutarinstruccion('select * from contadores');
    query:=self.DevolverBD;
    query.First;

    While not query.eof Do
    Begin




    xml.Add('<contadores>');
    xml.add('<serie>'+query.FieldByName('serie').AsString+'</serie>');
    xml.add('<actual>'+query.FieldByName('actual').AsString+'</actual>');
    xml.add('<tipoDoc>'+query.FieldByName('tipoDoc').AsString+'</tipoDoc>');
    xml.add('<contadorIni>'+query.FieldByName('contadorini').AsString+'</contadorIni>');
    xml.add('<contadorFin>'+query.FieldByName('contadorfin').AsString+'</contadorFin>');
    xml.add('<ano>'+query.FieldByName('ano').AsString+'</ano>');
    xml.add('<enviado>'+query.FieldByName('enviado').AsString+'</enviado>');
    xml.add('<almacen>'+query.FieldByName('almacen').AsString+'</almacen>');
    xml.add('<iva>'+query.FieldByName('iva').AsString+'</iva>');

    xml.Add('</contadores>');

    query.next;
    End;

    xml.Add('</NewDataSet>');
    xml.SaveToFile(TPath.GetPublicPath+'/temporal/'+'contadores.xml');
    freeandnil(xml);




end;






procedure TModeloComunicacion.escribircaja(id_pedido:integer);

var
  query:TSQLQuery;
  xml:tstrings;

    begin




    xml:=tstringlist.Create;
    xml.add ('<?xml version="1.0" standalone="yes"?>');
    xml.Add('<NewDataSet>');
    if(id_pedido = 0) then
        begin
        Ejecutarinstruccion('select * from caja');
        query:=self.DevolverBD;
        query.First;

        While not query.eof Do
        Begin




        xml.Add('<caja>');
        xml.add('<fecha>'+FormatDateTime('dd/mm/yyyy',query.FieldByName('fecha').AsDateTime)+'</fecha>');
        xml.add('<tipoC>'+query.FieldByName('tipoC').AsString+'</tipoC>');
        xml.add('<serie>'+query.FieldByName('serie').AsString+'</serie>');
        xml.add('<numero>'+query.FieldByName('numero').AsString+'</numero>');
        xml.add('<importe>'+numeroeuropeo(query.FieldByName('importe').AsString)+'</importe>');
        xml.add('<dependiente>'+query.FieldByName('dependiente').AsString+'</dependiente>');
        xml.add('<ano>'+query.FieldByName('ano').AsString+'</ano>');
        xml.add('<comentario>'+FormatoASCIISaltosImpresora(query.FieldByName('comentarios').AsString)+'</comentario>');
        xml.add('<cliente>'+query.FieldByName('cliente').AsString+'</cliente>');
        xml.add('<cobrado>'+numeroeuropeo(query.FieldByName('cobrado').AsString)+'</cobrado>');
        xml.add('<marca>'+query.FieldByName('marca').AsString+'</marca>');
        //xml.add('<enviado>'+query.FieldByName('enviado').AsString+'</enviado>');
        xml.add('<enviado></enviado>');
        xml.Add('</caja>');

        query.next;
        End;
    end;

    xml.Add('</NewDataSet>');
    xml.SaveToFile(TPath.GetPublicPath+'/temporal/'+'caja.xml');
    freeandnil(xml);




end;



 procedure TModeloComunicacion.escribirDPedidos(idped:integer);

var
  query:TSQLQuery;
  xml:tstrings;
  query1: TSQLQuery;
  modelopedido:TModeloPedido;

    begin




    xml:=tstringlist.Create;
    xml.add ('<?xml version="1.0" standalone="yes"?>');
    xml.Add('<NewDataSet>');

    if(idped = 0) then
    begin
      Ejecutarinstruccion('select * from dpedidos where marca="N" and procesada <> 1')
    end
    else
    begin
       modelopedido := TModeloPedido.Create;
       query1:=modelopedido.obtenerSerie(idped);
       Ejecutarinstruccion('select * from dpedidos where marca="N" and procesada <> 1 and  numero = '+query1.fieldbyname('numero').AsString+' and ano ='+copy(query1.FieldByName('fecha').AsString,9,10)+' and serie="'+query1.fieldbyname('serie').AsString+'"');
       modelopedido.liberarsql;
    end;

    query:=self.DevolverBD;
    query.First;

    While not query.eof Do
    Begin

    xml.Add('<dpedidos>');
    xml.add('<serie>'+query.FieldByName('serie').AsString+'</serie>');
    xml.add('<numero>'+query.FieldByName('numero').AsString+'</numero>');
    xml.add('<tipoDocumento>'+query.FieldByName('tipoDocumento').AsString+'</tipoDocumento>');
    xml.add('<linea>'+query.FieldByName('linea').AsString+'</linea>');
    xml.add('<articulo>'+query.FieldByName('articulo').AsString+'</articulo>');
    xml.add('<descripcion>'+FormatoASCIISaltosImpresora(query.FieldByName('descripcion').AsString)+'</descripcion>');
    xml.add('<cantidad>'+query.FieldByName('cantidad').AsString+'</cantidad>');
    xml.add('<precio>'+query.FieldByName('precio').AsString+'</precio>');
    xml.add('<importe>'+query.FieldByName('importe').AsString+'</importe>');
    xml.add('<estado>'+query.FieldByName('estado').AsString+'</estado>');
    xml.add('<pdto>'+query.FieldByName('pdto').AsString+'</pdto>');
    xml.add('<ano>'+query.FieldByName('ano').AsString+'</ano>');
    xml.add('<Servidas>'+query.FieldByName('servidas').AsString+'</Servidas>');
    xml.add('<bultos>'+query.FieldByName('bultos').AsString+'</bultos>');
    xml.add('<iva>'+query.FieldByName('iva').AsString+'</iva>');
    xml.add('<pdtoCab>'+numeroeuropeo(query.FieldByName('pdtoCab').AsString)+'</pdtoCab>');
    xml.add('<marca>'+query.FieldByName('marca').AsString+'</marca>');
    xml.add('<literalBultos>'+query.FieldByName('literalBultos').AsString+'</literalBultos>');
    xml.add('<unidades>'+query.FieldByName('unidades').AsString+'</unidades>');

    xml.Add('</dpedidos>');

    query.next;
    End;

    xml.Add('</NewDataSet>');
    xml.SaveToFile(TPath.GetPublicPath+'/temporal/'+'dpedidos.xml');
    freeandnil(xml);

end;



procedure TModeloComunicacion.escribirMPedidos(idped:integer);

var
  query:TSQLQuery;
  xml:tstrings;

    begin




    xml:=tstringlist.Create;
    xml.add ('<?xml version="1.0" standalone="yes"?>');
    xml.Add('<NewDataSet>');
    if(idped = 0) then
      Ejecutarinstruccion('select * from mpedidos where marca="N" and procesada <> 1')
    else
      Ejecutarinstruccion('select * from mpedidos where marca="N" and procesada <> 1 and id='+idped.ToString());

    query:=self.DevolverBD;
    query.First;

    While not query.eof Do
    Begin

    xml.Add('<mpedidos>');
    xml.add('<serie>'+query.FieldByName('serie').AsString+'</serie>');
    xml.add('<numero>'+query.FieldByName('numero').AsString+'</numero>');
    xml.add('<tipoDocumento>'+query.FieldByName('tipoDocumento').AsString+'</tipoDocumento>');
    xml.add('<fecha>'+FormatDateTime('dd/mm/yyyy',query.FieldByName('fecha').AsDateTime)+'</fecha>');
    xml.add('<cliente>'+query.FieldByName('cliente').AsString+'</cliente>');
    xml.add('<delegacion>'+query.FieldByName('delegacion').AsString+'</delegacion>');
    xml.add('<dependiente>'+query.FieldByName('dependiente').AsString+'</dependiente>');
    xml.add('<observaciones>'+FormatoASCIISaltosImpresora(query.FieldByName('observaciones').AsString)+'</observaciones>');
    xml.add('<base1>'+query.FieldByName('base1').AsString+'</base1>');
    xml.add('<base2>'+query.FieldByName('base2').AsString+'</base2>');
    xml.add('<base3>'+query.FieldByName('base3').AsString+'</base3>');
    xml.add('<rec1>'+query.FieldByName('rec1').AsString+'</rec1>');
    xml.add('<rec2>'+query.FieldByName('rec2').AsString+'</rec2>');
    xml.add('<rec3>'+query.FieldByName('rec3').AsString+'</rec3>');
    xml.add('<total>'+query.FieldByName('total').AsString+'</total>');
    xml.add('<bruto>'+query.FieldByName('bruto').AsString+'</bruto>');
    //xml.add('<cobrado>'+query.FieldByName('cobrado').AsString+'</cobrado>');
    xml.add('<p_descuento>'+query.FieldByName('p_descuento').AsString+'</p_descuento>');
    xml.add('<estado>'+query.FieldByName('estado').AsString+'</estado>');
    xml.add('<c_descuento>'+query.FieldByName('c_descuento').AsString+'</c_descuento>');
    xml.add('<iva1>'+query.FieldByName('iva1').AsString+'</iva1>');
    xml.add('<iva2>'+query.FieldByName('iva2').AsString+'</iva2>');
    xml.add('<iva3>'+query.FieldByName('iva3').AsString+'</iva3>');
    xml.add('<impIVA1>'+query.FieldByName('impIVA1').AsString+'</impIVA1>');
    xml.add('<impIVA2>'+query.FieldByName('impIVA2').AsString+'</impIVA2>');
    xml.add('<impIVA3>'+query.FieldByName('impIVA3').AsString+'</impIVA3>');
    xml.add('<impREC1>'+query.FieldByName('impREC1').AsString+'</impREC1>');
    xml.add('<impREC2>'+query.FieldByName('impREC2').AsString+'</impREC2>');
    xml.add('<impREC3>'+query.FieldByName('impREC3').AsString+'</impREC3>');
    xml.add('<marca>'+query.FieldByName('marca').AsString+'</marca>');
    xml.add('<enviado>'+query.FieldByName('enviado').AsString+'</enviado>');
    xml.add('<fentrega>'+FormatDateTime('dd/mm/yyyy',query.FieldByName('fentrega').AsDateTime)+'</fentrega>');
    xml.add('<hora>'+query.FieldByName('hora').AsString+'</hora>');

    xml.Add('</mpedidos>');

    query.next;
    End;

    xml.Add('</NewDataSet>');
    xml.SaveToFile(TPath.GetPublicPath+'/temporal/'+'mpedidos.xml');
    freeandnil(xml);




end;






end.
