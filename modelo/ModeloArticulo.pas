unit ModeloArticulo;

interface

   {$ZEROBASEDSTRINGS ON}

uses Modelo, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,FMX.Dialogs, System.SysUtils,Data.FMTBcd;


type
  TModeloArticulo = class(TModelo) // Tambi�n puede ser solo class si derivas directamente de TObject
  private
    var
  protected
    { Declaraciones protegidas de la clase }
  public
    function ListarArticulos( filtro:TDictionary<String,String>;orden:TDictionary<String,String>):TSQLQuery;
    function ListarArticulosHisPed( filtro:TDictionary<String,String>;orden:TDictionary<String,String>;cliente:integer):TSQLQuery;
    function ListarLotes(codigo:string; filtro:TDictionary<String,String>;orden:tdictionary<string,string>):TSQLQuery;
    function ConsultarArticulo( codigo: string ): TSQLQuery;
    function ControlLotes( codigo: string ): boolean;
    function SumatorioLotes( codigo: string ): string;
    function existe(codigo:string):boolean;
    function ConsultarArticulosCompletable(codigo: string ): TSQLQuery;
    function Obtenerdescuentos(codigoart,codigotabclt,familia,subfamilia: string ): TSQLQuery;

  published
    { Declaraciones publicadas de la clase }
  end;


implementation

 function TModeloArticulo.ListarArticulos( filtro:TDictionary<String,String>;orden:TDictionary<String,String>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;


 begin
 //Utiliza ITERADORESSSSSSSSS for(i=0;i<filtro.Count;i++)
 //C�digo,Nombre'+',replace(cast(Tarifa1 as text),".",",") as Tarifa1'+', replace(cast(Tarifa2 as text),".",",") as Tarifa2,'+'replace(cast(Tarifa3 as text),".",",") as Tarifa3,replace(cast(Tarifa4 as text),".",",") as Tarifa4,replace(cast(Tarifa5 as text),".",",") as Tarifa5,replace(cast(Tarifa6 as text),".",",") as Tarifa6'+', replace(cast(Stock as text),".",",") as Stock
    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

 if(filtro.Count = 0) then
    begin
      //showmessage('no tiene elementos');
      EjecutarInstruccion('select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock  from v_articulos order by  v_articulos.'+orden.items['column']+' '+orden.items['tipo'] );
    end
 else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if (caso <> 0) and (caso <> 1) then
        begin
        filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
        end;


      case caso of
      0: EjecutarInstruccion(' select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock from v_articulos where UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '") order by v_articulos.'+orden.items['column']+' '+orden.items['tipo']);
      1: EjecutarInstruccion('select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock from v_articulos where '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%" order by v_articulos.'+orden.items['column']+' '+orden.items['tipo'] );
      2: EjecutarInstruccion('select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock  from v_articulos where cast( replace('+ filtro.items['col'] +',",",".") as numeric) = '+ filtro.items['texto'] +' order by  v_articulos.'+orden.items['column']+' '+orden.items['tipo']);
      3: EjecutarInstruccion('select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock  from v_articulos where cast( replace('+ filtro.items['col'] +',",",".") as numeric) < '+ filtro.items['texto'] +' order by  v_articulos.'+orden.items['column']+' '+orden.items['tipo']);
      4: EjecutarInstruccion('select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock  from v_articulos where cast( replace('+ filtro.items['col'] +',",",".") as numeric) > '+ filtro.items['texto'] +' order by  v_articulos.'+orden.items['column']+' '+orden.items['tipo']);
      5: EjecutarInstruccion('select C�digo,Nombre,Tarifa1,Tarifa2,Tarifa3,Tarifa4,Tarifa5,Tarifa6,Stock  from v_articulos where cast( replace('+ filtro.items['col'] +',",",".") as numeric) <> '+ filtro.items['texto'] +' order by v_articulos.'+orden.items['column']+' '+orden.items['tipo']);
      end;

    end;

  //showmessage(inttostr(DevolverBD().RecordCount)+ 'primero');
 Result:=Self.DevolverBD();
end;

function TModeloArticulo.ListarArticulosHisPed( filtro:TDictionary<String,String>;orden:TDictionary<String,String>;cliente:integer):TSQLQuery;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;


 begin
 //Utiliza ITERADORESSSSSSSSS for(i=0;i<filtro.Count;i++)

    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

 if(filtro.Count = 0) then
    begin
      //showmessage('no tiene elementos');
      EjecutarInstruccion('select * from v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+' order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo'] );
    end
 else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if (caso <> 0) and (caso <> 1) then
        begin
        filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
        end;


      case caso of
      0: EjecutarInstruccion(' select * from v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+'  and UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '") order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo'] );
      1: EjecutarInstruccion('select  *  from  v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+' and '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%" order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo'] );
      2: EjecutarInstruccion('select  *  from v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+' and cast( replace('+ filtro.items['col'] +',",",".") as numeric) = '+ filtro.items['texto'] +' order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo']);
      3: EjecutarInstruccion('select  *  from v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+' where and cast( replace('+ filtro.items['col'] +',",",".") as numeric) < '+ filtro.items['texto'] +' order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo']);
      4: EjecutarInstruccion('select  *  from v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+' where and cast( replace('+ filtro.items['col'] +',",",".") as numeric) > '+ filtro.items['texto'] +' order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo']);
      5: EjecutarInstruccion('select  *  from v_articulosHisPed where v_articulosHisPed.cliente= '+inttostr(cliente)+' where and cast( replace('+ filtro.items['col'] +',",",".") as numeric) <> '+ filtro.items['texto'] +' order by  v_articulosHisPed.'+orden.items['column']+' '+orden.items['tipo']);
      end;

    end;

  //showmessage(inttostr(DevolverBD().RecordCount)+ 'primero');
 Result:=Self.DevolverBD();
end;




function TModeloArticulo.ConsultarArticulo( codigo: string ): TSQLQuery;

begin
      EjecutarInstruccion('select * from articulos where codigo = "'+codigo+'"');
      Result:=Self.DevolverBD();
end;

function  TModeloArticulo.Obtenerdescuentos(codigoart,codigotabclt,familia,subfamilia: string ): TSQLQuery;
var   Query: TSQLQuery;
begin
        if (familia <> '') and (subfamilia<>'') then
        begin
           EjecutarInstruccion('select precio,dto from ddescuentos  where cast(fam_ini as integer)<>0 and cast(fam_fin as integer)<>0 and  cast(sub_ini as integer)<>0 and cast(sub_fin as integer)<>0 and CodigoM="'+codigotabclt+'" and cast(fam_ini as integer)>='+familia+' and cast(fam_fin as integer)<='+familia+' and  cast(sub_ini as integer)>='+subfamilia+' and cast(sub_fin as integer)<='+subfamilia);
           Query:=Self.DevolverBD();
           if query.RecordCount<>0 then
           begin
            result:=query;
           end
           else
           begin
            EjecutarInstruccion('select precio,dto from ddescuentos  where  CodigoM="'+codigotabclt+'" and cod_ini ="'+codigoart+'"');
            Query:=Self.DevolverBD();
            result:=query;
           end;

        end
        else
        begin
          EjecutarInstruccion('select precio,dto from ddescuentos  where  CodigoM="'+codigotabclt+'" and cod_ini ="'+codigoart+'"');
          Query:=Self.DevolverBD();
          result:=query;
        end;
         {
        EjecutarInstruccion('select serie from articulos where codigo = "'+codigo+'"');
        Query:=Self.DevolverBD();
        Query.First;
        if(UPPERCASE(Query.FieldByName('serie').AsString) = 'S') then
            begin
            Result:=true;
            end
        else
            Result:= false;
          }




end;

function tmodeloarticulo.ControlLotes( codigo: string ): boolean;
    var   Query: TSQLQuery;
  begin
        EjecutarInstruccion('select serie from articulos where codigo = "'+codigo+'"');
        Query:=Self.DevolverBD();
        Query.First;
        if(UPPERCASE(Query.FieldByName('serie').AsString) = 'S') then
            begin
            Result:=true;
            end
        else
            Result:= false;

  end;
  function TModeloArticulo.SumatorioLotes( codigo: string ): string;
  var   Query: TSQLQuery;
  begin

        EjecutarInstruccion('select sum(cantidad) as stock from lotes where articulo = "'+codigo+'"');
        Query:=Self.DevolverBD();
        Query.First;
        result:= Query.FieldByName('stock').AsString;

  end;

 function TModeloArticulo.ListarLotes(codigo: string; filtro:TDictionary<String,String>;orden:tdictionary<string,string>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;


 begin
 //Utiliza ITERADORESSSSSSSSS for(i=0;i<filtro.Count;i++)

    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

 if(filtro.Count = 0) then
    begin
      //showmessage('no tiene elementos');
      EjecutarInstruccion('select * from v_lotesarticulos where Articulo = "'+codigo+'"'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
    end
 else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if filtro.Items['fecha']<>'S' then
      begin

            if (caso <> 0) and (caso <> 1) then
              begin
              filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
              end;


            case caso of
            0: EjecutarInstruccion('select  *  from v_lotesarticulos  where Articulo = "'+codigo+'" and UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '")'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            1: EjecutarInstruccion('select  * from v_lotesarticulos where Articulo = "'+codigo+'" and '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%"'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            2: EjecutarInstruccion('select  *  from v_lotesarticulos where Articulo = "'+codigo+'" and cast( replace('+ filtro.items['col'] +',",",".") as numeric) = '+ filtro.items['texto']+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            3: EjecutarInstruccion('select  * from v_lotesarticulos where Articulo = "'+codigo+'" and cast( replace('+ filtro.items['col'] +',",",".") as numeric) < '+ filtro.items['texto']+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            4: EjecutarInstruccion('select   * from v_lotesarticulos where Articulo = "'+codigo+'" and cast( replace('+ filtro.items['col'] +',",",".") as numeric) > '+ filtro.items['texto']+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            5: EjecutarInstruccion('select   * from v_lotesarticulos where Articulo = "'+codigo+'" and cast( replace('+ filtro.items['col'] +',",",".") as numeric) <> '+ filtro.items['texto']+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            end;
      end
      else
      begin
            case caso of

            2: EjecutarInstruccion('select  *  from v_lotesarticulos where Articulo = "'+codigo+'" and '+ filtro.items['col'] +' = "'+ filtro.items['texto']+'"'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            3: EjecutarInstruccion('select  *  from v_lotesarticulos where Articulo = "'+codigo+'" and  '+ filtro.items['col'] +' < "'+ filtro.items['texto']+'"'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            4: EjecutarInstruccion('select  *  from v_lotesarticulos where Articulo = "'+codigo+'" and  '+ filtro.items['col'] +' > "'+ filtro.items['texto']+'"'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            5: EjecutarInstruccion('select  * from v_lotesarticulos where Articulo = "'+codigo+'" and  '+ filtro.items['col'] +' <> "'+ filtro.items['texto']+'"'+ ' order by v_lotesarticulos.'+orden.items['column']+' '+orden.items['tipo']);
            end;
      end;

    end;

  //showmessage(inttostr(DevolverBD().RecordCount)+ 'primero');
 Result:=Self.DevolverBD();
end;
function TModeloArticulo.existe(codigo: string):boolean;
var query:TSQLQuery;
begin
  EjecutarInstruccion('select * from articulos where codigo = "'+codigo+'"');
  query:=Self.DevolverBD();
  if(query.RecordCount=0)then
  begin
    result:=false;
  end
  else
    result:=true;


end;

function TModeloArticulo.ConsultarArticulosCompletable(codigo: string ): TSQLQuery;
var query:TSQLQuery;
    barra:string;
begin

   EjecutarInstruccion('select Cbarras from configuracion');
   query:=Self.DevolverBD();
   query.First;
   barra:=query.FieldByName('Cbarras').AsString;

    EjecutarInstruccion('select * from articulos where codigo = "'+codigo+'"');
    query:=Self.DevolverBD();

    if(query.RecordCount=0)then
    begin


      if UPPERCASE(barra)='S' then
      begin

            EjecutarInstruccion('select codigo from cbarras where barra = "'+codigo+'"');
            query:=self.DevolverBD;

            if(query.RecordCount<>0)then
                begin
                query.First;
                EjecutarInstruccion('select * from articulos where codigo = "'+query.FieldByName('codigo').AsString+'"');
                query:=self.DevolverBD;
                end;
      end;
    end;

    result:=query;


end;



end.
