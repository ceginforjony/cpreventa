unit ModeloAlbaran;

interface

   {$ZEROBASEDSTRINGS ON}

  uses Modelo, Generics.Collections,Data.DbxSqlite, Data.DB, Data.SqlExpr,FMX.Dialogs, System.SysUtils,funciones,System.Variants,Data.FMTBcd;


type
  TModeloAlbaran = class(TModelo) // Tambi�n puede ser solo class si derivas directamente de TObject
  private
  //function ObtenerUltimaLineaAlb(serie:string;numero:integer;ano:integer):integer;
  {}
  protected
    { Declaraciones protegidas de la clase }
  public
    function ListarAlbaranesCliente(codigoClt:string; filtro:TDictionary<String,String>;orden: TDictionary<String,String> ):TSQLQuery;
    function ListarAlbaranes(filtro:TDictionary<String,String>;orden: TDictionary<String,String> ):TSQLQuery;
    function obtenerconfiguracionAlb():TSQLQuery;
    function  ObtenerDatosAlb(idalb:integer):TSQLQuery;
    procedure actualizarcobro(idalb:integer;datoscobro:tdictionary<string,string>);
    procedure addAlblineacaja(idalb:integer;datoscobro:tdictionary<string,string>);

  published
    { Declaraciones publicadas de la clase }
  end;






implementation

 function TModeloAlbaran.ListarAlbaranesCliente(codigoClt:string; filtro:TDictionary<String,String>;orden: TDictionary<String,String>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;


 begin

    //  id,Serie,N�mero,strftime("%d/%m/%Y",Fecha)as Fecha,replace(cast( Total as text),".","," ) as Total,replace(cast(Cobrado as text),".","," )as Cobrado
    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

 if(filtro.Count = 0) then
    begin

      EjecutarInstruccion('select id,Serie,N�mero,Fecha, Total, Cobrado  from v_albaranes where  C�dCliente =' +codigoclt + ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
    end
 else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if filtro.Items['fecha']<>'S' then
      begin
          if (caso <> 0) and (caso <> 1) then
            begin
            filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
            end;


          case caso of
          0: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where C�dCliente =' +codigoclt+' and   UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '")'+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          1: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where C�dCliente =' +codigoclt+' and  '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%"'+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          2: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where C�dCliente =' +codigoclt+' and   cast('+ filtro.items['col'] +' as numeric) = '+ filtro.items['texto']+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          3: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where C�dCliente =' +codigoclt+' and    '+ filtro.items['col'] +' < '+ filtro.items['texto']+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          4: EjecutarInstruccion('select id,Serie,N�mero,Fecha, Total, Cobrado   from v_albaranes where C�dCliente =' +codigoclt+' and   '+ filtro.items['col'] +' > '+ filtro.items['texto']+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          5: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where C�dCliente =' +codigoclt+' and  cast('+ filtro.items['col'] +' as numeric) <> '+ filtro.items['texto']+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          end;

      end
      else
      begin
           case caso of

            2: EjecutarInstruccion('select id,Serie,N�mero, Fecha,Total, Cobrado   from v_albaranes where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' = "'+ filtro.items['texto']+'"'+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            3: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' < "'+ filtro.items['texto']+'"'+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            4: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' > "'+ filtro.items['texto']+'"'+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            5: EjecutarInstruccion('select id,Serie,N�mero, Fecha, Total, Cobrado   from v_albaranes where  C�dCliente =' +codigoclt+' and '+ filtro.items['col'] +' <> "'+ filtro.items['texto']+'"'+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            end;
      end;
    end;

  //showmessage(inttostr(DevolverBD().RecordCount)+ 'primero');
 Result:=Self.DevolverBD();
end;
function TModeloAlbaran.ListarAlbaranes(filtro:TDictionary<String,String>;orden: TDictionary<String,String>):TSQLQuery ;
 var caso: integer;
     diccionario: tdictionary<string, Integer>;
      rut1,rut:string;


 begin
 //Utiliza ITERADORESSSSSSSSS for(i=0;i<filtro.Count;i++)

     if ruta<>'-1' then
      begin
        rut1:=' where ruta="'+ruta+'" ';
        rut:=' ruta="'+ruta+'" ';
      end
     else
      begin
        rut:='';
        rut1:='';

      end;



    diccionario:=tdictionary<string,integer>.create;
    diccionario.Add('Es igual a',0);
    diccionario.Add('Contenido en',1);
    diccionario.Add('Igual',2);
    diccionario.Add('Menor que',3);
    diccionario.Add('Mayor que',4);
    diccionario.Add('Diferente de',5);

    if(filtro.Count = 0) then
    begin
      //showmessage('no tiene elementos');
      EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente,Total, Cobrado  from v_albaranes '+ rut1 +' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
    end
    else
    begin

      caso:=diccionario.Items[filtro.Items['tipo']];
      if filtro.Items['fecha']<>'S' then
      begin
          if (caso <> 0) and (caso <> 1) then
            begin
            filtro.Items['texto']:= stringreplace(filtro.Items['texto'],',','.',[rfReplaceAll]);
            end;


          case caso of
          0: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente, Total,Cobrado   from v_albaranes where    UPPER("'+ filtro.items['col'] +'") = UPPER("'+filtro.items['texto']+ '")'+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          1: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente,Total, Cobrado   from v_albaranes where '+ filtro.Items['col'] + ' like "%'+filtro.items['texto']+'%"'+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          2: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente,Total,Cobrado   from v_albaranes where    cast('+ filtro.items['col'] +' as numeric) = '+ filtro.items['texto']+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          3: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente, Total, Cobrado   from v_albaranes where     '+ filtro.items['col'] +' < '+ filtro.items['texto']+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          4: EjecutarInstruccion('select id,Serie,N�mero,Fecha,C�dCliente,NomCliente, Total, Cobrado   from v_albaranes where    '+ filtro.items['col'] +' > '+ filtro.items['texto']+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          5: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente, Total, Cobrado   from v_albaranes where   cast('+ filtro.items['col'] +' as numeric) <> '+ filtro.items['texto']+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
          end;

      end
      else
      begin
           case caso of

            2: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente,Total,Cobrado   from v_albaranes where  '+ filtro.items['col'] +' = "'+ filtro.items['texto']+'"'+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            3: EjecutarInstruccion('select id,Serie,N�mero,Fecha,C�dCliente,NomCliente, Total, Cobrado   from v_albaranes where  '+ filtro.items['col'] +' < "'+ filtro.items['texto']+'"'+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            4: EjecutarInstruccion('select id,Serie,N�mero,Fecha,C�dCliente,NomCliente, Total, Cobrado   from v_albaranes where '+ filtro.items['col'] +' > "'+ filtro.items['texto']+'"'+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            5: EjecutarInstruccion('select id,Serie,N�mero, Fecha,C�dCliente,NomCliente, Total, Cobrado   from v_albaranes where '+ filtro.items['col'] +' <> "'+ filtro.items['texto']+'"'+rut+ ' order by v_albaranes.'+orden.items['column']+' '+orden.items['tipo']  );
            end;
      end;
    end;


    Result:=Self.DevolverBD();
end;

function TModeloAlbaran.obtenerconfiguracionAlb():TSQLQuery;
begin
    EjecutarInstruccion('select * from configuracion');
    Result:=Self.DevolverBD();
end;

function tmodeloalbaran.ObtenerDatosAlb(idalb:integer):TSQLQuery;
begin
    EjecutarInstruccion('select numero,serie,fecha,cliente,importe as total,cobrado,delegacion,dependiente,observaciones,ano,enviado from albaranes where id='+inttostr(idalb));
    result:=Self.DevolverBD();
end;

procedure tmodeloalbaran.actualizarcobro(idalb:integer;datoscobro:tdictionary<string,string>);
var cobro:string;
          cobrado:double;
          cobrar:string;
          queryconf:TSQLQuery;
          DecTotal:integer;
begin
    queryconf:=obtenerconfiguracionAlb();
    queryconf.first;
    DecTotal:=queryconf.fieldbyname('NumDecTot').asinteger;
    cobro:=stringreplace(datoscobro.Items['cobro'],'.',',',[rfReplaceAll]);
    ejecutarinstruccion('select *  from Albaranes where id = '+inttostr(idalb));
    cobrado:=redondear(Devolverbd().FieldByName('cobrado').AsFloat+strtofloat(cobro),DecTotal);
    cobrar:=formatearnumero(DecTotal,floattostr(cobrado));
    cobrar:=stringreplace(cobrar,',','.',[rfReplaceAll]);
    Ejecutarmodificacion('update Albaranes set cobrado =   '+cobrar+' where id = '+inttostr(idalb));



    //cobro:=stringreplace(datoscobro.Items['cobro'],',','.',[rfReplaceAll]);
    //Ejecutarmodificacion('update Albaranes set cobrado = cobrado + '+cobro+' where id = '+inttostr(idalb));
end;

procedure tmodeloalbaran.addAlblineacaja(idalb:integer;datoscobro:tdictionary<string,string>);
var query: TSQLQuery;
    columnas:array[0..11] of string;
    valores:array[0..11] of variant;
begin
     columnas[0]:='numero';
     columnas[1]:='serie';
     columnas[2]:='fecha';
     columnas[3]:='ano';
     columnas[4]:='cliente';
     columnas[5]:='dependiente';
     columnas[6]:='importe';
     columnas[7]:='cobrado';
     columnas[8]:='marca';
     columnas[9]:='tipoC';
     columnas[10]:='Enviado';
     columnas[11]:='Comentarios';

     query:=obtenerdatosalb(idalb);
     query.First;
     valores[0]:= query.FieldByName('numero').asinteger;
     valores[1]:= query.FieldByName('serie').asstring;
     valores[2]:= formatdatetime('YYYY-MM-DD',DATE);
     valores[3]:= anoamericano(formatdatetime('YYYY-MM-DD',query.FieldByName('fecha').AsDateTime)) ;
     valores[4]:= query.FieldByName('cliente').asstring;


     valores[6]:= query.FieldByName('total').asfloat;

     valores[7]:=strtofloat(datoscobro.items['cobro']);
     valores[8]:='N';
     valores[9]:=-3;
     valores[10]:='NULL';
     valores[11]:=datoscobro.items['comentarios'];




      EjecutarInstruccion('select dependiente from configuracion');
      valores[5]:=self.DevolverBD().FieldByName('dependiente').AsString;
      EjecutarInsert('Caja',columnas,valores);

end;


end.
