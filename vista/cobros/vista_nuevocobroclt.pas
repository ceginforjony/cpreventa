unit vista_nuevocobroclt;

interface
      {$ZEROBASEDSTRINGS ON}
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls,carga_inicial, FMX.ListBox,Generics.Collections, FMX.Layouts,funciones,
  FMX.Memo,cliente,albaran,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt,caja,controlador,factura, FMX.Controls.Presentation,Data.FMTBcd;


type
  TFormnuevocobroclt = class(TForm)
    ToolBar1: TToolBar;
    Label1: TLabel;
    botoncancelar: TButton;
    carga: TAniIndicator;
    ScrollBoxcobro: TScrollBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Label2: TLabel;
    Editclt: TEdit;
    Panel3: TPanel;
    Label3: TLabel;
    ComboBoxTipo: TComboBox;
    Panel4: TPanel;
    editnombre: TEdit;
    Panel6: TPanel;
    botonAceptar: TButton;
    Panelselec: TPanel;
    Label5: TLabel;
    ComboBoxselec: TComboBox;
    ListBoxItem1: TListBoxItem;
    Panelopcion: TPanel;
    Label6: TLabel;
    ComboBoxopcion: TComboBox;
    Panelvalor: TPanel;
    Label4: TLabel;
    Editvalor: TEdit;
    Panelobser: TPanel;
    Label7: TLabel;
    Editobser: TEdit;
    Panelpendiente: TPanel;
    Label8: TLabel;
    Editpendiente: TEdit;
     procedure botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
      procedure ComboBoxTipoChange(Sender: TObject);
    procedure ComboBoxselecChange(Sender: TObject);
    procedure botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EditvalorChange(Sender: TObject);
  private
    control:tcontrolador;
  cliente:tcliente;
  albaran:talbaran;
  factura:tfactura;
  caja:tcaja;
  configuracion:tdictionary<string,variant>;
  querytipo:TSQLQuery;
  tipoid:tdictionary<integer,string>;
  tiponombre:tdictionary<string,integer>;
   opcionid:tdictionary<integer,string>;
   opcionnombre:tdictionary<string,integer>;
  queryalb:TSQLQuery;
  queryfac:TSQLQuery;
  claveidalb:tdictionary<string,integer>;
  //clavemarcaalb:tdictionary<string,string>;
  clavependientealb:tdictionary<string,string>;
  claveidfac:tdictionary<string,integer>;
  //clavemarcafac:tdictionary<string,string>;
  clavependientefac:tdictionary<string,string>;

  abortarchange:boolean;
  cerrar:boolean;
    { Private declarations }
  public
   codclt:string { Public declarations }
  end;

var
  Formnuevocobroclt: TFormnuevocobroclt;

implementation
uses vista_cobrosclt;
{$R *.fmx}


procedure TFormnuevocobroclt.botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
var datoscobro:tdictionary<string,variant>;
begin
datoscobro:=tdictionary<string,variant>.create;

if comboboxtipo.Selected=nil  then
begin
 Alerta(self,'No se ha seleccionado el tipo del cobro');
 exit;
end;


if tiponombre.Items[comboboxtipo.Selected.Text]=1 then
begin
    if editvalor.Text='' then
      alerta(self,'El valor del cobro no puede estar vac�o, introduce el importe a cobrar')
    else
    begin
        datoscobro.Add('clt',editclt.Text);
        datoscobro.Add('valor',editvalor.Text);
        datoscobro.Add('obser',editobser.text);


        caja.registrarcobroentrada(datoscobro);
        close;
    end;

    //entrada
end
else
begin
    if tiponombre.Items[comboboxtipo.Selected.Text]=2  then
    begin
         //salida
    if editvalor.Text='' then
      alerta(self,'El valor del cobro no puede estar vac�o, introduce el importe a cobrar')
    else
    begin
        datoscobro.Add('clt',editclt.Text);
        datoscobro.Add('valor',editvalor.Text);
        datoscobro.Add('obser',editobser.text);
        datoscobro.Add('tipo',opcionnombre.Items[comboboxopcion.Selected.Text]);
        caja.registrarcobrosalida(datoscobro);
        close;
    end;


    end
    else
    begin
      if tiponombre.Items[comboboxtipo.Selected.Text]=-3 then
      begin
         if comboboxselec.Selected <>nil then
         begin
         albaran.MostrarCobrarAlbaran(claveidalb.Items[comboboxselec.Selected.Text],formcobrosclt);
         cerrar:=false;
         close;
         end
         else
          alerta(self,'No se ha seleccionado ning�n albar�n para cobrar');



        //albaran
      end
      else
      begin
          if tiponombre.Items[comboboxtipo.Selected.Text]=3 then
          begin
            //factura

              if comboboxselec.Selected <>nil then
             begin
             factura.MostrarCobrarfactura(claveidfac.Items[comboboxselec.Selected.Text],formcobrosclt);
             cerrar:=false;
             close;
             end
             else
                alerta(self,'No se ha seleccionado ning�n Documento para cobrar');

          end;


      end;

    end;



end;






end;

procedure TFormnuevocobroclt.botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormnuevocobroclt.ComboBoxselecChange(Sender: TObject);
begin
 if tiponombre.Items[comboboxtipo.Selected.Text]=-3  then
 begin

 editpendiente.Text:=clavependientealb.Items[comboboxselec.Selected.Text];
 end
 else
 begin
 editpendiente.Text:=clavependientefac.Items[comboboxselec.Selected.Text];
 end;




end;

procedure TFormnuevocobroclt.ComboBoxTipoChange(Sender: TObject);
var position:single;
    i:integer;
begin
if tiponombre.Items[comboboxtipo.Selected.Text]=1 then
begin
         abortarchange:=true;
         panelselec.Visible:=false;
         panelpendiente.Visible:=false;
         panelvalor.Visible:=false;
         panelobser.Visible:=false;
         panelopcion.Visible:=false;
         panelobser.Visible:=true;
         panelvalor.Visible:=true;


         editvalor.Text:='';
         editobser.Text:='Cobro. ';
         abortarchange:=false;
end
else
begin
    if tiponombre.Items[comboboxtipo.Selected.Text]=2  then
    begin
         //salida
         abortarchange:=true;
        // panelpendiente.Visible:=false;
        // panelvalor.Visible:=false;
         panelselec.Visible:=false;
         panelpendiente.Visible:=false;

         panelvalor.Visible:=false;

         panelobser.Visible:=false;
         panelopcion.Visible:=false;



         panelopcion.Visible:=true;

         panelobser.Visible:=true;
         panelvalor.Visible:=true;
         //Los dos ultimos paneles aveces no salen en el orden correto, el siguiente codigo soluciona el problema
         if panelobser.Position.Y > panelvalor.Position.y then
         begin
         position:=panelobser.Position.Y;
         panelobser.Position.y:=panelvalor.Position.y;
         panelvalor.Position.y:=position;
         end;


         editvalor.Text:='';
         editobser.Text:='Gasto. ';
         for I := 0 to comboboxopcion.Count-1 do
          begin
            if comboboxopcion.ListBox.ItemByIndex(i).Text= tipoid.Items[2] then
               begin
               comboboxopcion.ItemIndex:=i;
               break;
               end;



          end;

         abortarchange:=false;

    end
    else
    begin
      if tiponombre.Items[comboboxtipo.Selected.Text]=-3 then
      begin



       if claveidalb.Count<>0 then
           claveidalb.clear;
        //if  clavemarcaalb.Count<>0 then
        //    clavemarcaalb.clear;
        if  clavependientealb.Count<>0 then
            clavependientealb.clear;
        if  comboboxselec.Count<>0 then
            comboboxselec.clear;
        editpendiente.Text:='';
        queryalb.First;
        carga.visible:=true;
        carga.Enabled:=true;
        while not queryalb.eof do
        begin

        application.ProcessMessages;
        claveidalb.Add(queryalb.FieldByName('serie').AsString+'-'+queryalb.FieldByName('numero').AsString+'/'+anoEuropeo(FormatDateTime('dd/mm/yyyy',queryalb.fieldbyname('fecha').AsDateTime)),queryalb.FieldByName('id').Asinteger);
        //clavemarcaalb.Add(queryalb.FieldByName('serie').AsString+'-'+queryalb.FieldByName('numero').AsString+'/'+anoAmericano(queryalb.FieldByName('fecha').AsString),queryalb.FieldByName('tipo').Asstring);

        clavependientealb.Add(queryalb.FieldByName('serie').AsString+'-'+queryalb.FieldByName('numero').AsString+'/'+anoEuropeo(FormatDateTime('dd/mm/yyyy',queryalb.fieldbyname('fecha').AsDateTime)),formatearnumero(configuracion.items['NumDecTot'],floattostr(queryalb.FieldByName('importe').Asfloat-queryalb.FieldByName('cobrado').Asfloat)));
        comboboxselec.Items.Add(queryalb.FieldByName('serie').AsString+'-'+queryalb.FieldByName('numero').AsString+'/'+anoEuropeo(FormatDateTime('dd/mm/yyyy',queryalb.fieldbyname('fecha').AsDateTime)));


        queryalb.Next;
        end;
         carga.Enabled:=false;
         carga.Visible:=false;
         panelselec.Visible:=true;
         panelpendiente.Visible:=true;

         panelvalor.Visible:=false;
         panelobser.Visible:=false;
         panelopcion.Visible:=false;



        //albaran
      end
      else
      begin
          if tiponombre.Items[comboboxtipo.Selected.Text]=3 then
          begin
              if claveidfac.Count<>0 then
                 claveidfac.clear;
              //if  clavemarcafac.Count<>0 then
              //    clavemarcafac.clear;
              if  clavependientefac.Count<>0 then
                  clavependientefac.clear;
              if  comboboxselec.Count<>0 then
                  comboboxselec.clear;
              editpendiente.Text:='';
              queryfac.First;
              carga.visible:=true;
              carga.Enabled:=true;
              while not queryfac.eof do
              begin

              application.ProcessMessages;
              claveidfac.Add(queryfac.FieldByName('serie').AsString+'-'+queryfac.FieldByName('numero').AsString+'/'+anoEuropeo(FormatDateTime('dd/mm/yyyy',queryfac.fieldbyname('fecha').AsDateTime)),queryfac.FieldByName('id').Asinteger);
              //clavemarcafac.Add(queryfac.FieldByName('serie').AsString+'-'+queryfac.FieldByName('numero').AsString+'/'+anoAmericano(queryfac.FieldByName('fecha').AsString),queryfac.FieldByName('tipo').Asstring);

              clavependientefac.Add(queryfac.FieldByName('serie').AsString+'-'+queryfac.FieldByName('numero').AsString+'/'+anoEuropeo(FormatDateTime('dd/mm/yyyy',queryfac.fieldbyname('fecha').AsDateTime)),formatearnumero(configuracion.items['NumDecTot'],floattostr(queryfac.FieldByName('importe').Asfloat-queryfac.FieldByName('cobrado').Asfloat)));
              comboboxselec.Items.Add(queryfac.FieldByName('serie').AsString+'-'+queryfac.FieldByName('numero').AsString+'/'+anoEuropeo(FormatDateTime('dd/mm/yyyy',queryfac.fieldbyname('fecha').AsDateTime)));


              queryfac.Next;
              end;
               carga.Enabled:=false;
               carga.Visible:=false;
               panelselec.Visible:=true;
               panelpendiente.Visible:=true;

               panelvalor.Visible:=false;
               panelobser.Visible:=false;
               panelopcion.Visible:=false;
          end;
      end;

    end;



end;











end;
{
procedure TForm4.EditcltChange(Sender: TObject);
var datosclt:tdictionary<string,string>;
begin
datosclt:=tdictionary<string,string>.create;
if abortarchange then
    exit;
abortarchange:=true;
datosclt:=cliente.datoscltcompletable(editclt.text);
if datosclt.Count <> 0 then
begin
    editclt.text:=datosclt.items['codclt'];
    editnombre.text:=datosclt.items['nombre'];
    queryalb:=caja.obtenerimpagoalb(editclt.text);
    //aqui query factura cuando se implemente;
    if comboboxtipo.Count<>0 then
       comboboxtipo.clear;



    comboboxtipo.Items.Add(tipoid.Items[1]);
    comboboxtipo.Items.Add(tipoid.Items[2]);
    if queryalb.RecordCount<>0 then
    begin
        comboboxtipo.Items.Add(tipoid.Items[-3]);
        comboboxtipo.ListBox.ItemByIndex(2).StyledSettings := comboboxtipo.ListBox.ItemByIndex(2).StyledSettings - [TStyledSetting.ssOther];
        comboboxtipo.ListBox.ItemByIndex(2).textsettings.font.size:=16;
        comboboxtipo.ListBox.ItemByIndex(2).textsettings.horzalign:=ttextalign.Center;
    end;




    comboboxtipo.ListBox.ItemByIndex(0).StyledSettings := comboboxtipo.ListBox.ItemByIndex(0).StyledSettings - [TStyledSetting.ssOther];
    comboboxtipo.ListBox.ItemByIndex(0).textsettings.font.size:=16;
    comboboxtipo.ListBox.ItemByIndex(0).textsettings.horzalign:=ttextalign.Center;
    comboboxtipo.ListBox.ItemByIndex(1).StyledSettings := comboboxtipo.ListBox.ItemByIndex(1).StyledSettings - [TStyledSetting.ssOther];
    comboboxtipo.ListBox.ItemByIndex(1).textsettings.font.size:=16;
    comboboxtipo.ListBox.ItemByIndex(1).textsettings.horzalign:=ttextalign.Center;

    if comboboxselec.Count<>0 then
        comboboxselec.Clear;
    if claveidalb.Count<>0 then
        claveidalb.Clear;
    if clavemarcaalb.Count<>0 then
        clavemarcaalb.Clear;
    if clavependientealb.Count<>0 then
        clavependientealb.Clear;

    editobser.text:='';
    editvalor.text:='';
    editpendiente.Text:='';

    panelselec.visible:=false;
    panelvalor.visible:=false;
    panelopcion.visible:=false;
    panelobser.visible:=false;
    panelpendiente.Visible:=false;




end
else
begin
      editclt.Text:='';
      editnombre.text:='';
      if ComboBoxTipo.Count<>0 then
         ComboBoxTipo.Clear;
      if comboboxselec.Count<>0 then
         comboboxselec.Clear;
      if claveidalb.Count<>0 then
         claveidalb.Clear;
      if clavemarcaalb.Count<>0 then
         clavemarcaalb.Clear;
       if clavependientealb.Count<>0 then
         clavependientealb.Clear;
        comboboxtipo.Items.Add(tipoid.Items[1]);
        comboboxtipo.Items.Add(tipoid.Items[2]);
        comboboxtipo.ListBox.ItemByIndex(0).StyledSettings := comboboxtipo.ListBox.ItemByIndex(0).StyledSettings - [TStyledSetting.ssOther];
        comboboxtipo.ListBox.ItemByIndex(0).textsettings.font.size:=16;
        comboboxtipo.ListBox.ItemByIndex(0).textsettings.horzalign:=ttextalign.Center;
        comboboxtipo.ListBox.ItemByIndex(1).StyledSettings := comboboxtipo.ListBox.ItemByIndex(1).StyledSettings - [TStyledSetting.ssOther];
        comboboxtipo.ListBox.ItemByIndex(1).textsettings.font.size:=16;
        comboboxtipo.ListBox.ItemByIndex(1).textsettings.horzalign:=ttextalign.Center;
      editobser.text:='';
      editvalor.text:='';
      editpendiente.Text:='';

      panelselec.visible:=false;
      panelvalor.visible:=false;
      panelopcion.visible:=false;
      panelobser.visible:=false;
      panelpendiente.Visible:=false;


      alerta(self,'C�digo de cliente no encontrado');
end;



abortarchange:=false;
end;
 }
procedure TFormnuevocobroclt.EditvalorChange(Sender: TObject);
var valor:string;
begin
valor:=formatearnumero(configuracion.items['NumDecTot'],editvalor.text);
editvalor.text:=valor;

end;

procedure TFormnuevocobroclt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if cerrar then
begin
 formcobrosclt.show;
end;
cerrar:=true;
action:=tcloseaction.cahide;

end;

procedure TFormnuevocobroclt.FormCreate(Sender: TObject);
begin
tipoid:=tdictionary<integer,string>.create;
tiponombre:=tdictionary<string,integer>.create;
claveidalb:=tdictionary<string,integer>.create;
//clavemarcaalb:=tdictionary<string,string>.create;
clavependientealb:=tdictionary<string,string>.create;
claveidfac:=tdictionary<string,integer>.create;
//clavemarcafac:=tdictionary<string,string>.create;
clavependientefac:=tdictionary<string,string>.create;

opcionid:=tdictionary<integer,string>.create;
opcionnombre:=tdictionary<string,integer>.create;
abortarchange:=true;
 control:=tcontrolador.Create;
 cliente:=tcliente.Create;
 albaran:=talbaran.Create;
 factura:=tfactura.create;
 caja:=tcaja.Create;
 OnVirtualKeyboardShown:=TTecladoVirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladovirtual.FormVirtualKeyboardHidden;
cerrar:=true;

end;

procedure TFormnuevocobroclt.FormShow(Sender: TObject);
var
  I: Integer;
  datosclt:tdictionary<string,string>;
begin
configuracion := control.obtenerConfiguracion();
abortarchange:=true;
datosclt:=tdictionary<string,string>.create;
datosclt:=cliente.consultarcliente(codclt);
editclt.Text:=codclt;
editnombre.text:=datosclt.items['cliente'];
if ComboBoxTipo.Count<>0 then
   ComboBoxTipo.Clear;
if comboboxselec.Count<>0 then
   comboboxselec.Clear;
if comboboxopcion.Count<>0 then
   comboboxopcion.Clear;
if claveidalb.Count<>0 then
   claveidalb.Clear;
//if clavemarcaalb.Count<>0 then
//   clavemarcaalb.Clear;
if clavependientealb.Count<>0 then
   clavependientealb.Clear;

 if claveidfac.Count<>0 then
   claveidfac.Clear;
//if clavemarcafac.Count<>0 then
   //clavemarcafac.Clear;
if clavependientefac.Count<>0 then
   clavependientefac.Clear;


if tipoid.Count<>0 then
   tipoid.Clear;
if tiponombre.Count<>0 then
   tiponombre.Clear;
if opcionid.Count<>0 then
   opcionid.Clear;
if opcionnombre.Count<>0 then
   opcionnombre.Clear;
//freeandnil(querytipo);
querytipo:=caja.obtenertiposconcepto();
querytipo.First;

while not querytipo.Eof do
begin
    if UPPERCASE(querytipo.fieldbyname('tipo').asstring)='F' then
    begin
          tipoid.add(querytipo.fieldbyname('codigo').asinteger,querytipo.fieldbyname('nombre').asstring);
          tiponombre.add(querytipo.fieldbyname('nombre').asstring,querytipo.fieldbyname('codigo').asinteger);
          if querytipo.fieldbyname('codigo').asinteger=2 then
          begin
            comboboxopcion.items.add(querytipo.fieldbyname('nombre').asstring);

          //opcionid.add(querytipo.fieldbyname('codigo').asinteger,querytipo.fieldbyname('nombre').asstring);
          opcionnombre.add(querytipo.fieldbyname('nombre').asstring,querytipo.fieldbyname('codigo').asinteger);
          //comboboxopcion.ItemIndex:=0;
          end;


    end
    else
    begin
          comboboxopcion.items.add(querytipo.fieldbyname('nombre').asstring);
          //opcionid.add(querytipo.fieldbyname('codigo').asinteger,querytipo.fieldbyname('nombre').asstring);
          opcionnombre.add(querytipo.fieldbyname('nombre').asstring,querytipo.fieldbyname('codigo').asinteger);

    end;


    querytipo.next;
end;

  for I := 0 to comboboxopcion.Count-1 do
  begin
    if comboboxopcion.ListBox.ItemByIndex(i).Text= tipoid.Items[2] then
       comboboxopcion.ItemIndex:=i;

    comboboxopcion.ListBox.ItemByIndex(i).StyledSettings := comboboxopcion.ListBox.ItemByIndex(i).StyledSettings - [TStyledSetting.ssOther];
    comboboxopcion.ListBox.ItemByIndex(i).textsettings.font.size:=16;
    comboboxopcion.ListBox.ItemByIndex(i).textsettings.horzalign:=ttextalign.Center;

  end;

  //freeandnil(queryalb);
  //freeandnil(queryfac);
  queryalb:=caja.obtenerimpagoalb(codclt);
  queryfac:=caja.obtenerimpagofac(codclt);
    //aqui query factura cuando se implemente;
    if comboboxtipo.Count<>0 then
       comboboxtipo.clear;



    comboboxtipo.Items.Add(tipoid.Items[1]);
    comboboxtipo.Items.Add(tipoid.Items[2]);
    if queryalb.RecordCount<>0 then
    begin
        comboboxtipo.Items.Add(tipoid.Items[-3]);
        comboboxtipo.ListBox.ItemByIndex(2).StyledSettings := comboboxtipo.ListBox.ItemByIndex(2).StyledSettings - [TStyledSetting.ssOther];
        comboboxtipo.ListBox.ItemByIndex(2).textsettings.font.size:=16;
        comboboxtipo.ListBox.ItemByIndex(2).textsettings.horzalign:=ttextalign.Center;
    end;
    if queryfac.RecordCount<>0 then
    begin
        comboboxtipo.Items.Add(tipoid.Items[3]);
        if queryalb.RecordCount<>0 then
        begin

            comboboxtipo.ListBox.ItemByIndex(3).StyledSettings := comboboxtipo.ListBox.ItemByIndex(3).StyledSettings - [TStyledSetting.ssOther];
            comboboxtipo.ListBox.ItemByIndex(3).textsettings.font.size:=16;
            comboboxtipo.ListBox.ItemByIndex(3).textsettings.horzalign:=ttextalign.Center;
        end
        else
        begin
            comboboxtipo.ListBox.ItemByIndex(2).StyledSettings := comboboxtipo.ListBox.ItemByIndex(2).StyledSettings - [TStyledSetting.ssOther];
            comboboxtipo.ListBox.ItemByIndex(2).textsettings.font.size:=16;
            comboboxtipo.ListBox.ItemByIndex(2).textsettings.horzalign:=ttextalign.Center;
        end;

    end;



    comboboxtipo.ListBox.ItemByIndex(0).StyledSettings := comboboxtipo.ListBox.ItemByIndex(0).StyledSettings - [TStyledSetting.ssOther];
    comboboxtipo.ListBox.ItemByIndex(0).textsettings.font.size:=16;
    comboboxtipo.ListBox.ItemByIndex(0).textsettings.horzalign:=ttextalign.Center;
    comboboxtipo.ListBox.ItemByIndex(1).StyledSettings := comboboxtipo.ListBox.ItemByIndex(1).StyledSettings - [TStyledSetting.ssOther];
    comboboxtipo.ListBox.ItemByIndex(1).textsettings.font.size:=16;
    comboboxtipo.ListBox.ItemByIndex(1).textsettings.horzalign:=ttextalign.Center;







editobser.text:='';
editvalor.text:='';
editpendiente.Text:='';

panelselec.visible:=false;
panelvalor.visible:=false;
panelopcion.visible:=false;
panelobser.visible:=false;
panelpendiente.Visible:=false;









abortarchange:=false;





end;
{
procedure TForm4.SearchEditButton2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

formlistaclientes.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
      editclt.Text:=formlistaclientes.seleccionado;
      //formlistarart.Hide;
      end;


    end);





end;
 }



end.
