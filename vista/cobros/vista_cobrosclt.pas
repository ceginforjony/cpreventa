unit vista_cobrosclt;

interface
                   {$ZEROBASEDSTRINGS ON}
uses
    System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,funciones,caja,vista_busquedacobroclt,
  FMX.Controls.Presentation, FMX.Grid.Style, FMX.ScrollBox;


type
  TFormcobrosclt = class(TForm)
    ToolBarCaja: TToolBar;
    Labelcaja: TLabel;
    botonAtras: TButton;
    PanelGrid: TPanel;
    gridcaja: TStringGrid;
    PanelBotones: TPanel;
    BotonNuevoCobro: TButton;
    Icocobro: TImage;
    Label1: TLabel;
    PanelBuscador: TPanel;
    botonbusqueda: TButton;
    Image2: TImage;
    botondesplegar: TButton;
    Image3: TImage;
    botonrefresh: TButton;
    Image4: TImage;
    botonnext: TButton;
    Image5: TImage;
    Carga: TAniIndicator;
    PanelSaldo: TPanel;
    Rectangle1: TRectangle;
    LabelSaldo: TLabel;
    DataSetProvider: TDataSetProvider;
    ClientDataSet: TClientDataSet;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure gridcajaHeaderClick(Column: TColumn);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonNuevoCobroMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PanelBotonesResize(Sender: TObject);
    procedure gridcajaDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
  private
  caj:tcaja;
  cerrar:boolean;
  querycaja:TSQLQuery;
  ordenacion:Tdictionary<string,string>;
  busqueda: TDictionary<String,String>;
  configuracion:TDictionary<String,variant>;
    { Private declarations }
  public
  codclt:string;
    { Public declarations }
  end;

var
  Formcobrosclt: TFormcobrosclt;

implementation
uses vista_clientes;
{$R *.fmx}

procedure TFormcobrosclt.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormcobrosclt.botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;

begin
       //showmessage('entro en busqueda');


       formbusquedacobroclt.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin

          caj.copiardiccionario(formbusquedacobroclt.busqueda,busqueda);


          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not caj.buscar(clientdataset,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                    botonnext.visible:=false;
                    orden:=tdictionary<string,string>.create;
                    orden.add('column','Fecha');
                    orden.add('tipo','desc');

                    caj.Listarcobroscajaclt(codclt,querycaja, busqueda,orden);
                     DataSetProvider.DataSet:=querycaja;
                     ClientDataSet.active:=false;

                     ClientDataSet.active:=true;
                     caj.inicializarGrid(gridcaja,ClientDataSet);

                     caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
                     caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
                     caj.sizecolgrid(gridcaja,7,250,ClientDataSet);
                     ordenacion:=caj.inicializarordenaciongrid(gridcaja);
                     ordenacion.items['Fecha']:='asc';









              end;

      end;


    end);


end;

procedure TFormcobrosclt.botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=275;
      end;







end;

procedure TFormcobrosclt.botonnextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 if(not caj.buscar(clientdataset,busqueda,false)) then
       alerta(self,'error inexperado')
end;

procedure TFormcobrosclt.BotonNuevoCobroMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
caj.mostrarnuevocobroclt(codclt);
cerrar:=false;
close;
end;

procedure TFormcobrosclt.botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
  var orden:tdictionary<string,string>;
  begin
  if busqueda.Count <> 0 then
  begin
      busqueda.Clear;
  end;
  carga.visible:=true;
  orden:=tdictionary<string,string>.create;
  orden.add('column','Fecha');
  orden.add('tipo','desc');


     caj.Listarcobroscajaclt(codclt,querycaja, busqueda,orden);
     DataSetProvider.DataSet:=querycaja;
     ClientDataSet.active:=false;
     application.ProcessMessages;

     application.ProcessMessages;
     ClientDataSet.active:=true;
     caj.inicializarGrid(gridcaja,ClientDataSet);

     caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
     caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
     caj.sizecolgrid(gridcaja,7,250,ClientDataSet);
     ordenacion:=caj.inicializarordenaciongrid(gridcaja);
     ordenacion.items['Fecha']:='asc';
     carga.visible:=false;


end;

procedure TFormcobrosclt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if cerrar then
begin
clientdataset.close;
formclientes.show;

end;
cerrar:=true;
action:=tcloseaction.cahide;
end;

procedure TFormcobrosclt.FormCreate(Sender: TObject);
begin
 caj:=tcaja.create;
 cerrar:=true;
 caj.InicializarBotonera(panelbotones,2,3);
  //querycaja:= TSQLQuery;
 busqueda:= TDictionary<String,String>.create;





end;

procedure TFormcobrosclt.FormShow(Sender: TObject);
var orden:tdictionary<string,string>;
begin
 if busqueda.Count <> 0 then
  begin
      busqueda.Clear;
  end;
  configuracion:=caj.obtenerConfiguracion();

  carga.visible:=true;
  orden:=tdictionary<string,string>.create;
  orden.add('column','Fecha');
  orden.add('tipo','desc');
  labelcaja.text:='Cobros, Clt: '+codclt+'.';

     //application.ProcessMessages;

    //application.ProcessMessages;
     caj.Listarcobroscajaclt(codclt,querycaja, busqueda,orden);
     DataSetProvider.DataSet:=querycaja;
    if clientdataset.active=true then
    begin
        labelsaldo.text:='Saldo: '+formatearnumero(configuracion.Items['NumDecTot'],caj.obtenersaldoclt(codclt));
        ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,7,250,ClientDataSet);
         ordenacion:=caj.inicializarordenaciongrid(gridcaja);
         ordenacion.items['Fecha']:='asc';
    end
    else
      begin

        labelsaldo.text:='Saldo: '+formatearnumero(configuracion.Items['NumDecTot'],caj.obtenersaldoclt(codclt));
        //ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,7,250,ClientDataSet);
         ordenacion:=caj.inicializarordenaciongrid(gridcaja);
         ordenacion.items['Fecha']:='asc';

      end;

  carga.visible:=false;

end;

procedure TFormcobrosclt.gridcajaDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridcaja.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

procedure TFormcobrosclt.gridcajaHeaderClick(Column: TColumn);
var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=caj.inicializarordenaciongrid(gridcaja);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin
        caj.Listarcobroscajaclt(codclt,querycaja, busqueda,orden);
     DataSetProvider.DataSet:=querycaja;
     ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,7,250,ClientDataSet);



  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

        caj.Listarcobroscajaclt(codclt,querycaja, busqueda,orden);
        DataSetProvider.DataSet:=querycaja;
        ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,7,250,ClientDataSet);

        botonnext.Visible:=false;



end;




   carga.visible:=false;

end;




procedure TFormcobrosclt.PanelBotonesResize(Sender: TObject);
begin
caj.InicializarBotonera(panelbotones,2,3);
end;

end.
