unit vista_caja;

interface
           {$ZEROBASEDSTRINGS ON}
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,funciones,caja,vista_busquedacobro,vista_nuevocobro,
  FMX.Controls.Presentation,system.IOUtils, FMX.Grid.Style,
  FMX.ScrollBox;


type
  TFormCaja = class(TForm)
    ToolBarCaja: TToolBar;
    Labelcaja: TLabel;
    botonAtras: TButton;
    PanelGrid: TPanel;
    gridcaja: TStringGrid;
    PanelBotones: TPanel;
    BotonNuevoCobro: TButton;
    Icocobro: TImage;
    Label1: TLabel;
    PanelBuscador: TPanel;
    botonbusqueda: TButton;
    Image2: TImage;
    botondesplegar: TButton;
    Image3: TImage;
    botonrefresh: TButton;
    Image4: TImage;
    botonnext: TButton;
    Image5: TImage;
    Carga: TAniIndicator;
    ClientDataSet: TClientDataSet;
    DataSetProvider: TDataSetProvider;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PanelSaldo: TPanel;
    Rectangle1: TRectangle;
    LabelSaldo: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonNuevoCobroMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PanelBotonesResize(Sender: TObject);
    procedure gridcajaDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
    procedure gridcajaCellDblClick(const Column: TColumn; const Row: Integer);
  private

  caj:tcaja;
  cerrar:boolean;
  querycaja:TSQLQuery;
  ordenacion:Tdictionary<string,string>;
  busqueda: TDictionary<String,String>;
  configuracion:TDictionary<String,variant>;

   { Private declarations }
  public
    { Public declarations }
  end;

var
  FormCaja: TFormCaja;

implementation
 uses vista_menu;
{$R *.fmx}


procedure TFormCaja.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormCaja.botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;

begin
       //showmessage('entro en busqueda');


       formbusquedacobro.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin

          caj.copiardiccionario(formbusquedacobro.busqueda,busqueda);


          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not caj.buscar(clientdataset,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                    botonnext.visible:=false;
                    orden:=tdictionary<string,string>.create;
                    orden.add('column','Fecha');
                    orden.add('tipo','desc');

                    caj.Listarcobroscaja(querycaja, busqueda,orden);
                     DataSetProvider.DataSet:=querycaja;
                     ClientDataSet.active:=false;

                     ClientDataSet.active:=true;
                     caj.inicializarGrid(gridcaja,ClientDataSet);

                     caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
                     caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
                     caj.sizecolgrid(gridcaja,8,250,ClientDataSet);
                     ordenacion:=caj.inicializarordenaciongrid(gridcaja);
                     ordenacion.items['Fecha']:='asc';









              end;

      end;


    end);


end;

procedure TFormCaja.botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=275;
      end;







end;

procedure TFormCaja.botonnextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 if(not caj.buscar(clientdataset,busqueda,false)) then
       alerta(self,'error inexperado')
end;

procedure TFormCaja.BotonNuevoCobroMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin

    If not fileExists(TPath.getPublicPath+'/NGC.txt') then
    begin
            caj.cargarvista(formnuevocobro);
            cerrar:=false;
            close;
    end
      else
        alerta(self,'Imposible Realizar Operación, No dispone de permisos');


end;

procedure TFormCaja.botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
  var orden:tdictionary<string,string>;
  begin
  if busqueda.Count <> 0 then
  begin
      busqueda.Clear;
  end;
  carga.visible:=true;
  orden:=tdictionary<string,string>.create;
  orden.add('column','Fecha');
  orden.add('tipo','desc');


     caj.Listarcobroscaja(querycaja, busqueda,orden);
     DataSetProvider.DataSet:=querycaja;
     ClientDataSet.active:=false;
     application.ProcessMessages;

     application.ProcessMessages;
     ClientDataSet.active:=true;
     caj.inicializarGrid(gridcaja,ClientDataSet);

     caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
     caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
     caj.sizecolgrid(gridcaja,8,250,ClientDataSet);
     ordenacion:=caj.inicializarordenaciongrid(gridcaja);
     ordenacion.items['Fecha']:='asc';
     carga.visible:=false;


end;

procedure TFormCaja.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if cerrar then
begin
clientdataset.close;
form2.activate;

end;
cerrar:=true;
action:=tcloseaction.cahide;
end;

procedure TFormCaja.FormCreate(Sender: TObject);
begin
 caj:=tcaja.create;
 cerrar:=true;
  caj.InicializarBotonera(panelbotones,2,3);
  //querycaja:= TSQLQuery;
 busqueda:= TDictionary<String,String>.create;





end;

procedure TFormCaja.FormShow(Sender: TObject);
var orden:tdictionary<string,string>;
begin
 if busqueda.Count <> 0 then
  begin
      busqueda.Clear;
  end;
  configuracion:=caj.obtenerConfiguracion();

  carga.visible:=true;
  orden:=tdictionary<string,string>.create;
  orden.add('column','Fecha');
  orden.add('tipo','desc');


     //application.ProcessMessages;

    //application.ProcessMessages;
     caj.Listarcobroscaja(querycaja, busqueda,orden);
     DataSetProvider.DataSet:=querycaja;
    if clientdataset.active=true then
    begin
        labelsaldo.text:='Saldo: '+formatearnumero(configuracion.Items['NumDecTot'],caj.obtenersaldo());
        ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,8,250,ClientDataSet);
         ordenacion:=caj.inicializarordenaciongrid(gridcaja);
         ordenacion.items['Fecha']:='asc';
    end
    else
      begin

        labelsaldo.text:='Saldo: '+formatearnumero(configuracion.Items['NumDecTot'],caj.obtenersaldo());
        //ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,8,250,ClientDataSet);
         ordenacion:=caj.inicializarordenaciongrid(gridcaja);
         ordenacion.items['Fecha']:='asc';

      end;

  carga.visible:=false;

end;

procedure TFormCaja.gridcajaCellDblClick(const Column: TColumn;
  const Row: Integer);
  var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=caj.inicializarordenaciongrid(gridcaja);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin
        caj.Listarcobroscaja(querycaja, busqueda,orden);
     DataSetProvider.DataSet:=querycaja;
     ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
        caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,8,250,ClientDataSet);



  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

        caj.Listarcobroscaja(querycaja, busqueda,orden);
        DataSetProvider.DataSet:=querycaja;
        ClientDataSet.active:=false;
        application.ProcessMessages;

        application.ProcessMessages;
        ClientDataSet.active:=true;
        caj.inicializarGrid(gridcaja,ClientDataSet);

        caj.sizecolgrid(gridcaja,0,0,ClientDataSet);
         caj.sizecolgrid(gridcaja,2,120,ClientDataSet);
        caj.sizecolgrid(gridcaja,8,250,ClientDataSet);

        botonnext.Visible:=false;



end;




   carga.visible:=false;
end;

procedure TFormCaja.gridcajaDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridcaja.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

procedure TFormCaja.PanelBotonesResize(Sender: TObject);
begin
  caj.InicializarBotonera(panelbotones,2,3);
end;

end.
