unit mensaje;

interface
                 {$ZEROBASEDSTRINGS ON}
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Objects, FMX.Layouts, FMX.Controls.Presentation;

type
  TForm17 = class(TForm)
    Layautmessage: TLayout;
    Panel11: TPanel;
    Rectangle7: TRectangle;
    Layout15: TLayout;
    Layout16: TLayout;
    Rectangle8: TRectangle;
    Labeltitle: TLabel;
    Layout17: TLayout;
    Button1: TButton;
    Image1: TImage;
    Labelcontenido: TLabel;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    { Private declarations }
  public
  titulo:string;
  contenido:string;
  size:integer;
    { Public declarations }
  end;

var
  Form17: TForm17;

implementation

{$R *.fmx}
 uses carga_inicial, vista_clientes;
procedure TForm17.Button1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

close;
end;

procedure TForm17.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=tcloseaction.caHide;
end;

procedure TForm17.FormShow(Sender: TObject);
begin
labeltitle.Text:=titulo;
labelcontenido.Text:=contenido;
panel11.Height:=size;


end;

end.
