unit vista_ajustes;

interface
      {$ZEROBASEDSTRINGS ON}
uses
 System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,funciones,FMX.ListBox,comunicacion,Androidapi.JNI.BluetoothAdapter,Androidapi.JNI.JavaTypes,
  Androidapi.JNIBridge,Android.JNI.Toast,Androidapi.Helpers,usuario,controlador,
  FMX.Controls.Presentation;

type
  TFormAjustes = class(TForm)
    ToolBarImpresion: TToolBar;
    Labelimpresion: TLabel;
    botonAtras: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    botonAsignar: TButton;
    IcoAceptar: TImage;
    LabelAceptar: TLabel;
    Panel7: TPanel;
    Panel8: TPanel;
    Label2: TLabel;
    Panel9: TPanel;
    ComboBoxescala: TComboBox;
    ListBoxItemdefecto: TListBoxItem;
    ListBoxItemMediana: TListBoxItem;
    ListBoxItemGrande: TListBoxItem;
    ListBoxItemEnorme: TListBoxItem;
    ListBoxItemGigantesco: TListBoxItem;
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel2Resize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure botonAsignarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
  user:tusuario;  { Private declarations }
  ajustes:tdictionary<string,string>;
  public
    { Public declarations }
  end;

var
  FormAjustes: TFormAjustes;

implementation
 uses vista_menu;
{$R *.fmx}

procedure TFormAjustes.botonAsignarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var ajus:tdictionary<string,string>;
    controlador:tcontrolador;
begin

  ajus:=tdictionary<string,string>.create;

  case comboboxescala.Selected.Index of
        0:ajus.Add('escala','0');
        1:ajus.Add('escala','1.1');
        2:ajus.Add('escala','1.2');
        3:ajus.Add('escala','1.3');
        4:ajus.Add('escala','1.4');
      end;

     user.actualizarajustes(ajus);

  //ajus.add('documento',)
    controlador:= tcontrolador.Create;
    controlador.escalar;






alerta(self,'Las opciones fueron establecidas.');


end;

procedure TFormAjustes.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormAjustes.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  freeandnil(ajustes);
  form2.activate;
  action:=tcloseaction.caHide;





end;

procedure TFormAjustes.FormCreate(Sender: TObject);
begin


 user:=tusuario.create;
 user.InicializarBotonera(panel2,2,20);



end;

procedure TFormAjustes.FormShow(Sender: TObject);
begin

  ajustes:=user.obtenerajustes;

  if ajustes.items['escala']='0' then
    self.Comboboxescala.ItemIndex:=0
  else
    if ajustes.items['escala']='1.1' then
        self.Comboboxescala.ItemIndex:=1
    else
        if ajustes.items['escala']='1.2' then
           self.Comboboxescala.ItemIndex:=2
        else
          if ajustes.items['escala']='1.3' then
              self.Comboboxescala.ItemIndex:=3
          else
                if ajustes.items['escala']='1.4' then
                  self.Comboboxescala.ItemIndex:=4
                else
                  self.Comboboxescala.ItemIndex:=0;




end;

procedure TFormAjustes.Panel2Resize(Sender: TObject);
begin
 user.InicializarBotonera(panel2,2,20);
end;

end.
