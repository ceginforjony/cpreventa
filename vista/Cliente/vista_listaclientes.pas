unit vista_listaclientes;

interface
        {$ZEROBASEDSTRINGS ON}
uses
 System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid, cliente,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,vista_buscarCliente,Generics.Collections{,vista_albaranescliente},funciones,
  FMX.Controls.Presentation, FMX.Grid.Style, FMX.ScrollBox;


type
  TFormlistaclientes = class(TForm)
    ToolBarCliente: TToolBar;
    LabelClientes: TLabel;
    botonAtras: TButton;
    PanelGrid: TPanel;
    gridcliente: TStringGrid;
    PanelBotones: TPanel;
    Carga: TAniIndicator;
    Botonseleccionar: TButton;
    Icoselec: TImage;
    Label1: TLabel;
    DataSetProvider1: TDataSetProvider;
    ClientDataSet1: TClientDataSet;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PanelBuscador: TPanel;
    botonbusqueda: TButton;
    Image1: TImage;
    botonabajo: TButton;
    Image6: TImage;
    botonrefresh: TButton;
    Image7: TImage;
    botonnext: TButton;
    Image8: TImage;
    botonarriba: TButton;
    Image9: TImage;
    botonderecha: TButton;
    Image10: TImage;
    botonizquierda: TButton;
    Image11: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
        procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
     procedure FormShow(Sender: TObject);
    procedure gridclienteHeaderClick(Column: TColumn);
    procedure BotonseleccionarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure gridclienteDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
    procedure botonizquierdaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonderechaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private

  cliente: tcliente;
  queryCliente: TSQLQuery;
  //busqueda: TDictionary<String,String>;
  busqueda: TDictionary<String,String>;
  ordenacion: TDictionary<string,string>;
    { Private declarations }
  public
   seleccionado:string; { Public declarations }
  end;

var
  Formlistaclientes: TFormlistaclientes;

implementation

{$R *.fmx}


procedure TFormlistaclientes.botonabajoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
clientdataset1.next;
end;

procedure TFormlistaclientes.botonarribaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
ClientDataSet1.Prior;
end;

procedure TFormlistaclientes.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormlistaclientes.botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);

var orden:tdictionary<string,string>;

begin
       //showmessage('entro en busqueda');

       formbusquedaclt.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
          cliente.copiardiccionario(formbusquedaclt.busqueda,busqueda);


          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not cliente.buscar(clientdataset1,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                      botonnext.visible:=false;
                      orden:=tdictionary<string,string>.create;
                      orden.add('column','C�digo');
                      orden.add('tipo','asc');


                          //application.ProcessMessages;
                        cliente.Listarclientes(querycliente, busqueda,orden);
                         //application.ProcessMessages;
                        DataSetProvider1.DataSet:=querycliente;
                        clientdataset1.Close;

                        ClientDataSet1.open;
                        cliente.inicializarGrid(gridcliente,ClientDataSet1);
                        cliente.sizecolgrid(gridcliente,1,200,ClientDataSet1);
                        cliente.sizecolgrid(gridcliente,2,200,ClientDataSet1);
                        cliente.sizecolgrid(gridcliente,3,200,ClientDataSet1);
                        ordenacion:=cliente.inicializarordenaciongrid(gridcliente);
                        ordenacion.items['C�digo']:='desc';

                      //ClientDataset1.Refresh;

                      //showmessage(inttostr(queryarticulo.RecordCount));



              end;

      end;
    end);


end;

procedure TFormlistaclientes.botonderechaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridcliente.ScrollBy(-1*gridcliente.Columns[0].Width,0);
end;

procedure TFormlistaclientes.botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

   if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=275;
      end;



end;

procedure TFormlistaclientes.botonizquierdaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridcliente.ScrollBy(  gridcliente.Columns[0].Width,0);
end;

procedure TFormlistaclientes.botonnextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
          if(not cliente.buscar(clientdataset1,busqueda,false)) then
       alerta(self,'error inexperado');
end;

procedure TFormlistaclientes.botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
 var orden:tdictionary<string,string>;
  begin
     orden:=tdictionary<string,string>.create;
    orden.add('column','C�digo');
    orden.add('tipo','asc');
if(busqueda.count <> 0) then
   busqueda.Clear;
     carga.visible:=true;
    //application.ProcessMessages;
  cliente.Listarclientes(querycliente, busqueda,orden);
   //application.ProcessMessages;
  DataSetProvider1.DataSet:=querycliente;
  clientdataset1.Close;
  application.ProcessMessages;
  application.ProcessMessages;
  ClientDataSet1.open;
  cliente.inicializarGrid(gridcliente,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,1,200,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,2,200,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,3,200,ClientDataSet1);
  ordenacion:=cliente.inicializarordenaciongrid(gridcliente);
  ordenacion.items['C�digo']:='desc';


  carga.visible:=false;



 // botonnext.Visible:=false;


end;



procedure TFormlistaclientes.BotonseleccionarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
   seleccionado:=clientdataset1.fieldbyname('codigo').AsString;
   modalresult:= mrOK;
   close;
end;

procedure TFormlistaclientes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  clientdataset1.close;
action:=tcloseaction.cahide;

end;

procedure TFormlistaclientes.FormCreate(Sender: TObject);
begin
   application.ProcessMessages;
   busqueda:= TDictionary<string,string>.create;
  cliente:= tcliente.Create;
  cliente.InicializarBotonera(panelbotones,2,3);
  cliente.ReescalarLetraLabel(self);








end;

procedure TFormlistaclientes.FormShow(Sender: TObject);
var orden:tdictionary<string,string>;
begin

if clientdataset1.active=false then
begin
      orden:=tdictionary<string,string>.create;
      orden.add('column','C�digo');
      orden.add('tipo','asc');
  if busqueda.count <> 0 then
      busqueda.clear;
  carga.visible:=true;
    //application.ProcessMessages;
  cliente.Listarclientes(querycliente, busqueda,orden);
   //application.ProcessMessages;
  DataSetProvider1.DataSet:=querycliente;
  application.ProcessMessages;
  application.ProcessMessages;
  ClientDataSet1.active:=true;
  cliente.inicializarGrid(gridcliente,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,1,200,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,2,200,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,3,200,ClientDataSet1);
  ordenacion:=cliente.inicializarordenaciongrid(gridcliente);
  ordenacion.items['C�digo']:='desc';


  carga.visible:=false;
end;
end;
procedure TFormlistaclientes.gridclienteDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridcliente.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

procedure TFormlistaclientes.gridclienteHeaderClick(Column: TColumn);
var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=cliente.inicializarordenaciongrid(gridcliente);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin
   cliente.Listarclientes(querycliente, busqueda,orden);
   //application.ProcessMessages;
  DataSetProvider1.DataSet:=querycliente;
  clientdataset1.Close;
  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;
   cliente.inicializarGrid(gridcliente,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,1,200,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,2,200,ClientDataSet1);
  cliente.sizecolgrid(gridcliente,3,200,ClientDataSet1);




  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

                  cliente.Listarclientes(querycliente, busqueda,orden);
   //application.ProcessMessages;
                  DataSetProvider1.DataSet:=querycliente;
                  clientdataset1.Close;
                  Application.processmessages;
                  Application.processmessages;
                  ClientDataSet1.active:=true;
                   cliente.inicializarGrid(gridcliente,ClientDataSet1);
                  cliente.sizecolgrid(gridcliente,1,200,ClientDataSet1);
                  cliente.sizecolgrid(gridcliente,2,200,ClientDataSet1);
                  cliente.sizecolgrid(gridcliente,3,200,ClientDataSet1);
                  botonnext.Visible:=false;



end;




   carga.visible:=false;




end;

end.
