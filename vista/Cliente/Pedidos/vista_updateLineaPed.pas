unit vista_updateLineaPed;

interface
                   {$ZEROBASEDSTRINGS ON}
uses
   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts,cliente, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,pedido, FMX.Colors, FMX.Edit,
  FMX.ListBox,articulo,fmx.Ani,funciones,vista_listarArticulos,vista_listarartpedhis,
  FMX.Controls.Presentation{,vista_asignarLotePed};


type
  TFormupdatelineaped = class(TForm)
    ToolBarlineadeventa: TToolBar;
    LabelPed: TLabel;
    botonAtras: TButton;
    ScrollBox1: TScrollBox;
    Panel1: TPanel;
    Panel3: TPanel;
    descripcion: TEdit;
    Panel2: TPanel;
    botonAceptar: TButton;
    IcoAceptar: TImage;
    LabelUpdate: TLabel;
    Panel4: TPanel;
    Labelcodigo: TLabel;
    LabelIva: TLabel;
    Editcodigo: TEdit;
    SearchEditButton2: TSearchEditButton;
    ComboBoxIVa: TComboBox;
    Panel5: TPanel;
    Label2: TLabel;
    editcantidad: TEdit;
    Labelunds: TLabel;
    Panelmanejabulto: TPanel;
    Label1: TLabel;
    ComboBoxBultos: TComboBox;
    editbulto: TEdit;
    LabelStock: TLabel;
    Paneldes: TPanel;
    Label8: TLabel;
    Editdes: TEdit;
    Panel8: TPanel;
    Label6: TLabel;
    Editimporte: TEdit;
    Panel9: TPanel;
    EditTARIFA: TEdit;
    EditBotonMAS: TEditButton;
    ImageMAS: TImage;
    Labelprecio: TLabel;
    BotonHistorial: TButton;
    Panel6: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EditcodigoChange(Sender: TObject);
    procedure editcantidadChange(Sender: TObject);
    procedure EditTARIFAChange(Sender: TObject);
    procedure EditdesChange(Sender: TObject);
    procedure EditBotonMASMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure SearchEditButton2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Panel2Resize(Sender: TObject);
    procedure editbultoChange(Sender: TObject);
    procedure BotonHistorialMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
   pedido:  tpedido;
    cliente:  tcliente;
    articulo: tarticulos;
     queryBultos:TSQLQuery;
    abortarchange:boolean;
    configuracion: tdictionary<string,variant>;
    serie: tdictionary<string,string>;
    tarifas: tdictionary<integer,string>;
    descuentos:tdictionary<string,string>;
    DatosArt:tdictionary<string,variant>;
    DatosLineaped:tdictionary<string,string>;
    nueva:boolean;
    codigoartini:string;
    cantidadini:double;
    lote:string;

    conttarifa:integer;
    cerrar:boolean;
  procedure msgyes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
  procedure msgno (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);

    { Private declarations }
  public
  idlineaped:integer;
  padre:tform;
    { Public declarations }
  end;

var
  Formupdatelineaped: TFormupdatelineaped;

implementation

{$R *.fmx}

procedure TFormupdatelineaped.botonAceptarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
tbutton(sender).enabled:=false;
application.ProcessMessages;
confirmar(self,'�Desea usted confirmar la actualizaci�n de la l�nea de venta?',msgyes,msgno);
tbutton(sender).enabled:=true;
  {var DatosLineaPednueva:tdictionary<string,string>;

begin
DatosLineaPednueva:=tdictionary<string,string>.create;
if editcodigo.Text = ''  then
  begin
    if Uppercase(configuracion.Items['PermitirSinCodigo']) ='S' then
    begin



      datoslineapednueva.Add('codigo',editcodigo.Text);
      //datoslineaped.Add('IvaPrecios',configuracion.Items['IvaPrecios']);
            if(UPPERCASE(serie.items['conIva'])='S') then
      begin
          datosLineaPednueva.Add('iva',comboboxiva.Selected.Text);
      end
      else
      begin
          if(datosart.count <> 0) then
          begin
              datosLineaPednueva.Add('iva',datosart.items['iva']);
          end
          else
               datosLineaPednueva.Add('iva',configuracion.items['iva1']);
      end;
      datoslineapednueva.Add('precio',edittarifa.text);
      datoslineapednueva.Add('descuento',editdes.text);
            if(UPPERCASE(configuracion['ManejaBultos'])='S')then
      begin
        datoslineapednueva.Add('bultos',editbulto.text);
        datoslineapednueva.Add('LiteralBultos',comboboxbultos.selected.Text);
      end
      else
      begin
        datoslineapednueva.Add('bultos','0');
        datoslineapednueva.Add('LiteralBultos','');
      end;
      datoslineapednueva.add('descripcion',descripcion.Text);
      datoslineapednueva.Add('cantidad',editcantidad.Text);
      datoslineapednueva.Add('importe',editimporte.text);
      datoslineapednueva.Add('unidades',labelunds.text);
      idlineaped:=pedido.A�adirLineaPed(datoslineapednueva,idped);
      pedido.reiniciarlotes(idlineaped);


      close;
      //llamada a a�adir linea
      //cerrar formulario



    end
    else
    begin
      alerta(self,'No permitido la venta sin codigo');
    end;
  end
  else
  begin
      datoslineapednueva.Add('codigo',editcodigo.Text);
      //datoslineaped.Add('IvaPrecios',configuracion.Items['IvaPrecios']);
      if(UPPERCASE(serie.items['conIva'])='S') then
      begin
          datosLineaPednueva.Add('iva',comboboxiva.Selected.Text);
      end
      else
      begin
          if(datosart.count <> 0) then
          begin
              datosLineaPednueva.Add('iva',datosart.items['iva']);
          end
          else
               datosLineaPednueva.Add('iva',configuracion.items['iva1']);
      end;
      datoslineapednueva.Add('precio',edittarifa.text);
      datoslineapednueva.Add('descuento',editdes.text);
      if(UPPERCASE(configuracion['ManejaBultos'])='S')then
      begin
        datoslineapednueva.Add('bultos',editbulto.text);
        datoslineapednueva.Add('LiteralBultos',comboboxbultos.selected.Text);
      end
      else
      begin
        datoslineapednueva.Add('bultos','0');
        datoslineapednueva.Add('LiteralBultos','');
      end;
      datoslineapednueva.add('descripcion',descripcion.Text);
      datoslineapednueva.Add('cantidad',editcantidad.Text);
      datoslineapednueva.Add('importe',editimporte.text);
      datoslineapednueva.Add('unidades',labelunds.text);
      //idlineaped:=pedido.A�adirLineaPed(datoslineapednueva,idped);

      if codigoartini <> editcodigo.text then
      begin
      pedido.reiniciarlotes(idlineaped);
      if(uppercase(datosart.items['lote'])='S') then
        begin
          pedido.mostrarAsignarLote(idlineaped,padre);
          nueva:=true;
          cerrar:=false;
          close;
        end
        else
          close;
      end
      else
      begin
      if(uppercase(lote)='S') and (strtofloat(cantidadini)<>strtofloat(editcantidad.text)) then
        begin
          pedido.mostrarAsignarLote(idlineaped,padre);
          nueva:=true;
          cerrar:=false;
          close;
        end
        else
          close;



      end;




  end;


 }

end;

procedure TFormupdatelineaped.botonAtrasMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormupdatelineaped.BotonHistorialMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin


  FormListarPedHistorial.cliente:=strtoint(pedido.obtenerSerielinea(idlineaped).Items['cliente']);
  FormListarPedHistorial.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
      editcodigo.Text:=FormListarPedHistorial.seleccionado;

      //formlistarart.Hide;
      end;


    end);











end;

procedure TFormupdatelineaped.Button1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
{
   if(uppercase(lote)='S') then
        begin


          pedido.mostrarAsignarLote(idlineaped,self);
          cerrar:=false;
          close;
        end
        else
   alerta(self,'El Art�culo de la l�nea a editar no dispone de control de lotes.');
 }




end;

procedure TFormupdatelineaped.EditBotonMASMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var encontrado:boolean;
begin
encontrado:=false;
conttarifa:=conttarifa+1;
while not encontrado do
begin
  if conttarifa=8 then
     conttarifa:=1;
  if(tarifas.ContainsKey(conttarifa)) then
  begin
  //showmessage('contador'+ inttostr(conttarifa));
  if conttarifa=7 then
  begin
      LabelPrecio.Text := 'PrecioD:';
  end
  else
      LabelPrecio.Text := 'PrecioT'+inttostr(conttarifa)+':';

  edittarifa.text:=tarifas.Items[conttarifa];
  encontrado:=true;
  end
  else
  begin
    conttarifa:=conttarifa+ 1;
  end;

end;



end;

procedure TFormupdatelineaped.editbultoChange(Sender: TObject);
var bulto:string;
begin
bulto:=formatearnumero(0,editbulto.text);
if(bulto='')then
begin
  editbulto.text:=formatearnumero(0,'0');
  editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],'0');
end
else
begin
   if datosart.Count<>0 then
   begin
     editbulto.text:=bulto;
     editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],floattostr(datosart.Items['unicaja']*strtoint(bulto)));
   end
   else
    editbulto.text:=bulto;
end;
end;

procedure TFormupdatelineaped.editcantidadChange(Sender: TObject);
var cantidad:string;
     calculo:double;
     precio:string;
     descuento:string;
begin
cantidad:=formatearnumero(configuracion.items['NumDecCan'],editcantidad.text);
if(cantidad='')then
begin
  editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],'0');
  cantidad:='0';
end
else
   editcantidad.text:=cantidad;
      if(editdes.Text='') then
    begin
      descuento:='0';
    end
    else
      descuento:=editdes.Text;
   if(edittarifa.Text='') then
   begin
      precio:='0';
   end
   else
      precio:=edittarifa.Text;



   calculo:=(strtofloat(cantidad))* (strtofloat(precio)-(strtofloat(precio)*((strtofloat(descuento))/100 )));
   editimporte.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(calculo));

end;
procedure TFormupdatelineaped.EditcodigoChange(Sender: TObject);
var
  i: Integer;
  encontrado:boolean;
  iva:tdictionary<string,integer>;
  caso:integer;
begin
//abortar change es necesario puesto que se evita que cuando se autocomplete el codigo y se asigne al edit, no vuelva a llamar al evento onchage
  if(abortarchange)then
        exit;
  abortarchange:=true;
  iva:=tdictionary<string,integer>.create;
  iva.Add(configuracion.Items['iva1'],0);
  iva.Add(configuracion.Items['iva2'],1);
  iva.Add(configuracion.Items['iva3'],2);
  encontrado:=false;
  if tarifas.Count <> 0 then
     tarifas.Clear;
  if descuentos.Count <> 0 then
      descuentos.Clear;
  datosart:=articulo.CosultarArticulosCompletable(editcodigo.text);
  if datosart.count<>0 then
  begin
    editcodigo.Text := datosart.Items['codigo'];
    descripcion.text:= datosart.items['descripcion'];
    editbulto.Text:='0';
    if(UPPERCASE(serie.items['conIva'])='S') then
    BEGIN
    caso:=iva.Items[datosart.items['iva']];
    case caso of
     0: begin
             comboboxiva.ItemIndex:=0;
        end;
     1: begin
              comboboxiva.ItemIndex:=1;
        end;
     2 :begin
              comboboxiva.ItemIndex:=2;
        end;
    end;


    END;




    if UpperCase(configuracion.Items['PCambioTarifa'])= 'S' then
    begin
      if (Uppercase(configuracion.items['IvaPrecios']) = 'S') and (uppercase(serie.items['ConIva'])='S')   then
      BEGIN
      for i := 1 to 6 do
       begin
        if(configuracion.items['VerTarifa'+inttostr(i)]='S') THEN
              begin
              tarifas.add(i,DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),datosart.items['t'+inttostr(i)]));
              end;
        end;


      END
      ELSE
      BEGIN
      for i := 1 to 6 do
       begin
        if(configuracion.items['VerTarifa'+inttostr(i)]='S') THEN
              begin
              tarifas.add(i,formatearnumero(configuracion.items['NumDecPre'],datosart.items['t'+inttostr(i)]));
              end;
        end;
       END;
       if tarifas.count <> 0 then
         begin
          conttarifa:=1;
           while not encontrado do
           begin
              if(tarifas.ContainsKey(conttarifa)) then
               begin
                LabelPrecio.Text := 'PrecioT'+inttostr(conttarifa)+':';
                edittarifa.text:=tarifas.Items[conttarifa];
                encontrado:=true;
               end
               else
                begin
                  conttarifa:=conttarifa + 1;
                end;

            end;
        end
        else
          begin
            LabelPrecio.Text:= 'Precio:';
            edittarifa.Text :=  formatearnumero(configuracion.items['NumDecPre'],'0');

          end;

        editdes.Text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
        descuentos:=articulo.obtenerdescuento(datosart.items['codigo'],serie.items['cliente']);
        if descuentos.count <> 0 then
        begin

        if strtofloat(descuentos.items['precio'])<>0 then
         begin
        if (Uppercase(configuracion.items['IvaPrecios']) = 'S') and (uppercase(serie.items['ConIva'])='S')   then
        begin
          edittarifa.text:=DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),descuentos.items['precio']);
          tarifas.add(7,DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),descuentos.items['precio']));
        end
        else
        begin
        edittarifa.text:=formatearnumero(configuracion.items['NumDecPre'],descuentos.items['precio']);
        tarifas.add(7,formatearnumero(configuracion.items['NumDecPre'],descuentos.items['precio']));
        end;
         labelprecio.text:='PrecioD:';
         conttarifa:=7;
         end;



        editdes.text:=formatearnumero(configuracion.items['NumDecPorDes'],descuentos.items['des']);


        end;
        labelStock.Text:='Stock Art�culo: '+formatearnumero(configuracion.items['NumDecCan'],datosart.Items['stock'])+' '+datosart.Items['unidad'];
        labelunds.Text:=datosart.Items['unidad'];

        editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],'1');
        if(UPPERCASE(configuracion['ManejaBultos'])='S')then
        begin
          comboboxbultos.ItemIndex:=0;
        //scrollbox1
        end;
        if(descuentos.Count=0)and (tarifas.Count=0)then
          begin
              editbotonmas.Enabled:= false;
          end
        else
          begin
              editbotonmas.Enabled:= true;
          end;
        end
        else
        begin
        editbotonmas.Enabled:= false;
        LabelPrecio.Text:= 'Precio:';
        edittarifa.Text :=  formatearnumero(configuracion.items['NumDecPre'],'0');
        editdes.Text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
        descuentos:=articulo.obtenerdescuento(datosart.items['codigo'],serie.items['cliente']);


        if (Uppercase(configuracion.items['IvaPrecios']) = 'S') and (uppercase(serie.items['ConIva'])='S')   then
        BEGIN
        encontrado:=false;
        i:=1;
        while((not encontrado) and (i<7)) do
        begin
        if(configuracion.items['VerTarifa'+inttostr(i)]='S') THEN
        begin
              tarifas.add(i,DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),datosart.items['t'+inttostr(i)]));
              conttarifa:=i;
              encontrado:=true;
              LabelPrecio.Text := 'PrecioT'+inttostr(i)+':';
              edittarifa.text:=tarifas.Items[i];
              editbotonmas.Enabled:= true;
        end
        else
            i:=i + 1;
        end;

      END
      ELSE
      BEGIN
      encontrado:=false;
      i:=1;
        while((not encontrado) and (i<7)) do
        begin
        if(configuracion.items['VerTarifa'+inttostr(i)]='S') THEN
        begin
              tarifas.add(i,formatearnumero(configuracion.items['NumDecPre'],datosart.items['t'+inttostr(i)]));
              conttarifa:=i;
              encontrado:=true;
              LabelPrecio.Text := 'PrecioT'+inttostr(i)+':';
              edittarifa.text:=tarifas.Items[i];
              editbotonmas.Enabled:= true;
        end
        else
            i:=i + 1;
        end;
      END;









        if descuentos.count <> 0 then
        begin
        if strtofloat(descuentos.items['precio'])<>0 then
        begin
          if (Uppercase(configuracion.items['IvaPrecios']) = 'S') and (uppercase(serie.items['ConIva'])='S')   then
        begin
          edittarifa.text:=DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),descuentos.items['precio']);
          tarifas.add(7,DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),descuentos.items['precio']));
        end
        else
        begin
        edittarifa.text:=formatearnumero(configuracion.items['NumDecPre'],descuentos.items['precio']);
        tarifas.add(7,formatearnumero(configuracion.items['NumDecPre'],descuentos.items['precio']));
        end;
        labelprecio.text:='PrecioD:';

        conttarifa:=7;
        editbotonmas.Enabled:= true;
        end;



        editdes.text:=formatearnumero(configuracion.items['NumDecPorDes'],descuentos.items['des']);


        end;
        labelStock.Text:='Stock Art�culo: '+formatearnumero(configuracion.items['NumDecCan'],datosart.Items['stock'])+' '+datosart.Items['unidad'];
        labelunds.Text:=datosart.Items['unidad'];

        editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],'1');
        if(UPPERCASE(configuracion['ManejaBultos'])='S')then
        begin
          comboboxbultos.ItemIndex:=0;
        //scrollbox1
        end;


        end;
  end

  else
    begin
      labelStock.Text:='Stock Art�culo: '+formatearnumero(configuracion.items['NumDecCan'],'0')+' UNDS.';
      editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],'0');
      labelprecio.text:='Precio:';
      labelunds.Text:='UNDS';
      edittarifa.Text:=formatearnumero(configuracion.items['NumDecPre'],'0');
      editbotonmas.Enabled:=false;
      editimporte.Text:=formatearnumero(configuracion.items['NumDecTot'],'0');
      editdes.Text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
      descripcion.Text:='';
      editcodigo.Text:='';

      if(UPPERCASE(serie.items['conIva'])='S') then
      BEGIN

        comboboxiva.ItemIndex:=0;

      END;
      if(UPPERCASE(configuracion['ManejaBultos'])='S')then
      begin
       comboboxbultos.ItemIndex:=0;
        //scrollbox1
      end;
      editbulto.Text:='0';
      alerta(self,'C�digo de art�culo no encontrado');
    end;









abortarchange:=false;
end;

procedure TFormupdatelineaped.EditdesChange(Sender: TObject);
var descuento:string;
    calculo:double;
    cantidad:string;
    precio:string;
begin
descuento:=formatearnumero(configuracion.items['NumDecPorDes'],editdes.text);
if(descuento='')then
begin
  editdes.text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
  descuento:='0';
end
else
   editdes.text:=descuento;
   if(editcantidad.Text='') then
    begin
      cantidad:='0';
    end
    else
      cantidad:=editcantidad.Text;
   if(edittarifa.Text='') then
   begin
      precio:='0';
   end
   else
      precio:=edittarifa.Text;


   calculo:=(strtofloat(cantidad))* (strtofloat(precio)-(strtofloat(precio)*((strtofloat(descuento))/100 )));

   editimporte.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(calculo));


end;


procedure TFormupdatelineaped.EditTARIFAChange(Sender: TObject);
 var precio:string;
     calculo:double;
     cantidad:string;
     descuento:string;
begin
precio:=formatearnumero(configuracion.items['NumDecPre'],edittarifa.text);
if(precio='')then
begin
  precio:='0';
  edittarifa.text:=formatearnumero(configuracion.items['NumDecPre'],'0');
end
else
   edittarifa.text:=precio;

      if(editdes.Text='') then
    begin
      descuento:='0';
    end
    else
      descuento:=editdes.Text;
   if(editcantidad.Text='') then
   begin
      cantidad:='0';
   end
   else
      cantidad:=editcantidad.Text;



   calculo:=(strtofloat(cantidad))* (strtofloat(precio)-(strtofloat(precio)*(strtofloat(descuento)/100 )));
//calculo:=strtofloat(precio)*strtofloat(editcantidad.text);
   editimporte.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(calculo));
end;

procedure TFormupdatelineaped.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 if cerrar then
 begin
 nueva:=true;
 padre.show;
 end;

 cerrar:=true;
 Action:= TCloseAction.cahide;
end;

procedure TFormupdatelineaped.FormCreate(Sender: TObject);
begin

pedido:=Tpedido.create;
pedido.InicializarBotonera(panel2,2,20);
configuracion:=tdictionary<string,variant>.create;
serie:=tdictionary<string,string>.create;
tarifas:=tdictionary<integer,string>.create;
descuentos:=tdictionary<string,string>.create;
articulo := tarticulos.Create;
datosart:=tdictionary<string,variant>.create;
datoslineaped:=tdictionary<string,string>.create;
conttarifa:=0;
abortarchange:=false;
OnVirtualKeyboardShown:=TTecladovirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladoVirtual.FormVirtualKeyboardHidden;
nueva:=true;
cerrar:=true;



end;

procedure TFormupdatelineaped.FormShow(Sender: TObject);
 var i:integer;
     iva:tdictionary<string,integer>;
     caso:integer;
begin
if nueva then
begin
i:=0;
  configuracion := pedido.obtenerConfiguracionPed();
  iva:=tdictionary<string,integer>.create;
  iva.Add(configuracion.Items['iva1'],0);
  iva.Add(configuracion.Items['iva2'],1);
  iva.Add(configuracion.Items['iva3'],2);
  editbotonmas.Enabled:=false;

  if DatosArt.count <> 0 then
     datosart.clear;
   if Datoslineaped.count <> 0 then
     datoslineaped.clear;
  if descuentos.count <> 0 then
     descuentos.clear;
  if tarifas.count <> 0 then
     tarifas.clear;

  if serie.count <> 0 then
     serie.clear;
  if comboboxiva.count <> 0 then
     comboboxiva.clear;
  if comboboxbultos.count <> 0 then
      comboboxbultos.clear;



  serie := pedido.obtenerSerielinea(idlineaped);

  datoslineaped := pedido.obtenerdatoslineaped(idlineaped);

  labelped.text:='L�nea de Venta, Ped: '+serie.items['serie']+'-'+serie.items['numero']+'/'+serie.items['fecha'];


  {
  abortarchange:=true;
  editcodigo.Text:='-';
  abortarchange:=false;
  }
  {
  labelunds.Text:='UNDS';

  labelStock.Text:='Stock Art�culo: '+formatearnumero(configuracion.items['NumDecCan'],'0')+' UNDS.';
  editcantidad.text:=formatearnumero(configuracion.items['NumDecCan'],'0');
  labelprecio.text:='Precio:';
  edittarifa.Text:=formatearnumero(configuracion.items['NumDecPre'],'0');
  editbotonmas.Enabled:=false;
  editimporte.Text:=formatearnumero(configuracion.items['NumDecTot'],'0');
  editdes.Text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
  descripcion.text:='';
   }

  if(UPPERCASE(serie.items['conIva'])='N') then
    begin
      labelIVA.FontColor:= talphacolorrec.red;

      labelIVA.Width:=82;
      labelIVA.TEXT:='(*) Sin IVA';
      comboboxiva.visible:=false;

    end
    else
      begin
      comboboxiva.Items.Add(configuracion.items['iva1']);
      comboboxiva.Items.Add(configuracion.items['iva2']);
      comboboxiva.Items.Add(configuracion.items['iva3']);

      caso:=iva.Items[datoslineaped.items['iva']];
      case caso of
       0: begin
               comboboxiva.ItemIndex:=0;
          end;
       1: begin
                comboboxiva.ItemIndex:=1;
          end;
       2 :begin
                comboboxiva.ItemIndex:=2;
          end;
      end;

       comboboxiva.Visible:=true;
      labelIVA.Width:=33;
      labelIVA.TEXT:='IVA';
      labelIVA.FontColor:= talphacolorrec.Black;
      end;


    if(UPPERCASE(configuracion['DtoLineas'])='N')then
    paneldes.Visible:=false
  else
     paneldes.Visible:=true;
  if(UPPERCASE(configuracion['PCambioDtoLinea'])='N')then
    editdes.CanFocus:=false
    else
    editdes.CanFocus:=true;
  if(UPPERCASE(configuracion['PCambioDePrecio'])='N')then
    edittarifa.CanFocus:=false
    else
    edittarifa.CanFocus:=true;
  if(UPPERCASE(configuracion['ManejaBultos'])='N')then
    begin
    panelmanejabulto.visible:=false;
    end
    else
    begin
        panelmanejabulto.visible:=true;
       editbulto.text:=datoslineaped.Items['bultos'];
       comboboxbultos.items.add('Bultos');
       comboboxbultos.ItemIndex:=0;
       pedido.obtenerLiteralesBultos(queryBultos);
       while not queryBultos.eof do
       BEGIN
         i:=i+1;
        comboboxbultos.items.add(querybultos.fieldbyname('texto').asstring);
        if querybultos.fieldbyname ('texto').asstring = datoslineaped.Items['literalBultos'] then
             comboboxbultos.ItemIndex:=i;
        queryBultos.next;


       END;
    end;


    abortarchange:=true;
    editcodigo.Text:=datoslineaped.items['articulo'];
    abortarchange:=false;


    codigoartini:=datoslineaped.items['articulo'];
    cantidadini:=strtofloat(datoslineaped.items['cantidad']);
     if datoslineaped.items['articulo']<>'' then
        datosart:=articulo.CosultarArticulosCompletable(datoslineaped.items['articulo']);


     editcantidad.Text:=datoslineaped.Items['cantidad'];
     labelprecio.Text:='Precio:';
     edittarifa.Text:=datoslineaped.Items['precio'];
     editdes.Text:=datoslineaped.Items['pdto'];
     descripcion.Text:=datoslineaped.Items['descripcion'];
     lote:='N';
  if datosart.count<>0 then
  begin
     //lote:=datosart.items['lote'];
     labelStock.Text:='Stock Art�culo: '+formatearnumero(configuracion.items['NumDecCan'],floattostr(strtofloat(datosart.Items['stock'])+cantidadini))+' '+datosart.Items['unidad'];
    if UpperCase(configuracion.Items['PCambioTarifa'])= 'S' then
    begin
      if (Uppercase(configuracion.items['IvaPrecios']) = 'S') and (uppercase(serie.items['ConIva'])='S')   then
      BEGIN
        for i := 1 to 6 do
         begin
          if(configuracion.items['VerTarifa'+inttostr(i)]='S') THEN
                begin
                tarifas.add(i,DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),datosart.items['t'+inttostr(i)]));
                end;
          end;
      END
      ELSE
      BEGIN
          for i := 1 to 6 do
           begin
            if(configuracion.items['VerTarifa'+inttostr(i)]='S') THEN
                  begin
                  tarifas.add(i,formatearnumero(configuracion.items['NumDecPre'],datosart.items['t'+inttostr(i)]));
                  end;
            end;
       END;

        descuentos:=articulo.obtenerdescuento(datosart.items['codigo'],serie.items['cliente']);
        if descuentos.count <> 0 then
        begin
        if strtofloat(descuentos.items['precio'])<>0 then
         begin
            if (Uppercase(configuracion.items['IvaPrecios']) = 'S') and (uppercase(serie.items['ConIva'])='S')   then
            begin
              //edittarifa.text:=DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),descuentos.items['precio']);
              tarifas.add(7,DescontarIvaPrecio(configuracion.items['NumDecPre'],strtoint(datosart.items['iva']),descuentos.items['precio']));
            end
            else
            begin
            //edittarifa.text:=formatearnumero(configuracion.items['NumDecPre'],descuentos.items['precio']);
                  tarifas.add(7,formatearnumero(configuracion.items['NumDecPre'],descuentos.items['precio']));
            end;
         end;




        end;
      if tarifas.Count<> 0 then
         editbotonmas.Enabled:=true;
      conttarifa:=0;

    end;

    end;
  nueva:=false;
end;

end;

procedure TFormupdatelineaped.SearchEditButton2MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
 begin


 formlistarart.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
      editcodigo.Text:=formlistarart.seleccionado;
      //formlistarart.Hide;
      end;


    end);




end;

 procedure TFormupdatelineaped.msgyes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
 var DatosLineaPednueva:tdictionary<string,string>;

begin
DatosLineaPednueva:=tdictionary<string,string>.create;
if editcodigo.Text = ''  then
  begin
    if Uppercase(configuracion.Items['PermitirSinCodigo']) ='S' then
    begin



      datoslineapednueva.Add('codigo',editcodigo.Text);
      //datoslineaped.Add('IvaPrecios',configuracion.Items['IvaPrecios']);
            if(UPPERCASE(serie.items['conIva'])='S') then
      begin
          datosLineaPednueva.Add('iva',comboboxiva.Selected.Text);
      end
      else
      begin
          if(datosart.count <> 0) then
          begin
              datosLineaPednueva.Add('iva',datosart.items['iva']);
          end
          else
               datosLineaPednueva.Add('iva',configuracion.items['iva1']);
      end;
      datoslineapednueva.Add('precio',edittarifa.text);
      datoslineapednueva.Add('descuento',editdes.text);
            if(UPPERCASE(configuracion['ManejaBultos'])='S')then
      begin
        datoslineapednueva.Add('bultos',editbulto.text);
        datoslineapednueva.Add('LiteralBultos',comboboxbultos.selected.Text);
      end
      else
      begin
        datoslineapednueva.Add('bultos','0');
        datoslineapednueva.Add('LiteralBultos','');
      end;
      datoslineapednueva.add('descripcion',descripcion.Text);
      datoslineapednueva.Add('cantidad',editcantidad.Text);
      datoslineapednueva.Add('importe',editimporte.text);
      datoslineapednueva.Add('unidades',labelunds.text);
      pedido.updateLineaPed(datoslineapednueva,idlineaped);
      //pedido.reiniciarlotes(idlineaped);


      close;
      //llamada a a�adir linea
      //cerrar formulario



    end
    else
    begin
      alerta(self,'No permitido la venta sin codigo');
    end;
  end
  else
  begin
      datoslineapednueva.Add('codigo',editcodigo.Text);
      //datoslineaped.Add('IvaPrecios',configuracion.Items['IvaPrecios']);
      if(UPPERCASE(serie.items['conIva'])='S') then
      begin
          datosLineaPednueva.Add('iva',comboboxiva.Selected.Text);
      end
      else
      begin
          if(datosart.count <> 0) then
          begin
              datosLineaPednueva.Add('iva',datosart.items['iva']);
          end
          else
               datosLineaPednueva.Add('iva',configuracion.items['iva1']);
      end;
      datoslineapednueva.Add('precio',edittarifa.text);
      datoslineapednueva.Add('descuento',editdes.text);
      if(UPPERCASE(configuracion['ManejaBultos'])='S')then
      begin
        datoslineapednueva.Add('bultos',editbulto.text);
        datoslineapednueva.Add('LiteralBultos',comboboxbultos.selected.Text);
      end
      else
      begin
        datoslineapednueva.Add('bultos','0');
        datoslineapednueva.Add('LiteralBultos','');
      end;
      datoslineapednueva.add('descripcion',descripcion.Text);
      datoslineapednueva.Add('cantidad',editcantidad.Text);
      datoslineapednueva.Add('importe',editimporte.text);
      datoslineapednueva.Add('unidades',labelunds.text);
      pedido.updateLineaPed(datoslineapednueva,idlineaped);
      {
      if codigoartini <> editcodigo.text then
      begin
      pedido.reiniciarlotes(idlineaped);
      if(uppercase(datosart.items['lote'])='S') then
        begin
          pedido.mostrarAsignarLote(idlineaped,padre);
          nueva:=true;
          cerrar:=false;
          close;
        end
        else
          close;
      end
      else
      begin
      if(uppercase(lote)='S') and (cantidadini<>strtofloat(editcantidad.text)) then
        begin
          pedido.mostrarAsignarLote(idlineaped,padre);
          nueva:=true;
          cerrar:=false;
          close;
        end
        else
          close;



      end;
       }

       close;

  end;




 end;
 procedure TFormupdatelineaped.Panel2Resize(Sender: TObject);
begin
 pedido.InicializarBotonera(panel2,2,20);
end;

procedure TFormupdatelineaped.msgno (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
 begin
   exit();
 end;


end.
