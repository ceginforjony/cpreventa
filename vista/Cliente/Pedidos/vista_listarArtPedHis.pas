unit vista_listarArtPedHis;

interface
           {$ZEROBASEDSTRINGS ON}
uses
    System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid, articulo,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,vista_buscarArticulo,funciones,
  FMX.Controls.Presentation, FMX.Grid.Style, FMX.ScrollBox;


type
  TFormListarPedHistorial = class(TForm)
    ToolBarArticulo: TToolBar;
    LabelArticulos: TLabel;
    botonAtras: TButton;
    PanelGrid: TPanel;
    gridarticulo: TStringGrid;
    PanelBotones: TPanel;
    Botonseleccionar: TButton;
    Icoselec: TImage;
    Label1: TLabel;
    Carga: TAniIndicator;
    ClientDataSet1: TClientDataSet;
    BindSourceDB1: TBindSourceDB;
    DataSetProvider1: TDataSetProvider;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PanelBuscador: TPanel;
    botonabajo: TButton;
    Image6: TImage;
    botonarriba: TButton;
    Image9: TImage;
    botonbusqueda: TButton;
    Image1: TImage;
    botonderecha: TButton;
    Image10: TImage;
    botonizquierda: TButton;
    Image11: TImage;
    botonnext: TButton;
    Image8: TImage;
    botonrefresh: TButton;
    Image7: TImage;

    procedure FormCreate(Sender: TObject);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonseleccionarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PanelBotonesResize(Sender: TObject);
    procedure gridarticuloDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
    procedure gridarticuloCellDblClick(const Column: TColumn;
      const Row: Integer);
    procedure botonizquierdaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonderechaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    { Private declarations }
      articulo: tarticulos;
      queryArticulo: TSQLQuery;
      busqueda: TDictionary<String,String>;
      ordenacion:Tdictionary<string,string>;

  public
     cliente:integer;
   seleccionado:string;
   codigo: string;
     descripcion: string;
    { Public declarations }
  end;

var
  FormListarPedHistorial: TFormListarPedHistorial;

implementation

{$R *.fmx}

procedure TFormListarPedHistorial.botonabajoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
clientdataset1.Next;
end;

procedure TFormListarPedHistorial.botonarribaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
clientdataset1.Prior;
end;

procedure TFormListarPedHistorial.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

  close;


end;

procedure TFormListarPedHistorial.botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;

begin
       //showmessage('entro en busqueda');


    formbusquedaart.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
          articulo.copiardiccionario(formbusquedaart.busqueda,busqueda);


          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not articulo.buscar(clientdataset1,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                      botonnext.visible:=false;
                             botonnext.visible:=false;
                    orden:=tdictionary<string,string>.create;
                    orden.add('column','Nombre');
                    orden.add('tipo','asc');

                    articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,cliente);

                    DataSetProvider1.DataSet:=queryArticulo;
                    clientdataset1.Close;
                    ClientDataSet1.open;

                    articulo.inicializarGrid(gridarticulo,ClientDataSet1);
                    //Application.processmessages;
                    articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
                    articulo.mostrartarifas(gridarticulo,ClientDataSet1);
                    ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);
                    ordenacion.items['Nombre']:='desc';



                      //showmessage(inttostr(queryarticulo.RecordCount));



              end;

      end;

    end);


end;

procedure TFormListarPedHistorial.botonderechaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridarticulo.ScrollBy( gridarticulo.Columns[0].Width,0);
end;

procedure TFormListarPedHistorial.botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

      if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=275;
      end;

   //articulo.inicializarGrid(gridarticulo);
   //articulo.sizecolgrid(gridarticulo,1,200);








end;

procedure TFormListarPedHistorial.botonizquierdaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridarticulo.ScrollBy( -1* gridarticulo.Columns[0].Width,0);
end;

procedure TFormListarPedHistorial.botonnextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
     if(not articulo.buscar(clientdataset1,busqueda,false)) then
       showmessage('error inexperado');
end;

procedure TFormListarPedHistorial.botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
 var orden:tdictionary<string,string>;
  begin
     carga.visible:=true;
   orden:=tdictionary<string,string>.create;
    orden.add('column','Nombre');
    orden.add('tipo','asc');



  if(busqueda.count <> 0) then
     busqueda.Clear;


    articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,cliente);

  DataSetProvider1.DataSet:=queryArticulo;
  clientdataset1.Close;
  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;

  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
  //Application.processmessages;
  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
  articulo.mostrartarifas(gridarticulo,ClientDataSet1);
  ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);
  ordenacion.items['Nombre']:='desc';
  carga.Visible:=false;


    //articulo.inicializarGrid(gridarticulo);
    //articulo.sizecolgrid(gridarticulo,1,200);


//application.ProcessMessages;


  botonnext.visible:=false;
//carga.Enabled:=False;


end;

procedure TFormListarPedHistorial.BotonseleccionarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

      seleccionado:=clientdataset1.fieldbyname('codigo').AsString;
      modalresult:= mrOK;
      close;





end;

procedure TFormListarPedHistorial.FormClose(Sender: TObject; var Action: TCloseAction);
begin

clientdataset1.close;
action:=tcloseaction.cahide;

end;

procedure TFormListarPedHistorial.FormCreate(Sender: TObject);
begin
  application.ProcessMessages;
  busqueda:= TDictionary<string,string>.create;
  articulo:= tarticulos.Create;
  articulo.InicializarBotonera(panelbotones,2,3);
  articulo.ReescalarLetraLabel(self);
   // No hace falta crear memoria al queryarticulo puesto que se crea al hacer la copia dentro de listar articulo ( SQL:=MODELOARTICULO.SQL)
  //queryArticulo:= TSQLQuery.Create(application);
  //Application.processmessages;








end;

procedure TFormListarPedHistorial.FormShow(Sender: TObject);
 var orden:tdictionary<string,string>;

begin
   carga.Visible:=true;
      orden:=tdictionary<string,string>.create;
      orden.add('column','Nombre');
      orden.add('tipo','asc');

    if busqueda.count <> 0 then
        busqueda.clear;
     if(Trim(codigo)<>'') then
     begin

      busqueda.Add('col','C�digo');
      busqueda.Add('accion','filtro');
      busqueda.Add('tipo','Es igual a');
      busqueda.Add('texto',Trim(codigo));
      busqueda.Add('fecha','N');

     end
     else
     begin
       if(Trim(descripcion)<> '') then
       begin
              busqueda.Add('col','Nombre');
              busqueda.Add('accion','filtro');
              busqueda.Add('tipo','Contenido en');
              busqueda.Add('texto',Trim(descripcion));
              busqueda.Add('fecha','N');
       end;
     end;


  //showmessage('cliente'+inttostr(cliente));
  articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,cliente);

  DataSetProvider1.DataSet:=queryArticulo;

  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;

  articulo.inicializarGrid(gridarticulo,clientdataset1);
  //Application.processmessages;
  articulo.sizecolgrid(gridarticulo,1,290,clientdataset1);
  articulo.mostrartarifas(gridarticulo,clientdataset1);
  ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);
  ordenacion.items['Nombre']:='desc';
  carga.Visible:=false;

end;

procedure TFormListarPedHistorial.gridarticuloCellDblClick(
  const Column: TColumn; const Row: Integer);
  var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin
       articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,cliente);

  DataSetProvider1.DataSet:=queryArticulo;
  clientdataset1.Close;
  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;

  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
  //Application.processmessages;
  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
   articulo.mostrartarifas(gridarticulo,ClientDataSet1);



  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

                  articulo.ListarArticuloshisPed(queryarticulo, busqueda,orden,cliente);


                  DataSetProvider1.DataSet:=queryArticulo;
                  clientdataset1.Close;
                  Application.processmessages;
                  Application.processmessages;
                  ClientDataSet1.active:=true;

                  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
                  //Application.processmessages;
                  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
                  articulo.mostrartarifas(gridarticulo,ClientDataSet1);
                  botonnext.Visible:=false;



end;




   carga.visible:=false;
end;

procedure TFormListarPedHistorial.gridarticuloDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridarticulo.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

procedure TFormListarPedHistorial.PanelBotonesResize(Sender: TObject);
begin
  articulo.InicializarBotonera(panelbotones,2,3);
end;

end.
