unit vista_NuevoPedido;

interface
          {$ZEROBASEDSTRINGS ON}
uses
     System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,pedido, FMX.Colors, FMX.Edit,
  FMX.ListBox,funciones,FMX.Calendar, FMX.VirtualKeyboard,FMX.Platform,cliente, FMX.Memo, FMX.DateTimeCtrls, System.Math.Vectors, FMX.Controls3D,
  FMX.Layers3D, FMX.ScrollBox, FMX.Controls.Presentation,
  FMX.Memo.Types;


type
  Tformnuevoped = class(TForm)
    ToolBarnuevoped: TToolBar;
    Labelped: TLabel;
    botonAtras: TButton;
    ScrollBox1: TScrollBox;
    Panel1: TPanel;
    Panel3: TPanel;
    Labelclt: TLabel;
    EditClt: TEdit;
    Panel8: TPanel;
    LabelNum: TLabel;
    EditNum: TEdit;
    Panel2: TPanel;
    Panel4: TPanel;
    Labelserie: TLabel;
    Labelfecha: TLabel;
    Editfecha: TEdit;
    botonAceptar: TButton;
    IcoAceptar: TImage;
    LabelAceptar: TLabel;
    BotonHistorico: TButton;
    IcoLotes: TImage;
    LabelHistorico: TLabel;
    BotonLinea: TButton;
    IcoLinea: TImage;
    LabelLineas: TLabel;
    Panel5: TPanel;
    Panel7: TPanel;
    Rectangle2: TRectangle;
    Label13: TLabel;
    Label14: TLabel;
    Panel9: TPanel;
    botoncomentarios: TColorButton;
    Label19: TLabel;
    EditDesPor: TEdit;
    EditDes: TEdit;
    ComboBoxSerie: TComboBox;
    LayautDesglose: TLayout;
    Rectangle6: TRectangle;
    Panel11: TPanel;
    Rectangle7: TRectangle;
    Layout15: TLayout;
    Button1: TButton;
    Layout16: TLayout;
    Rectangle8: TRectangle;
    Label16: TLabel;
    Layout17: TLayout;
    Layout18: TLayout;
    Layout19: TLayout;
    Labeliva3: TLabel;
    Labeliva3imp: TLabel;
    Layout20: TLayout;
    Labeliva2: TLabel;
    Labeliva2imp: TLabel;
    Layout21: TLayout;
    Labeliva1: TLabel;
    Labeliva1imp: TLabel;
    Layout22: TLayout;
    Labeltotaliva: TLabel;
    Labeltotalivaimp: TLabel;
    Layout23: TLayout;
    Labelrec2: TLabel;
    Labelrec2imp: TLabel;
    Layout24: TLayout;
    Labelrec1: TLabel;
    Labelrec1imp: TLabel;
    Layout25: TLayout;
    Labelrec3: TLabel;
    Labelrec3imp: TLabel;
    Layout26: TLayout;
    Labeltotalrec: TLabel;
    Labeltotalrecimp: TLabel;
    Layout27: TLayout;
    Labeltotalimp: TLabel;
    Labelimptotal: TLabel;
    Panel10: TPanel;
    Label1: TLabel;
    Editfechaprev: TEdit;
    Layoutcomentarios: TLayout;
    Rectangle3: TRectangle;
    Panel12: TPanel;
    Rectangle4: TRectangle;
    Layout2: TLayout;
    Button2: TButton;
    Layout3: TLayout;
    Rectangle5: TRectangle;
    Label2: TLabel;
    Layout4: TLayout;
    Button3: TButton;
    Memocomentarios: TMemo;
    Rectangle9: TRectangle;
    Layoutcalendario: TLayout;
    Calendar1: TCalendar;
    botoncalendario: TButton;
    Image2: TImage;
    Rectangle1: TRectangle;
    Layout1: TLayout;
    Panel6: TPanel;
    BotonDetalles: TColorButton;
    Label12: TLabel;
    Layout8: TLayout;
    Layout9: TLayout;
    LabelBruto: TLabel;
    Label6: TLabel;
    Layout3D1: TLayout3D;
    Layout10: TLayout;
    LabelImp: TLabel;
    Label7: TLabel;
    Layout11: TLayout;
    LabelTot: TLabel;
    Labeltotal: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBoxSerieChange(Sender: TObject);
    procedure BotonDetallesMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonLineaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonHistoricoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EditDesPorChange(Sender: TObject);
    procedure EditDesChange(Sender: TObject);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botoncomentariosClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Panel2Resize(Sender: TObject);
    procedure Button2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Button3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure EditfechaprevChange(Sender: TObject);
    procedure botoncomentariosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botoncalendarioMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Calendar1DateSelected(Sender: TObject);
  private

  queryContador: TSQLQuery;
  Ped2: tpedido;
  clt:tcliente;
  serie:tdictionary<string,string>;
  totales:tdictionary<string,string>;
  datosPed:tdictionary<string,variant>;
  detalleactivado:boolean;
  configuracion:tdictionary<string,variant>;
  abortarchange:boolean;
   idPed:integer;
   cerrar:boolean;
   procedure reiniciartotales();
   procedure msgyes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
   procedure msgno (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
   procedure msgaceptar (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
    procedure msgdescuadre (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);

    { Private declarations }
  public
  codigoclt:string;
  { Public declarations }
  end;
  var formnuevoped:tformnuevoped;

implementation

{$R *.fmx}

procedure Tformnuevoped.botonAceptarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  if (idped=-1) or   (not ped2.ExisteLineasPed(idped))  then
    alerta(self,'Operaci�n Abortada. El pedido no dispone de l�neas')

  else
  //if ped2.confirmarlineasconlotes(idped)<> -1 then
  //   confirmar(self,'Exite descuadre de lote. �Desea usted resolver el descuadre?',msgdescuadre,msgaceptar,nil,'Confirmar','Si','No')
  //else
  close;




end;

procedure Tformnuevoped.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin


  if Layoutcalendario.Visible then
  begin
  layoutcalendario.Visible:=false;
  exit;

  end;
   if LayautDesglose.Visible then
  begin
    LayautDesglose.Visible:=false;
    exit;

  end;
   if Layoutcomentarios.Visible then
  begin
    Layoutcomentarios.Visible:=false;
    if idped=-1 then

    memocomentarios.Text:=''
    else
      memocomentarios.Text:=ped2.obtenercomentarios(idped);

    exit;

  end;


 if idped<>-1  then
 begin
  confirmar(self,'�Desea usted cancelar el pedido?',msgyes,msgno,nil,'Confirmar','Si','No');
 end
 else
 close;

end;

procedure Tformnuevoped.BotonDetallesMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 LAYAUTDESGLOSE.Visible:=TRUE;
labeliva1.Text:='Imp IVA '+configuracion.Items['iva1']+'%:';
labeliva1imp.Text:=totales.Items['impIVA1'];
labeliva2.Text:='Imp IVA '+configuracion.Items['iva2']+'%:';
labeliva2imp.Text:=totales.Items['impIVA2'];
labeliva3.Text:='Imp IVA '+configuracion.Items['iva3']+'%:';
labeliva3imp.Text:=totales.Items['impIVA3'];
labeltotalivaimp.Text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])));
labelrec1.Text:='Imp REC '+configuracion.Items['rec1']+'%:';
labelrec1imp.Text:=totales.Items['impREC1'];
labelrec2.Text:='Imp REC '+configuracion.Items['rec2']+'%:';
labelrec2imp.Text:=totales.Items['impREC2'];
labelrec3.Text:='Imp REC '+configuracion.Items['rec3']+'%:';
labelrec3imp.Text:=totales.Items['impREC3'];
labeltotalrecimp.Text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
labelimptotal.Text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])+strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));



end;

procedure Tformnuevoped.BotonLineaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

  if (idped<>-1) and (ped2.ExisteLineasPed(idped)) then
  begin
    //MostrarLineasPed(idped);
    //showmessage('aqui no entra');
    ped2.mostrarlineasped(idped,self);
    cerrar:=false;
    close;
  end
  else
    begin
      if idPed =-1 then
      begin
        DatosPed.add('serie',comboboxserie.selected.text);
        DatosPed.add('numero',editnum.text);
        DatosPed.add('cliente',editclt.text);
        //DatosPed.add('fecha','');
        DatosPed.add('desPor',editdespor.text);
        DatosPed.add('des',editdes.text);
        datosped.add('comentarios',memocomentarios.text);
        datosped.add('fentrega',fechaamericana(editfechaprev.text));
        datosPed.add('fecha',fechaamericana(editfecha.text));

        comboboxserie.enabled:=false;
        idped:=ped2.CrearNuevoPed(DatosPed);
        ped2.MostrarAddLineaPed(idped,self);
        cerrar:=false;
        close;
      end
      else
      begin

          ped2.MostrarAddLineaPed(idped,self);
          cerrar:=false;
          close;
      end;
    end;


end;

procedure Tformnuevoped.BotonHistoricoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
{
if (idped<>-1) and ped2.comprobarlineasconlotes(idped) then
begin
  ped2.mostrarasignacionlotes(idped,self);
  cerrar:=false;
  close;
end
else
  alerta(self,'El pedido no contiene l�neas con lotes.');
 }

 ped2.mostrarhistoricoped(strtoint(editclt.text),self);


end;

procedure Tformnuevoped.botoncalendarioMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin


layoutcalendario.Visible:=true;


end;

procedure Tformnuevoped.botoncomentariosClick(Sender: TObject);
begin
 //application.ProcessMessages;
end;

procedure Tformnuevoped.botoncomentariosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin

 Layoutcomentarios.Visible:=true;



end;

procedure Tformnuevoped.Button1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
LAYAUTDESGLOSE.Visible:=FALSE;
end;

procedure Tformnuevoped.Button2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin


 if (idped=-1) then
 begin
  memocomentarios.text:='';
  Layoutcomentarios.Visible:=FALSE;
 end
 else
 begin
  memocomentarios.text:=ped2.obtenercomentarios(idped);
  Layoutcomentarios.Visible:=FALSE;
 end;


end;

procedure Tformnuevoped.Button3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

  if (idped=-1) then
 begin
  //memocomentarios.text:='';
  Layoutcomentarios.Visible:=FALSE;
 end
 else
  begin
  ped2.actualizarcomentarios(idped,memocomentarios.text);
  Layoutcomentarios.Visible:=FALSE;
  end;












end;

procedure Tformnuevoped.Calendar1DateSelected(Sender: TObject);
begin

  editfechaprev.Text:= formatdatetime('DD/MM/YYYY',calendar1.Date);
  layoutcalendario.Visible:=false;


end;

procedure Tformnuevoped.ComboBoxSerieChange(Sender: TObject);
begin

editnum.Text:=serie.Items[comboboxserie.selected.text];

end;

procedure Tformnuevoped.EditDesChange(Sender: TObject);
var descuento:string;
begin
if abortarchange then
    exit;
 descuento:=formatearnumero(configuracion.items['NumDecTot'],editdes.text);
 abortarchange:=true;
if(descuento='')then
begin

  editdes.text:=formatearnumero(configuracion.items['NumDecTot'],'0');

end
else
    begin
    editdes.text:=descuento;
    if(strtofloat(descuento)<>0) then
    editdespor.Text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
    end;

if idped <> -1 then
begin
ped2.modificardescuentos(idped,editdespor.Text,editdes.Text);
reiniciartotales();
end;




abortarchange:=false;
end;

procedure Tformnuevoped.EditDesPorChange(Sender: TObject);
var descuento:string;
begin
if abortarchange then
    exit;
 descuento:=formatearnumero(configuracion.items['NumDecPorDes'],editdespor.text);
 abortarchange:=true;
if(descuento='')then
begin

  editdespor.text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');

end
else
  begin
   editdespor.text:=descuento;
   if(strtofloat(descuento)<>0) then
    editdes.Text:=formatearnumero(configuracion.items['NumDecTot'],'0');
  end;
if idped <> -1 then
begin
ped2.modificardescuentos(idped,editdespor.Text,editdes.Text);
reiniciartotales();
end;




abortarchange:=false;
end;

procedure Tformnuevoped.EditfechaprevChange(Sender: TObject);
begin

 if idped <> -1 then
begin
ped2.actualizarfechaprev(idped,fechaamericana(editfechaprev.text));
//reiniciartotales();
end;







end;

procedure Tformnuevoped.FormClose(Sender: TObject; var Action: TCloseAction);
begin

   //free;
    //form19.Show;

    //application.ProcessMessages;

  //Action:= TCloseAction.caNone;
  //ped2.Free;

  //application.ProcessMessages;




  if cerrar then
  begin
 idped:=-1;
 ped2.regresarpedclt();
  end;
  cerrar:=true;
 Action:= TCloseAction.cahide;
 //free;
end;

procedure Tformnuevoped.FormCreate(Sender: TObject);
begin
Ped2:=tpedido.Create;
clt:=tcliente.create;
serie:=tdictionary<string,string>.create;
totales:=tdictionary<string,string>.create;
DatosPed:=tdictionary<string,variant>.create;
detalleactivado:=false;
abortarchange:=false;
ped2.InicializarBotonera(panel2,3,15);
idped:=-1;
OnVirtualKeyboardShown:=TTecladovirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladoVirtual.FormVirtualKeyboardHidden;
cerrar:=true;
//querycontador:=TSQLQuery.Create(self);

end;
procedure Tformnuevoped.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  FService : IFMXVirtualKeyboardService;

begin
  if Key = vkHardwareBack then
  begin
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
    if (FService <> nil) and (TVirtualKeyboardState.vksVisible in FService.VirtualKeyBoardState) then
    begin
      // Back button pressed, keyboard visible, so do nothing...
    end else
    begin

        botonAtrasMouseUp(botonatras,TMouseButton.mbLeft,Shift, 0, 0);
        // They changed their mind, so ignore the Back button press...
        Key := 0;

    end;
  end;
end;

procedure tformnuevoped.reiniciartotales();
begin
 if(idped <> -1) then
begin

    botondetalles.Enabled:=true;
    totales:=ped2.obtenertotales(idped);
    labelbruto.text:=formatearnumero(configuracion.items['NumDecTot'], totales.Items['bruto']);
    labelimp.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])+strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
    //labelimp.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])+strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
    labeltotal.text:=formatearnumero(configuracion.items['NumDecTot'],totales.Items['total']);
end;
end;

procedure Tformnuevoped.FormShow(Sender: TObject);


begin
if idped=-1 then
begin
memocomentarios.text:='';
configuracion := ped2.obtenerConfiguracionPed();
ped2.ObtenerContadoresPed(Querycontador);
queryContador.First;
if ComboBoxSerie.Count <> 0 then
    comboboxserie.Clear;
if Serie.Count <> 0 then
    serie.Clear;
if datosped.Count <> 0 then
    datosped.Clear;

while not querycontador.Eof do
begin

  comboboxserie.Items.Add(querycontador.FieldbyName('serie').AsString);//:= 'hola'+inttostr(i);//querycontador.ParamByName('serie').AsString;
  serie.Add(querycontador.FieldByName('serie').AsString,querycontador.FieldbyName('actual').AsString);
  querycontador.Next;
end;
comboboxserie.ItemIndex:=0;
editnum.text:= serie.Items[comboboxserie.selected.text];
editclt.Text:= codigoClt;
editfecha.text:=formatdatetime('DD/MM/YYYY',DATE);
editfechaprev.text:=formatdatetime('DD/MM/YYYY',DATE);
calendar1.Date:=DATE;
labelbruto.Text:=formatearnumero(configuracion.items['NumDecTot'],'0');
labelimp.text:=  formatearnumero(configuracion.items['NumDecTot'],'0');
labeltotal.Text:= formatearnumero(configuracion.items['NumDecTot'],'0');
editdes.Text:= formatearnumero(configuracion.items['NumDecTot'],'0');
editdespor.Text:=formatearnumero(configuracion.items['NumDecPorDes'],clt.obtenerdto(codigoClt));
if(configuracion.items['PCambioDtoCabecera'] = 'N') THEN
begin
     EditDesPor.Enabled := false;
     self.EditDes.Enabled := false;

end
else
begin
      EditDesPor.Enabled := true;
     self.EditDes.Enabled := true;
end;
botondetalles.Enabled:=false;
comboboxserie.enabled:=true;

end
else
  reiniciartotales();



//editfecha.Text:=formatdatetime('DD/MM/YYYY',DATE);


//EDITNUM.text:= comboboxserie.selected





end;

 procedure Tformnuevoped.msgyes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin


    ped2.eliminarped(idped);
    close;


end;
procedure Tformnuevoped.Panel2Resize(Sender: TObject);
begin
ped2.InicializarBotonera(panel2,3,15);
end;

procedure Tformnuevoped.msgno (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin

    exit;

end;
 procedure Tformnuevoped.msgaceptar (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin


    close;


end;
procedure Tformnuevoped.msgdescuadre (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin
  {
  ped2.mostrarasignacionlotes(idped,self);
  cerrar:=false;
  close;
   }
end;

end.
