unit vista_busquedaped;

interface
                {$ZEROBASEDSTRINGS ON}
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls,carga_inicial, FMX.ListBox,Generics.Collections, FMX.Layouts,funciones,controlador,
  FMX.Controls.Presentation;

type
  TFormbusquedaped = class(TForm)
    ToolBar1: TToolBar;
    Label1: TLabel;
    botoncancelar: TButton;
    ScrollBoxBusquedaArticulo: TScrollBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Label2: TLabel;
    ComboBoxbusqueda: TComboBox;
    Serie: TListBoxItem;
    Numero: TListBoxItem;
    Fecha: TListBoxItem;
    Total: TListBoxItem;
    Panel3: TPanel;
    Label3: TLabel;
    ComboBoxcadena: TComboBox;
    CONTENIDO: TListBoxItem;
    ESIGUAL: TListBoxItem;
    ComboBoxnumero: TComboBox;
    IGUAL: TListBoxItem;
    MENOR: TListBoxItem;
    MAYOR: TListBoxItem;
    DIFERENTE: TListBoxItem;
    Panel4: TPanel;
    Label4: TLabel;
    texto: TEdit;
    Panel5: TPanel;
    Checkaccion: TCheckBox;
    Panel6: TPanel;
    botonAceptar: TButton;
    CodCliente: TListBoxItem;
    NomCliente: TListBoxItem;
        procedure FormCreate(Sender: TObject);
    procedure ComboBoxbusquedaChange(Sender: TObject);
    procedure botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  controlador:tcontrolador;  { Private declarations }
  public
   busqueda: TDictionary<String,String>;
    { Public declarations }
  end;

var
  Formbusquedaped: TFormbusquedaped;

implementation

{$R *.fmx}

procedure  TFormbusquedaped.botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
 var
    accion: string;
    textoNum: double;
    Textoreplace: string;
    MiFecha:Tdatetime;

begin
     if checkaccion.IsChecked then
    begin
      accion:='busqueda';
    end
  else
      accion:='filtro';


  //busqueda.Add('col',comboboxbusqueda.Selected.Text);
  //busqueda.Add('accion',accion);
  if comboboxbusqueda.Selected.Tag = 0 then
    begin
    if texto.Text<>'' then
    begin
      busqueda.Add('col',comboboxbusqueda.Selected.Text);
      busqueda.Add('accion',accion);
      busqueda.Add('tipo',comboboxcadena.Selected.Text);
      busqueda.Add('texto',texto.Text);
      busqueda.add('fecha','N');
      modalresult:= mrOK;
      close;
    end
    else
      alerta(self,'El valor no puede estar vacio');

  end
  else
    begin
      if comboboxbusqueda.Selected.Text <> 'Fecha'  then
      begin

            textoreplace:= stringreplace(texto.text,'.',',',[rfReplaceAll]);

           if   TextToFloat(PChar(Textoreplace), textonum, fvExtended)then
           begin

                  busqueda.Add('col',comboboxbusqueda.Selected.Text);
                  busqueda.Add('accion',accion);
                  busqueda.Add('tipo',comboboxnumero.Selected.Text);
                  busqueda.Add('texto',texto.text);
                  busqueda.add('fecha','N');
                  modalresult:= mrOK;
                  close;
           end
           else
           begin
              alerta(self,'Valor incorrecto, debe introducir un formato num�rico para la opci�n introducida.');
              texto.text:='';
              //texto.setfocus;
           end;

      end
      else
      begin
          If (TryStrToDate(texto.text,mifecha)) then
          begin
               busqueda.Add('col',comboboxbusqueda.Selected.Text);
               busqueda.Add('accion',accion);
               busqueda.Add('tipo',comboboxnumero.Selected.Text);
               busqueda.Add('texto',FormatDateTime('yyyy-mm-dd',mifecha));
               busqueda.add('fecha','S');
               modalresult:= mrOK;
               close;
          end
          else
          begin
              alerta(self,'Fecha incorrecta. Formato o tiempo incorrecto: Asegurase de que el formato introducido sigue el patr�n: Dia/Mes/A�o.');
              texto.text:='';
          end;



      end;
    end;


end;

procedure  TFormbusquedaped.botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
       modalresult:= mrCancel;
      close;
end;

procedure TFormbusquedaped.ComboBoxbusquedaChange(Sender: TObject);
begin
   if comboboxbusqueda.Selected.Tag = 0 then
 begin
      comboboxnumero.Visible:=false;
      comboboxcadena.visible:=true;
      comboboxcadena.ItemIndex:=0;
      comboboxnumero.ItemIndex:=0;


 end
 else
 begin
      comboboxnumero.Visible:=true;
      comboboxcadena.visible:=false;
      comboboxcadena.ItemIndex:=0;
      comboboxnumero.ItemIndex:=0;


 end;









end;

procedure  TFormbusquedaped.FormClose(Sender: TObject; var Action: TCloseAction);
begin
action:=tcloseaction.caHide;
end;

procedure  TFormbusquedaped.FormCreate(Sender: TObject);
begin

busqueda:= tdictionary<string,string>.create;
OnVirtualKeyboardShown:=TTecladoVirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladovirtual.FormVirtualKeyboardHidden;
controlador:=tcontrolador.create;
controlador.ReescalarLetraLabel(self);
  {
comboboxbusqueda.ItemIndex:=0;
comboboxcadena.ItemIndex:=0;
comboboxnumero.ItemIndex:=0;
comboboxnumero.Visible:=false;
comboboxcadena.visible:=true;
}


end;

procedure  TFormbusquedaped.FormShow(Sender: TObject);
begin
 if busqueda.Count <> 0 then
    busqueda.Clear;
comboboxbusqueda.ItemIndex:=0;
comboboxcadena.ItemIndex:=0;
comboboxnumero.ItemIndex:=0;
comboboxnumero.Visible:=false;
comboboxcadena.visible:=true;
texto.Text:='';
 checkaccion.IsChecked:=false;




end;



end.
