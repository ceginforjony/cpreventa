unit vista_rectificarped;

interface
               {$ZEROBASEDSTRINGS ON}
uses
     System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,pedido, FMX.Colors, FMX.Edit,
  FMX.ListBox,funciones, FMX.VirtualKeyboard,FMX.Platform, FMX.Memo,
  FMX.Calendar, FMX.DateTimeCtrls, System.Math.Vectors, FMX.Controls3D,
  FMX.Layers3D, FMX.ScrollBox, FMX.Controls.Presentation, FMX.Memo.Types;

type
  TFormRecPed = class(TForm)
    ToolBarnuevoped: TToolBar;
    LabelPed: TLabel;
    botonAtras: TButton;
    ScrollBox1: TScrollBox;
    Panel1: TPanel;
    Panel3: TPanel;
    Labelclt: TLabel;
    EditClt: TEdit;
    Panel8: TPanel;
    LabelNum: TLabel;
    EditNum: TEdit;
    Panel2: TPanel;
    BotonHistorico: TButton;
    IcoLotes: TImage;
    Labelhistorico: TLabel;
    BotonLinea: TButton;
    IcoLinea: TImage;
    LabelLineas: TLabel;
    Panel4: TPanel;
    Labelserie: TLabel;
    Labelfecha: TLabel;
    Editfecha: TEdit;
    Panel5: TPanel;
    Panel7: TPanel;
    Rectangle2: TRectangle;
    Label13: TLabel;
    Label14: TLabel;
    EditDesPor: TEdit;
    EditDes: TEdit;
    Botoneliminar: TButton;
    Image1: TImage;
    Label1: TLabel;
    Editserie: TEdit;
    LayautDesglose: TLayout;
    Rectangle6: TRectangle;
    Panel11: TPanel;
    Rectangle7: TRectangle;
    Layout15: TLayout;
    Button1: TButton;
    Layout16: TLayout;
    Rectangle8: TRectangle;
    Label16: TLabel;
    Layout17: TLayout;
    Layout18: TLayout;
    Layout19: TLayout;
    Labeliva3: TLabel;
    Labeliva3imp: TLabel;
    Layout20: TLayout;
    Labeliva2: TLabel;
    Labeliva2imp: TLabel;
    Layout21: TLayout;
    Labeliva1: TLabel;
    Labeliva1imp: TLabel;
    Layout22: TLayout;
    Labeltotaliva: TLabel;
    Labeltotalivaimp: TLabel;
    Layout23: TLayout;
    Labelrec2: TLabel;
    Labelrec2imp: TLabel;
    Layout24: TLayout;
    Labelrec1: TLabel;
    Labelrec1imp: TLabel;
    Layout25: TLayout;
    Labelrec3: TLabel;
    Labelrec3imp: TLabel;
    Layout26: TLayout;
    Labeltotalrec: TLabel;
    Labeltotalrecimp: TLabel;
    Layout27: TLayout;
    Labeltotalimp: TLabel;
    Labelimptotal: TLabel;
    Panel10: TPanel;
    Label2: TLabel;
    Editfechaprev: TEdit;
    botoncalendario: TButton;
    Image2: TImage;
    Panel9: TPanel;
    botoncomentarios: TColorButton;
    Label19: TLabel;
    Layoutcalendario: TLayout;
    Calendar1: TCalendar;
    Layoutcomentarios: TLayout;
    Rectangle3: TRectangle;
    Panel12: TPanel;
    Rectangle4: TRectangle;
    Layout2: TLayout;
    Button2: TButton;
    Button3: TButton;
    Layout3: TLayout;
    Rectangle5: TRectangle;
    Label3: TLabel;
    Layout4: TLayout;
    Rectangle9: TRectangle;
    Memocomentarios: TMemo;
    Rectangle1: TRectangle;
    Layout1: TLayout;
    Panel6: TPanel;
    BotonDetalles: TColorButton;
    Label12: TLabel;
    Layout8: TLayout;
    Layout9: TLayout;
    LabelBruto: TLabel;
    Label6: TLabel;
    Layout3D1: TLayout3D;
    Layout10: TLayout;
    LabelImp: TLabel;
    Label7: TLabel;
    Layout11: TLayout;
    LabelTot: TLabel;
    Labeltotal: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonLineaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonHistoricoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotoneliminarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure EditDesChange(Sender: TObject);
    procedure EditDesPorChange(Sender: TObject);
    procedure BotonDetallesMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Panel2Resize(Sender: TObject);
    procedure EditfechaprevMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botoncomentariosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure EditfechaprevChange(Sender: TObject);
    procedure Calendar1DateSelected(Sender: TObject);
    procedure botoncalendarioMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Button3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure Button2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
  Ped2: tpedido;
  totales:tdictionary<string,string>;
  datosPed:tdictionary<string,string>;
  configuracion:tdictionary<string,variant>;
  abortarchange:boolean;
  nueva:boolean;

   cerrar:boolean;
   procedure reiniciartotales();
   procedure msgyes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
   procedure msgeliminaryes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
   procedure msgno (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
   procedure msgaceptar (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
    procedure msgdescuadre (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);

    { Private declarations }
  public
    idPed:integer;
    padre:tform; { Public declarations }
  end;

var
  FormRecPed: TFormRecPed;

implementation

{$R *.fmx}

procedure TFormRecPed.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin



  if Layoutcalendario.Visible then
  begin
  layoutcalendario.Visible:=false;
  exit;

  end;
   if LayautDesglose.Visible then
  begin
    LayautDesglose.Visible:=false;
    exit;

  end;
   if Layoutcomentarios.Visible then
  begin
    Layoutcomentarios.Visible:=false;

    memocomentarios.Text:=ped2.obtenercomentarios(idped);

    exit;

  end;










if not ped2.ExisteLineasPed(idped)  then
  confirmar(self,'El pedido no dispone de l�neas, �Deseas salir?.',msgyes,msgno,nil,'Confirmar','Si','No')
else
  {
  if ped2.confirmarlineasconlotes(idped)<> -1 then
     confirmar(self,'Exite descuadre de lote. �Desea usted resolver el descuadre?',msgdescuadre,msgaceptar,nil,'Confirmar','Si','No')

  else
  }
  close;



end;

procedure TFormRecPed.botoncalendarioMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin


layoutcalendario.Visible:=true;



end;

procedure TFormRecPed.botoncomentariosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
 Layoutcomentarios.Visible:=true;
end;

procedure TFormRecPed.BotonDetallesMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
  LAYAUTDESGLOSE.Visible:=TRUE;
labeliva1.Text:='Imp IVA '+configuracion.Items['iva1']+'%:';
labeliva1imp.Text:=totales.Items['impIVA1'];
labeliva2.Text:='Imp IVA '+configuracion.Items['iva2']+'%:';
labeliva2imp.Text:=totales.Items['impIVA2'];
labeliva3.Text:='Imp IVA '+configuracion.Items['iva3']+'%:';
labeliva3imp.Text:=totales.Items['impIVA3'];
labeltotalivaimp.Text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])));
labelrec1.Text:='Imp REC '+configuracion.Items['rec1']+'%:';
labelrec1imp.Text:=totales.Items['impREC1'];
labelrec2.Text:='Imp REC '+configuracion.Items['rec2']+'%:';
labelrec2imp.Text:=totales.Items['impREC2'];
labelrec3.Text:='Imp REC '+configuracion.Items['rec3']+'%:';
labelrec3imp.Text:=totales.Items['impREC3'];
labeltotalrecimp.Text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
labelimptotal.Text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])+strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
end;

procedure TFormRecPed.BotoneliminarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin


  confirmar(self,'�Desea usted cancelar el pedido?',msgeliminaryes,msgno,nil,'Confirmar','Si','No');






end;

procedure TFormRecPed.BotonLineaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 if ped2.ExisteLineasPed(idped) then
  begin
    //MostrarLineasPed(idped);
    //showmessage('aqui no entra');
    ped2.mostrarlineasped(idped,self);
    cerrar:=false;
    close;
  end
  else
      begin

          ped2.MostrarAddLineaPed(idped,self);
          cerrar:=false;
          close;
      end;








end;

procedure TFormRecPed.BotonHistoricoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 {
if ped2.comprobarlineasconlotes(idped) then
begin
  ped2.mostrarasignacionlotes(idped,self);
  cerrar:=false;
  close;
end
else
  alerta(self,'El pedido no contiene l�neas con lotes.');
  }
     ped2.mostrarhistoricoped(strtoint(editclt.text),self);

end;

procedure TFormRecPed.Button1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
 LAYAUTDESGLOSE.Visible:=FALSE;
end;

procedure TFormRecPed.Button2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin

  memocomentarios.text:=ped2.obtenercomentarios(idped);
  Layoutcomentarios.Visible:=FALSE;



end;

procedure TFormRecPed.Button3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin


  ped2.actualizarcomentarios(idped,memocomentarios.text);
  Layoutcomentarios.Visible:=FALSE;


end;

procedure TFormRecPed.Calendar1DateSelected(Sender: TObject);
begin
   editfechaprev.Text:= formatdatetime('DD/MM/YYYY',calendar1.Date);
  layoutcalendario.Visible:=false;
end;

procedure TFormRecPed.EditDesChange(Sender: TObject);
var descuento:string;
begin
if abortarchange then
    exit;
 descuento:=formatearnumero(configuracion.items['NumDecTot'],editdes.text);
 abortarchange:=true;
if(descuento='')then
begin

  editdes.text:=formatearnumero(configuracion.items['NumDecTot'],'0');

end
else
    begin
    editdes.text:=descuento;
    if(strtofloat(descuento)<>0) then
    editdespor.Text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');
    end;

ped2.modificardescuentos(idped,editdespor.Text,editdes.Text);
reiniciartotales();





abortarchange:=false;
end;

procedure TFormRecPed.EditDesPorChange(Sender: TObject);
var descuento:string;
begin
if abortarchange then
    exit;
 descuento:=formatearnumero(configuracion.items['NumDecPorDes'],editdespor.text);
 abortarchange:=true;
if(descuento='')then
begin

  editdespor.text:=formatearnumero(configuracion.items['NumDecPorDes'],'0');

end
else
  begin
   editdespor.text:=descuento;
   if(strtofloat(descuento)<>0) then
    editdes.Text:=formatearnumero(configuracion.items['NumDecTot'],'0');
  end;

ped2.modificardescuentos(idped,editdespor.Text,editdes.Text);
reiniciartotales();





abortarchange:=false;
end;

procedure TFormRecPed.EditfechaprevChange(Sender: TObject);
begin
 ped2.actualizarfechaprev(idped,fechaamericana(editfechaprev.text));
end;

procedure TFormRecPed.EditfechaprevMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin


  //ped2.actualizarfechaprev(idped,fechaamericana(editfechaprev.text));


end;

procedure TFormRecPed.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if cerrar then
  begin
 nueva:=true;
 padre.show;
  end;
  cerrar:=true;
 Action:= TCloseAction.cahide;



end;

procedure TFormRecPed.FormCreate(Sender: TObject);
begin
Ped2:=tpedido.Create;
 ped2.InicializarBotonera(panel2,3,10);
totales:=tdictionary<string,string>.create;
DatosPed:=tdictionary<string,string>.create;
abortarchange:=false;
//configuracion := ped2.obtenerConfiguracionPed();
idped:=-1;
OnVirtualKeyboardShown:=TTecladovirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladoVirtual.FormVirtualKeyboardHidden;
cerrar:=true;
nueva:=true;

end;

procedure TFormRecPed.FormKeyUp(Sender: TObject; var Key: Word;
  var KeyChar: Char; Shift: TShiftState);
var
  FService : IFMXVirtualKeyboardService;

begin
  if Key = vkHardwareBack then
  begin
    TPlatformServices.Current.SupportsPlatformService(IFMXVirtualKeyboardService, IInterface(FService));
    if (FService <> nil) and (TVirtualKeyboardState.vksVisible in FService.VirtualKeyBoardState) then
    begin
      // Back button pressed, keyboard visible, so do nothing...
    end else
    begin

        botonAtrasMouseUp(botonatras,TMouseButton.mbLeft,Shift, 0, 0);
        // They changed their mind, so ignore the Back button press...
        Key := 0;

    end;
  end;
end;

procedure TFormRecPed.FormShow(Sender: TObject);
begin
if nueva then
begin
  configuracion := ped2.obtenerConfiguracionPed();
  datosped:=ped2.obtenerdatosped(idped);
  editserie.text:=datosped.items['serie'];
  editnum.text:= datosped.items['numero'];
  editclt.Text:=datosped.items['cliente'];
  editfecha.text:=fechaeuropea(datosped.items['fecha']);
 // ShowMessage(datosped.items['fechaprev']);
  editfechaprev.Text:=fechaeuropea(datosped.items['fechaprev']);
  //ShowMessage(fechaeuropea(datosped.items['fechaprev']));
 // alerta(datosped.items['fechaprev'],self);
  Memocomentarios.text:=datosped.items['comentarios'];
  calendar1.Date:=strtodate(editfechaprev.Text);
  abortarchange:=true;
  editdes.Text:= formatearnumero(configuracion.items['NumDecTot'],datosped.items['c_descuento']);
  editdespor.Text:=formatearnumero(configuracion.items['NumDecPorDes'],datosped.items['p_descuento']);
  if(configuracion.items['PCambioDtoCabecera'] = 'N') THEN
  begin
     EditDesPor.Enabled := false;
     self.EditDes.Enabled := false;

  end
  else
  begin
      EditDesPor.Enabled := true;
     self.EditDes.Enabled := true;
  end;
  abortarchange:=false;
   reiniciartotales();
   if ped2.comprobarultimoped(idped) then
      botoneliminar.Visible:=true
   else
       botoneliminar.Visible:=false;


 nueva:=false;
end
else
  reiniciartotales();





end;

procedure tFormRecPed.reiniciartotales();
begin

    totales:=ped2.obtenertotales(idped);
    labelbruto.text:=formatearnumero(configuracion.items['NumDecTot'], totales.Items['bruto']);
    labelimp.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])+strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
    //labelimp.text:=formatearnumero(configuracion.items['NumDecTot'],floattostr(strtofloat(totales.Items['impIVA1'])+strtofloat(totales.Items['impIVA2'])+strtofloat(totales.Items['impIVA3'])+strtofloat(totales.Items['impREC1'])+strtofloat(totales.Items['impREC2'])+strtofloat(totales.Items['impREC3'])));
    labeltotal.text:=formatearnumero(configuracion.items['NumDecTot'],totales.Items['total']);
end;
  procedure TFormRecPed.msgyes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin


    close;


end;
  procedure TFormRecPed.Panel2Resize(Sender: TObject);
begin
    ped2.InicializarBotonera(panel2,3,10);
end;

procedure TFormRecPed.msgeliminaryes (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin

    ped2.eliminarped(idped);
    close;


end;
procedure TFormRecPed.msgno (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin

    exit;

end;
 procedure TFormRecPed.msgaceptar (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin


    close;


end;
procedure TFormRecPed.msgdescuadre (Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Single);
begin
  {
  ped2.mostrarasignacionlotes(idped,self);
  cerrar:=false;
  close;
  }

end;

end.
