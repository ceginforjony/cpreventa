unit vista_busquedalineaventa;

interface
       {$ZEROBASEDSTRINGS ON}
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls,carga_inicial, FMX.ListBox,Generics.Collections, FMX.Layouts,funciones,controlador,
  FMX.Controls.Presentation;

type
  TFormBusquedaLineaVenta = class(TForm)
    ToolBar1: TToolBar;
    Label1: TLabel;
    botoncancelar: TButton;
    ScrollBoxBusquedaArticulo: TScrollBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Label2: TLabel;
    ComboBoxbusqueda: TComboBox;
    Descripcion: TListBoxItem;
    Articulo: TListBoxItem;
    Cantidad: TListBoxItem;
    Precio: TListBoxItem;
    Importe: TListBoxItem;
    Bultos: TListBoxItem;
    Panel3: TPanel;
    Label3: TLabel;
    ComboBoxcadena: TComboBox;
    CONTENIDO: TListBoxItem;
    ESIGUAL: TListBoxItem;
    ComboBoxnumero: TComboBox;
    IGUAL: TListBoxItem;
    MENOR: TListBoxItem;
    MAYOR: TListBoxItem;
    DIFERENTE: TListBoxItem;
    Panel4: TPanel;
    Label4: TLabel;
    texto: TEdit;
    Panel5: TPanel;
    Checkaccion: TCheckBox;
    Panel6: TPanel;
    botonAceptar: TButton;
    Servidas: TListBoxItem;
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxbusquedaChange(Sender: TObject);
    procedure botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
   controlador:tcontrolador; { Private declarations }
  public
   busqueda: TDictionary<String,String>; { Public declarations }
  end;

var
  FormBusquedaLineaVenta: TFormBusquedaLineaVenta;

implementation

{$R *.fmx}
procedure TFormBusquedaLineaVenta.botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
 var
    accion: string;
    textoNum: double;
    Textoreplace: string;
begin


  if checkaccion.IsChecked then
    begin
      accion:='busqueda';
    end
  else
      accion:='filtro';


  //busqueda.Add('col',comboboxbusqueda.Selected.Text);
  //busqueda.Add('accion',accion);
  if comboboxbusqueda.Selected.Tag = 0 then
    begin
    if texto.Text<>'' then
    begin
      busqueda.Add('col',comboboxbusqueda.Selected.Text);
      busqueda.Add('accion',accion);
      busqueda.Add('tipo',comboboxcadena.Selected.Text);
      busqueda.Add('texto',texto.Text);
      busqueda.Add('fecha','N');

      modalresult:= mrOK;
      close;
    end
    else
      alerta(self,'El valor no puede estar vacio');

  end
  else
    begin
          textoreplace:= stringreplace(texto.text,'.',',',[rfReplaceAll]);

         if   TextToFloat(PChar(Textoreplace), textonum, fvExtended)then
         begin

                busqueda.Add('col',comboboxbusqueda.Selected.Text);
                busqueda.Add('accion',accion);
                busqueda.Add('tipo',comboboxnumero.Selected.Text);
                busqueda.Add('texto',texto.text);
                busqueda.Add('fecha','N');
                modalresult:= mrOK;
                close;
         end
         else
         begin
            alerta(self,'Valor incorrecto, debe introducir un formato num�rico para la opci�n introducida.');
            texto.text:='';
            //texto.setfocus;
         end;

    end;

end;

procedure TFormBusquedaLineaVenta.botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
     modalresult:= mrCancel;
     close;
end;

procedure TFormBusquedaLineaVenta.ComboBoxbusquedaChange(Sender: TObject);
var listbox: tlistboxitem;
    i:integer;
begin

//showmessage('hola');

 if comboboxbusqueda.Selected.Tag = 0 then
 begin
      comboboxnumero.Visible:=false;
      comboboxcadena.visible:=true;
      comboboxcadena.ItemIndex:=0;
      comboboxnumero.ItemIndex:=0;

     //comboboxtipo.se
     {
     for I := 0 to componentcount -1  do
     begin
        if uppercase(components[I].ClassName)= 'TLISTBOXITEM' then
          begin
            listbox:= tlistboxitem(components[I]);
              if listbox.Tag = 3 then
               begin
                  listbox.Selectable:= true;
               end
               else if listbox.Tag = 4 then
                  listbox.Selectable := false;


          end;

        end;
        comboboxtipo.ItemIndex:=0;
        }
 end
 else
 begin
      comboboxnumero.Visible:=true;
      comboboxcadena.visible:=false;
      comboboxcadena.ItemIndex:=0;
      comboboxnumero.ItemIndex:=0;

      {
     for I := 0 to componentcount -1  do
     begin
        if uppercase(components[I].ClassName)= 'TLISTBOXITEM' then
          begin
            listbox:= tlistboxitem(components[I]);
              if listbox.Tag = 3 then
               begin
                  listbox.Selectable:= false;
               end
               else if listbox.Tag = 4 then
                  listbox.Selectable := true;


          end;

        end;
       comboboxtipo.ItemIndex:=1;

     }
 end;



end;

procedure TFormBusquedaLineaVenta.FormClose(Sender: TObject; var Action: TCloseAction);
begin

action:=tcloseaction.cahide;
end;

procedure TFormBusquedaLineaVenta.FormCreate(Sender: TObject);

begin

busqueda:= tdictionary<string,string>.create;
OnVirtualKeyboardShown:=TTecladoVirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladovirtual.FormVirtualKeyboardHidden;
controlador:=tcontrolador.create;
controlador.ReescalarLetraLabel(self);
end;

procedure TFormBusquedaLineaVenta.FormShow(Sender: TObject);
begin

 if busqueda.Count <> 0 then
    busqueda.Clear;
comboboxbusqueda.ItemIndex:=0;
comboboxcadena.ItemIndex:=0;
comboboxnumero.ItemIndex:=0;
comboboxnumero.Visible:=false;
comboboxcadena.visible:=true;
texto.Text:='';
 checkaccion.IsChecked:=false;
end;

end.


