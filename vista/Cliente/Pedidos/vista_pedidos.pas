unit vista_pedidos;

interface
          {$ZEROBASEDSTRINGS ON}
uses
   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient, pedido,cliente,funciones,vista_busquedaped,
  FMX.Controls.Presentation, FMX.Grid.Style, FMX.ScrollBox;


type
  TFormPedidos = class(TForm)
    ToolBarCliente: TToolBar;
    LabelPed: TLabel;
    botonAtras: TButton;
    BotonHistorial: TButton;
    BotonNormal: TButton;
    PanelGrid: TPanel;
    gridpedido: TStringGrid;
    PanelBotones: TPanel;
    BotonVer: TButton;
    Icobus: TImage;
    Label1: TLabel;
    PanelBuscador: TPanel;
    Carga: TAniIndicator;
    DataSetProvider: TDataSetProvider;
    ClientDataSet: TClientDataSet;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PanelSaldo: TPanel;
    Rectangle1: TRectangle;
    LabelSaldo: TLabel;
    botonabajo: TButton;
    Image6: TImage;
    botonarriba: TButton;
    Image9: TImage;
    botonbusqueda: TButton;
    Image2: TImage;
    botonderecha: TButton;
    Image10: TImage;
    botonizquierda: TButton;
    Image11: TImage;
    botonnext: TButton;
    Image8: TImage;
    botonrefresh: TButton;
    Image7: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BotonHistorialMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonNormalMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonVerMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonCobroMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure gridpedidoHeaderClick(Column: TColumn);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PanelBotonesResize(Sender: TObject);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure gridpedidoResize(Sender: TObject);
    procedure gridpedidoDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
    procedure botonizquierdaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonderechaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);

  private

     Ped1: tpedido;
    queryPedido: TSQLQuery;
    busqueda: TDictionary<String,String>;
    ordenacion:Tdictionary<string,string>;
    clt:tcliente;
    Marca:string;
    Alprincipio:boolean;
    refresco:boolean;
    cerrar:boolean;
    idgrid:string;
    configuracion:TDictionary<String,variant>;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormPedidos: TFormPedidos;

implementation
 uses vista_menu;
{$R *.fmx}

procedure TFormPedidos.botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
clientdataset.Next;
end;

procedure TFormPedidos.botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
clientdataset.Prior;
end;

procedure TFormPedidos.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormPedidos.botonbusquedaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
  var orden:tdictionary<string,string>;
  begin
   formbusquedaped.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
          //showmessage('aqui entra uno');
          ped1.copiardiccionario(formbusquedaped.busqueda,busqueda);



          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not ped1.buscar(clientdataset,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                      botonnext.visible:=false;

                        orden:=tdictionary<string,string>.create;
                        orden.add('column','Fecha');
                        orden.add('tipo','desc');
                        ordenacion:=ped1.inicializarordenaciongrid(gridpedido);
                        ordenacion.items['Fecha']:='asc';

                        busqueda.Add('marca',marca);
                        Ped1.Listarpedidos(querypedido, busqueda,orden);

                        DataSetProvider.DataSet:=querypedido;
                        clientdataset.Close;
                       clientdataset.Open;
                       ped1.inicializarGrid(gridpedido,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
                       ped1.ajustatamaño(gridpedido,ClientDataSet);

                      //ClientDataset1.Refresh;

                      //showmessage(inttostr(queryarticulo.RecordCount));



              end;

      end;
    end);



end;

procedure TFormPedidos.botonCobroMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
   {
  if not ped1.pedidocobrado(clientdataset.FieldByName('id').Asinteger) then
  begin

  ped1.MostrarCobrarPedido(clientdataset.FieldByName('id').Asinteger,self,'N');
  idgrid:=clientdataset.FieldByName('id').AsString;
  refresco:=true;
  cerrar:=false;
  close;
  end
  else
  alerta(self,'No se puede cobrar pedidos ya cobrados o importes negativos');

     }
end;

procedure TFormPedidos.botonderechaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridpedido.ScrollBy(gridpedido.Columns[0].Width,0);
end;

procedure TFormPedidos.botondesplegarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin

 if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=275;
      end;



end;

procedure TFormPedidos.BotonHistorialMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;
begin
carga.Visible:=true;
botonhistorial.Visible:=false;
botonnormal.Visible:=true;
//botoncobro.Visible:=false;
marca:='H';
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');
if busqueda.Count <> 0 then
   busqueda.Clear;
busqueda.Add('marca',marca);
Ped1.Listarpedidos(querypedido, busqueda,orden);
DataSetProvider.DataSet:=querypedido;
  clientdataset.Close;
  application.ProcessMessages;
  application.ProcessMessages;
  clientdataset.Open;
  ped1.inicializarGrid(gridpedido,ClientDataSet);
  ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
  ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
  ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
  ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
  ped1.ajustatamaño(gridpedido,ClientDataSet);

    ordenacion:=ped1.inicializarordenaciongrid(gridpedido);
    ordenacion.items['Fecha']:='asc';
 carga.Visible:=false;




end;

procedure TFormPedidos.botonizquierdaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridpedido.ScrollBy( -1* gridpedido.Columns[0].Width,0);
end;

procedure TFormPedidos.botonnextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if(not ped1.buscar(clientdataset,busqueda,false)) then
       alerta(self,'error inexperado');
end;

procedure TFormPedidos.BotonNormalMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
 var orden:tdictionary<string,string>;
begin
carga.Visible:=true;
botonhistorial.Visible:=true;
botonnormal.Visible:=false;
//botoncobro.Visible:=true;
marca:='N';
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');
if busqueda.count <> 0 then
   busqueda.Clear;
busqueda.Add('marca',marca);
Ped1.Listarpedidos(querypedido, busqueda,orden);
DataSetProvider.DataSet:=querypedido;
  clientdataset.Close;
   application.ProcessMessages;
  application.ProcessMessages;
  clientdataset.Open;
  ped1.inicializarGrid(gridpedido,ClientDataSet);
  ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
  ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
  ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
  ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
  ped1.ajustatamaño(gridpedido,ClientDataSet);


     ordenacion:=ped1.inicializarordenaciongrid(gridpedido);
     ordenacion.items['Fecha']:='asc';


  carga.Visible:=false;






end;

procedure TFormPedidos.botonrefreshMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;
begin
   carga.visible:=true;
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');

  if busqueda.Count <> 0 then
   busqueda.Clear;
busqueda.Add('marca',marca);
Ped1.Listarpedidos(querypedido, busqueda,orden);
DataSetProvider.DataSet:=querypedido;

  ordenacion:=ped1.inicializarordenaciongrid(gridpedido);
  ordenacion.items['Fecha']:='asc';
    clientdataset.Close;
    application.ProcessMessages;
    application.ProcessMessages;
    clientdataset.Open;
    ped1.inicializarGrid(gridpedido,ClientDataSet);
    ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
    ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
    ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
    ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
    ped1.ajustatamaño(gridpedido,ClientDataSet);
    carga.visible:=false;








end;

procedure TFormPedidos.BotonVerMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
if clientdataset.RecordCount <> 0 then
begin
  if marca='N' then
    begin
      ped1.MostrarRectificarPedido(clientdataset.FieldByName('id').Asinteger,self);
      idgrid:=clientdataset.FieldByName('id').AsString;
      refresco:=true;
    end
  else
    begin
      ped1.MostrarVerPedido(clientdataset.FieldByName('id').Asinteger,self);

    end;



cerrar:=false;
close;
end
else
  alerta(self,'Imposible realizar la operación');
end;

procedure TFormPedidos.Button1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
   {
  if clientdataset.RecordCount <> 0 then
begin

      ped1.imprimir(clientdataset.FieldByName('id').Asinteger,self);


end
else
  alerta(self,'Imposible realizar la operación');
    }




end;

procedure TFormPedidos.FormActivate(Sender: TObject);
begin

   //application.ProcessMessages;


end;

procedure TFormPedidos.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if cerrar then
  begin
  clientdataset.close;
  alprincipio:=false;
  refresco:=false;
  marca:='N';
  botonhistorial.Visible:=true;
  botonnormal.Visible:=false;
  //botoncobro.Visible:=true;

  form2.Activate;
  end;
  cerrar:=true;
  Action:= TCloseAction.cahide;


end;

procedure TFormPedidos.FormCreate(Sender: TObject);
begin
cerrar:=true;
ped1:=tpedido.create;
ped1.InicializarBotonera(panelbotones,3,10);
clt:=tcliente.create;
busqueda:=tdictionary<string,string>.create;
marca:='N';
Alprincipio:=false;
refresco:=false;

//querypedido:=TSQLQuery.Create(application);
//busqueda:=tdictionary<string,string>.create;


end;

procedure TFormPedidos.FormShow(Sender: TObject);
var orden:tdictionary<string,string>;
begin

    carga.visible:=true;
    configuracion:=ped1.obtenerConfiguracion();
    labelsaldo.text:='Total: '+formatearnumero(configuracion.Items['NumDecTot'],ped1.ObtenerTotalPedidos());
    if clientdataset.active=true then
    begin
      if refresco or alprincipio  then
      begin

              orden:=tdictionary<string,string>.create;
              orden.add('column','Fecha');
              orden.add('tipo','desc');
              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;
              Busqueda.Add('marca',marca);
              Ped1.Listarpedidos(querypedido, busqueda,orden);
              DataSetProvider.DataSet:=querypedido;
              ordenacion:=ped1.inicializarordenaciongrid(gridpedido);
              ordenacion.items['Fecha']:='asc';


                 //ClientDataSet.refresh;
                 if alprincipio then
                 begin
                        clientdataset.Close;
                       application.ProcessMessages;

                       application.ProcessMessages;
                       clientdataset.Open;
                       ped1.inicializarGrid(gridpedido,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
                       ped1.ajustatamaño(gridpedido,ClientDataSet);
                      alprincipio:=false;
                 end
                 else if refresco then
                      begin
                       clientdataset.Close;
                       application.ProcessMessages;

                       application.ProcessMessages;
                       clientdataset.Open;
                       ped1.inicializarGrid(gridpedido,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
                       ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
                       ped1.ajustatamaño(gridpedido,ClientDataSet);
                        while not clientdataset.eof do
                        begin
                          if clientdataset.FieldByName('id').AsString = idgrid then
                          begin
                          break;
                          end
                          else
                          clientdataset.Next;

                        end;
                        if clientdataset.Eof then
                           clientdataset.First;
                      refresco:=false;
                        {codigo para refresco}
                      end;
                end;

    end
    else
      begin

        application.ProcessMessages;

        application.ProcessMessages;
              orden:=tdictionary<string,string>.create;
              orden.add('column','Fecha');
              orden.add('tipo','desc');
              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;
              Busqueda.Add('marca',marca);
              Ped1.Listarpedidos(querypedido, busqueda,orden);
              DataSetProvider.DataSet:=querypedido;
        ClientDataSet.active:=true;
        ped1.inicializarGrid(gridpedido,ClientDataSet);

        ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
        ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
        ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
        ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
        ped1.ajustatamaño(gridpedido,ClientDataSet);
        ordenacion:=ped1.inicializarordenaciongrid(gridpedido);
        ordenacion.items['Fecha']:='asc';

      end;

    carga.visible:=false;

end;

procedure TFormPedidos.gridpedidoDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridpedido.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

procedure TFormPedidos.gridpedidoHeaderClick(Column: TColumn);
var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=ped1.inicializarordenaciongrid(gridpedido);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin



Ped1.Listarpedidos(querypedido, busqueda,orden);
DataSetProvider.DataSet:=querypedido;
   clientdataset.Close;
   application.ProcessMessages;
  application.ProcessMessages;
   clientdataset.Open;
   ped1.inicializarGrid(gridpedido,ClientDataSet);
   ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);
   ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
   ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
   ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
   ped1.ajustatamaño(gridpedido,ClientDataSet);

  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;
              Busqueda.Add('marca',marca);
              Ped1.Listarpedidos(querypedido, busqueda,orden);
              DataSetProvider.DataSet:=querypedido;

              clientdataset.Close;
              application.ProcessMessages;
              application.ProcessMessages;
              clientdataset.Open;
              ped1.inicializarGrid(gridpedido,ClientDataSet);
              ped1.sizecolgrid(gridpedido,0,0,ClientDataSet);

              ped1.sizecolgrid(gridpedido,1,50,ClientDataSet);
              ped1.sizecolgrid(gridpedido,3,90,ClientDataSet);
              ped1.sizecolgrid(gridpedido,5,200,ClientDataSet);
              ped1.ajustatamaño(gridpedido,ClientDataSet);
              botonnext.Visible:=false;



end;




   carga.visible:=false;










end;

procedure TFormPedidos.gridpedidoResize(Sender: TObject);
begin
ped1.ajustatamaño(gridpedido,ClientDataSet);
end;

procedure TFormPedidos.PanelBotonesResize(Sender: TObject);
begin
   ped1.InicializarBotonera(panelbotones,3,10);
end;

end.

