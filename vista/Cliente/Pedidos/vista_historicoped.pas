unit vista_historicoped;

interface
           {$ZEROBASEDSTRINGS ON}
uses
    System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient, articulo,funciones,vista_buscararticulo,cliente,
  FMX.Controls.Presentation, FMX.Grid.Style, FMX.ScrollBox;


type
  TFormhistoricoped = class(TForm)
    ToolBarCliente: TToolBar;
    botonAtras: TButton;
    Labelhistorico: TLabel;
    PanelGrid: TPanel;
    gridarticulo: TStringGrid;
    PanelBuscador: TPanel;
    Carga: TAniIndicator;
    PanelCliente: TPanel;
    Rectangle1: TRectangle;
    Labelcliente: TLabel;
    ClientDataSet1: TClientDataSet;
    DataSetProvider1: TDataSetProvider;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    botonabajo: TButton;
    Image6: TImage;
    botonarriba: TButton;
    Image9: TImage;
    botonbusqueda: TButton;
    Image1: TImage;
    botonderecha: TButton;
    Image10: TImage;
    botonizquierda: TButton;
    Image11: TImage;
    botonnext: TButton;
    Image8: TImage;
    botonrefresh: TButton;
    Image7: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure gridarticuloDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
    procedure gridarticuloCellDblClick(const Column: TColumn;
      const Row: Integer);
    procedure botonizquierdaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonderechaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
  articulo: tarticulos;
  clt:tcliente;
  queryarticulo:TSQLQuery;
  serie:tdictionary<string,string>;
  busqueda: TDictionary<String,String>;
  ordenacion:Tdictionary<string,string>;
  cerrar:boolean;

    { Private declarations }
  public

    padre: tform;
    client:integer;
    { Public declarations }
  end;

var
  Formhistoricoped: TFormhistoricoped;

implementation

{$R *.fmx}

procedure TFormhistoricoped.botonabajoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
ClientDataSet1.Next;
end;

procedure TFormhistoricoped.botonarribaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
ClientDataSet1.Prior;
end;

procedure TFormhistoricoped.botonAtrasMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
CLOSE;
end;

procedure TFormhistoricoped.botonbusquedaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;

begin
       //showmessage('entro en busqueda');


    formbusquedaart.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
          articulo.copiardiccionario(formbusquedaart.busqueda,busqueda);


          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not articulo.buscar(clientdataset1,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                      botonnext.visible:=false;
                             botonnext.visible:=false;
                    orden:=tdictionary<string,string>.create;
                    orden.add('column','Nombre');
                    orden.add('tipo','asc');

                    articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,client);

                    DataSetProvider1.DataSet:=queryArticulo;
                    clientdataset1.Close;
                    ClientDataSet1.open;

                    articulo.inicializarGrid(gridarticulo,ClientDataSet1);
                    //Application.processmessages;
                    articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
                    articulo.mostrartarifas(gridarticulo,ClientDataSet1);
                    ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);
                    ordenacion.items['Nombre']:='desc';



                      //showmessage(inttostr(queryarticulo.RecordCount));



              end;

      end;

    end);


end;

procedure TFormhistoricoped.botonderechaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridarticulo.ScrollBy(  gridarticulo.Columns[0].Width,0);
end;

procedure TFormhistoricoped.botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=290;
      end;

end;

procedure TFormhistoricoped.botonizquierdaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridarticulo.ScrollBy( -1* gridarticulo.Columns[0].Width,0);
end;

procedure TFormhistoricoped.botonnextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
 if(not articulo.buscar(clientdataset1,busqueda,false)) then
       alerta(self,'error inexperado');
end;

procedure TFormhistoricoped.botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
 var orden:tdictionary<string,string>;
  begin
     carga.visible:=true;
   orden:=tdictionary<string,string>.create;
    orden.add('column','Nombre');
    orden.add('tipo','asc');



  if(busqueda.count <> 0) then
     busqueda.Clear;


    articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,client);

  DataSetProvider1.DataSet:=queryArticulo;
  clientdataset1.Close;
  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;

  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
  //Application.processmessages;
  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
  articulo.mostrartarifas(gridarticulo,ClientDataSet1);
  ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);
  ordenacion.items['Nombre']:='desc';
  carga.Visible:=false;


    //articulo.inicializarGrid(gridarticulo);
    //articulo.sizecolgrid(gridarticulo,1,200);


//application.ProcessMessages;


  botonnext.visible:=false;





end;

procedure TFormhistoricoped.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  clientdataset1.close;
  padre.show;
  Action:= TCloseAction.cahide;




end;

procedure TFormhistoricoped.FormCreate(Sender: TObject);
begin
   articulo:=tarticulos.create;

  busqueda:= TDictionary<String,String>.create;

end;

procedure TFormhistoricoped.FormShow(Sender: TObject);
 var orden:tdictionary<string,string>;
begin
    labelcliente.text:=clt.ConsultarCliente(inttostr(client)).Items['cliente'];
    carga.Visible:=true;
      orden:=tdictionary<string,string>.create;
      orden.add('column','Nombre');
      orden.add('tipo','asc');

    if busqueda.count <> 0 then
        busqueda.clear;

  articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,client);

  DataSetProvider1.DataSet:=queryArticulo;
  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;

  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
  //Application.processmessages;
  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
  articulo.mostrartarifas(gridarticulo,ClientDataSet1);
  ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);
  ordenacion.items['Nombre']:='desc';
  carga.Visible:=false;




end;

procedure TFormhistoricoped.gridarticuloCellDblClick(const Column: TColumn;
  const Row: Integer);
  var orden:tdictionary<string,string>;
    tipo:string;
begin
        carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=articulo.inicializarordenaciongrid(gridarticulo);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin
       articulo.ListarArticulosHisPed(queryarticulo, busqueda,orden,client);

  DataSetProvider1.DataSet:=queryArticulo;
  clientdataset1.Close;
  Application.processmessages;
  Application.processmessages;
  ClientDataSet1.active:=true;

  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
  //Application.processmessages;
  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
   articulo.mostrartarifas(gridarticulo,ClientDataSet1);



  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

                  articulo.ListarArticuloshisPed(queryarticulo, busqueda,orden,client);


                  DataSetProvider1.DataSet:=queryArticulo;
                  clientdataset1.Close;
                  Application.processmessages;
                  Application.processmessages;
                  ClientDataSet1.active:=true;

                  articulo.inicializarGrid(gridarticulo,ClientDataSet1);
                  //Application.processmessages;
                  articulo.sizecolgrid(gridarticulo,1,200,ClientDataSet1);
                  articulo.mostrartarifas(gridarticulo,ClientDataSet1);
                  botonnext.Visible:=false;



end;




   carga.visible:=false;

end;

procedure TFormhistoricoped.gridarticuloDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridarticulo.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

end.
