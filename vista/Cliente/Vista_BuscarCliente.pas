unit Vista_BuscarCliente;

interface
               {$ZEROBASEDSTRINGS ON}
uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.StdCtrls,carga_inicial, FMX.ListBox,Generics.Collections, FMX.Layouts,funciones,controlador,
  FMX.Controls.Presentation;
type
  TFormbusquedaclt = class(TForm)
    ToolBar1: TToolBar;
    Label1: TLabel;
    botoncancelar: TButton;
    ScrollBoxBusquedaArticulo: TScrollBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Label2: TLabel;
    ComboBoxbusqueda: TComboBox;
    Cliente: TListBoxItem;
    Poblacion: TListBoxItem;
    Codigo: TListBoxItem;
    Direccion: TListBoxItem;
    Telefono: TListBoxItem;
    NComercial: TListBoxItem;
    Panel3: TPanel;
    Label3: TLabel;
    ComboBoxcadena: TComboBox;
    ESIGUAL: TListBoxItem;
    CONTENIDO: TListBoxItem;
    ComboBoxnumero: TComboBox;
    IGUAL: TListBoxItem;
    MENOR: TListBoxItem;
    MAYOR: TListBoxItem;
    DIFERENTE: TListBoxItem;
    Panel4: TPanel;
    Label4: TLabel;
    texto: TEdit;
    Panel5: TPanel;
    Checkaccion: TCheckBox;
    Panel6: TPanel;
    botonAceptar: TButton;
    CodPostal: TListBoxItem;
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxbusquedaChange(Sender: TObject);
    procedure botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormShow(Sender: TObject);
  private

  controlador:tcontrolador;  { Private declarations }
  public
  var
      busqueda: TDictionary<String,String>;
    { Public declarations }
  end;
  var formbusquedaclt:tFormbusquedaclt;

implementation

{$R *.fmx}

procedure TFormbusquedaclt.botonAceptarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
   var
    accion: string;
    textoNum: double;
    Textoreplace: string;
  begin


  if checkaccion.IsChecked then
    begin
      accion:='busqueda';
    end
  else
      accion:='filtro';
  if comboboxbusqueda.Selected.Tag = 0 then
  begin
    if texto.Text<>'' then
    begin
      busqueda.Add('col',comboboxbusqueda.Selected.Text);
      busqueda.Add('accion',accion);
      busqueda.Add('tipo',comboboxcadena.Selected.Text);
      busqueda.Add('texto',texto.Text);
      busqueda.Add('fecha','N');
      modalresult:= mrOK;
      close;
    end
    else
      alerta(self,'El valor no puede estar vacio');

  end
  else
    begin
          textoreplace:= stringreplace(texto.text,'.',',',[rfReplaceAll]);

         if   TextToFloat(PChar(Textoreplace), textonum, fvExtended)then
         begin

                busqueda.Add('col',comboboxbusqueda.Selected.Text);
                busqueda.Add('accion',accion);
                busqueda.Add('tipo',comboboxnumero.Selected.Text);
                busqueda.Add('texto',texto.text);
                busqueda.Add('fecha','N');
                modalresult:= mrOK;
                close;
         end
         else
         begin
            alerta(self,'Texto incorrecto, debe introducir un formato num�rico para la opci�n introducida.');
            texto.text:='';

         end;

    end;
end;


procedure TFormbusquedaclt.botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
     modalresult:= mrCancel;
     close;
end;

procedure TFormbusquedaclt.ComboBoxbusquedaChange(Sender: TObject);
begin
   if comboboxbusqueda.Selected.Tag = 0 then
 begin
      comboboxnumero.Visible:=false;
      comboboxcadena.visible:=true;
      comboboxcadena.ItemIndex:=0;
      comboboxnumero.ItemIndex:=0;


 end
 else
 begin
      comboboxnumero.Visible:=true;
      comboboxcadena.visible:=false;
      comboboxcadena.ItemIndex:=0;
      comboboxnumero.ItemIndex:=0;


 end;

end;

procedure TFormbusquedaclt.FormCreate(Sender: TObject);
begin


  busqueda:= tdictionary<string,string>.create;
  OnVirtualKeyboardShown:=TTecladoVirtual.FormVirtualKeyboardShown;
OnVirtualKeyboardHidden:=TTecladovirtual.FormVirtualKeyboardHidden;
 controlador:=tcontrolador.create;
 controlador.ReescalarLetraLabel(self);

end;

procedure TFormbusquedaclt.FormShow(Sender: TObject);
begin
 if busqueda.Count <> 0 then
    busqueda.Clear;
comboboxbusqueda.ItemIndex:=0;
comboboxcadena.ItemIndex:=0;
comboboxnumero.ItemIndex:=0;
comboboxnumero.Visible:=false;
comboboxcadena.visible:=true;
texto.Text:='';
checkaccion.IsChecked:=false;
end;

end.
