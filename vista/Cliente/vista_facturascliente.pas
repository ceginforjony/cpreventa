unit vista_facturasCliente;

interface
          {$ZEROBASEDSTRINGS ON}
uses
   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient, factura,cliente,funciones,vista_busquedafacclt,
  FMX.Controls.Presentation,system.IOUtils, FMX.Grid.Style,
  FMX.ScrollBox;


type
  TFormfacturasclt = class(TForm)
    ToolBarCliente: TToolBar;
    LabelFac: TLabel;
    botonAtras: TButton;
    PanelGrid: TPanel;
    gridfactura: TStringGrid;
    PanelBotones: TPanel;
    Carga: TAniIndicator;
    ClientDataSet: TClientDataSet;
    DataSetProvider: TDataSetProvider;
    BindSourceDB: TBindSourceDB;
    BindingsList: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    botonCobro: TButton;
    Icocobro: TImage;
    Labelbotoncobro: TLabel;
    PanelInfoClt: TPanel;
    Rectangle1: TRectangle;
    LabelClt: TLabel;
    PanelBuscador: TPanel;
    botonabajo: TButton;
    Image6: TImage;
    botonarriba: TButton;
    Image9: TImage;
    botonbusqueda: TButton;
    Image1: TImage;
    botonderecha: TButton;
    Image10: TImage;
    botonizquierda: TButton;
    Image11: TImage;
    botonnext: TButton;
    Image8: TImage;
    botonrefresh: TButton;
    Image7: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonCobroMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PanelBotonesResize(Sender: TObject);
    procedure gridfacturaResize(Sender: TObject);
    procedure gridfacturaCellDblClick(const Column: TColumn;
      const Row: Integer);
    procedure botonizquierdaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonderechaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
    Fac1: tfactura;
    queryFactura: TSQLQuery;
    busqueda: TDictionary<String,String>;
    ordenacion:Tdictionary<string,string>;
    datosclt: TDictionary<String,String>;
    clt:tcliente;
    Alprincipio:boolean;
    refresco:boolean;
    cerrar:boolean;
    idgrid:string;

    { Private declarations }
  public
    { Public declarations }
    codigoClt:string;
  end;
  var formfacturasclt:tFormfacturasclt;



implementation

{$R *.fmx}

procedure TFormfacturasclt.botonabajoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
clientdataset.Next;
end;

procedure TFormfacturasclt.botonarribaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
clientdataset.Prior;
end;

procedure TFormfacturasclt.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormfacturasclt.botonbusquedaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
  var orden:tdictionary<string,string>;
  begin
   formbusquedafacclt.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin

          fac1.copiardiccionario(formbusquedafacclt.busqueda,busqueda);



          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not fac1.buscar(clientdataset,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                      botonnext.visible:=false;

                        orden:=tdictionary<string,string>.create;
                        orden.add('column','Fecha');
                        orden.add('tipo','desc');
                        ordenacion:=fac1.inicializarordenaciongrid(gridfactura);
                        ordenacion.items['Fecha']:='asc';


                        Fac1.Listarfacturascliente(codigoclt,queryfactura, busqueda,orden);

                        DataSetProvider.DataSet:=queryfactura;
                        clientdataset.Close;
                       clientdataset.Open;
                       fac1.inicializarGrid(gridfactura,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
                        fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
                        fac1.ajustatamaño(gridfactura,ClientDataSet);




              end;

      end;
    end);



end;

procedure TFormfacturasclt.botonCobroMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
      If not fileExists(TPath.getPublicPath+'/NGC.txt') then
    begin
            if not fac1.facturacobrado(clientdataset.FieldByName('id').Asinteger) then
            begin

                fac1.MostrarCobrarFactura(clientdataset.FieldByName('id').Asinteger,self);
                idgrid:=clientdataset.FieldByName('id').AsString;
                refresco:=true;
                cerrar:=false;
                close;
            end
            else
                alerta(self,'No se puede cobrar facturas ya cobrados o importes negativos');
    end
      else
        alerta(self,'Imposible Realizar Operación, No dispone de permisos');



end;

procedure TFormfacturasclt.botonderechaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridfactura.ScrollBy( gridfactura.Columns[0].Width,0);
end;

procedure TFormfacturasclt.botondesplegarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin

 if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=300;
      end;



end;

procedure TFormfacturasclt.botonizquierdaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
    gridfactura.ScrollBy( -1* gridfactura.Columns[0].Width,0);
end;

procedure TFormfacturasclt.botonnextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin

   if(not fac1.buscar(clientdataset,busqueda,false)) then
       alerta(self,'error inexperado');


end;

procedure TFormfacturasclt.botonrefreshMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;
begin
   carga.visible:=true;
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');

  if busqueda.Count <> 0 then
   busqueda.Clear;

Fac1.Listarfacturascliente(codigoclt,queryfactura, busqueda,orden);
DataSetProvider.DataSet:=queryfactura;

  ordenacion:=fac1.inicializarordenaciongrid(gridfactura);
  ordenacion.items['Fecha']:='asc';
    clientdataset.Close;
    application.ProcessMessages;
    application.ProcessMessages;
    clientdataset.Open;
    fac1.inicializarGrid(gridfactura,ClientDataSet);
    fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
    fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
    fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
    fac1.ajustatamaño(gridfactura,ClientDataSet);
    carga.visible:=false;








end;

procedure TFormfacturasclt.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if cerrar then
  begin
  clientdataset.close;
  alprincipio:=false;
  refresco:=false;


  clt.mostrarListaClientes();
  end;
  cerrar:=true;
  Action:= TCloseAction.cahide;


end;

procedure TFormfacturasclt.FormCreate(Sender: TObject);
begin
cerrar:=true;
fac1:=tfactura.create;
clt:=tcliente.create;
busqueda:=tdictionary<string,string>.create;
ordenacion:=tdictionary<string,string>.create;
 clt.InicializarBotonera(panelbotones,3,10);

Alprincipio:=false;
refresco:=false;



end;

procedure TFormfacturasclt.FormShow(Sender: TObject);
var orden:tdictionary<string,string>;
begin

    carga.visible:=true;
    if clientdataset.active=true then
    begin
      if refresco or alprincipio  then
      begin

              orden:=tdictionary<string,string>.create;
              orden.add('column','Fecha');
              orden.add('tipo','desc');
              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

              Fac1.Listarfacturascliente(codigoclt,queryfactura, busqueda,orden);
              DataSetProvider.DataSet:=queryfactura;
              ordenacion:=fac1.inicializarordenaciongrid(gridfactura);
              ordenacion.items['Fecha']:='asc';


                 if alprincipio then
                 begin
                        clientdataset.Close;
                       application.ProcessMessages;

                       application.ProcessMessages;
                       clientdataset.Open;
                       fac1.inicializarGrid(gridfactura,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
                      fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
                      fac1.ajustatamaño(gridfactura,ClientDataSet);
                      alprincipio:=false;
                 end
                 else if refresco then
                      begin
                       clientdataset.Close;
                       application.ProcessMessages;

                       application.ProcessMessages;
                       clientdataset.Open;
                       fac1.inicializarGrid(gridfactura,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
                       fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
                       fac1.ajustatamaño(gridfactura,ClientDataSet);
                        while not clientdataset.eof do
                        begin
                          if clientdataset.FieldByName('id').AsString = idgrid then
                          begin
                          break;
                          end
                          else
                          clientdataset.Next;

                        end;
                        if clientdataset.Eof then
                           clientdataset.First;

                      refresco:=false;
                        {codigo para refresco}
                      end;
                end;

    end
    else
      begin

        datosclt:=clt.ConsultarCliente(codigoclt);
        labelfac.text:=' Facturas, Clt: '+codigoclt;
        labelclt.Text:=datosclt.items['cliente'];
        application.ProcessMessages;

        application.ProcessMessages;
              orden:=tdictionary<string,string>.create;
              orden.add('column','Fecha');
              orden.add('tipo','desc');
              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

              Fac1.Listarfacturascliente(codigoclt,queryfactura, busqueda,orden);
              DataSetProvider.DataSet:=queryfactura;
        ClientDataSet.active:=true;
        fac1.inicializarGrid(gridfactura,ClientDataSet);

        fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
        fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
        fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
        fac1.ajustatamaño(gridfactura,ClientDataSet);
        ordenacion:=fac1.inicializarordenaciongrid(gridfactura);
        ordenacion.items['Fecha']:='asc';

      end;

    carga.visible:=false;

end;

procedure TFormfacturasclt.gridfacturaCellDblClick(const Column: TColumn;
  const Row: Integer);
  var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=fac1.inicializarordenaciongrid(gridfactura);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin



Fac1.Listarfacturascliente(codigoclt,queryfactura, busqueda,orden);
DataSetProvider.DataSet:=queryfactura;
   clientdataset.Close;
   application.ProcessMessages;
  application.ProcessMessages;
   clientdataset.Open;
   fac1.inicializarGrid(gridfactura,ClientDataSet);
   fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
   fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
   fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
   fac1.ajustatamaño(gridfactura,ClientDataSet);

  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;

              Fac1.Listarfacturascliente(codigoclt,queryfactura, busqueda,orden);
              DataSetProvider.DataSet:=queryfactura;

              clientdataset.Close;
              application.ProcessMessages;
              application.ProcessMessages;
              clientdataset.Open;
              fac1.inicializarGrid(gridfactura,ClientDataSet);
              fac1.sizecolgrid(gridfactura,0,0,ClientDataSet);
              fac1.sizecolgrid(gridfactura,1,50,ClientDataSet);
              fac1.sizecolgrid(gridfactura,3,90,ClientDataSet);
              fac1.ajustatamaño(gridfactura,ClientDataSet);
              botonnext.Visible:=false;



end;




   carga.visible:=false;
end;

procedure TFormfacturasclt.gridfacturaResize(Sender: TObject);
begin
fac1.ajustatamaño(gridfactura,ClientDataSet);
end;

procedure TFormfacturasclt.PanelBotonesResize(Sender: TObject);
begin
 clt.InicializarBotonera(panelbotones,3,10);
end;

end.
