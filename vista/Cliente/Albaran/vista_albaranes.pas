unit vista_albaranes;

interface
          {$ZEROBASEDSTRINGS ON}
uses
   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient, albaran,cliente,funciones,vista_busquedaalb, FMX.Controls.Presentation,system.IOUtils,
  FMX.Grid.Style, FMX.ScrollBox;


type
  TFormAlbaranes = class(TForm)
    ToolBarCliente: TToolBar;
    LabelAlb: TLabel;
    botonAtras: TButton;
    PanelGrid: TPanel;
    gridalbaran: TStringGrid;
    PanelBotones: TPanel;
    botonCobro: TButton;
    Icocobro: TImage;
    Labelbotoncobro: TLabel;
    Carga: TAniIndicator;
    DataSetProvider: TDataSetProvider;
    ClientDataSet: TClientDataSet;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    PanelBuscador: TPanel;
    botonbusqueda: TButton;
    Image1: TImage;
    botonabajo: TButton;
    Image6: TImage;
    botonrefresh: TButton;
    Image7: TImage;
    botonnext: TButton;
    Image8: TImage;
    botonarriba: TButton;
    Image9: TImage;
    botonderecha: TButton;
    Image10: TImage;
    botonizquierda: TButton;
    Image11: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BotonHistorialMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonNormalMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonrefreshMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botondesplegarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure BotonVerMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonCobroMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonnextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonbusquedaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure PanelBotonesResize(Sender: TObject);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure gridalbaranResize(Sender: TObject);
    procedure gridalbaranDrawColumnCell(Sender: TObject; const Canvas: TCanvas;
      const Column: TColumn; const [Ref] Bounds: TRectF; const Row: Integer;
      const [Ref] Value: TValue; const State: TGridDrawStates);
    procedure gridalbaranCellDblClick(const Column: TColumn;
      const Row: Integer);
    procedure botonizquierdaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonderechaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonarribaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonabajoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);

  private

     Alb1: talbaran;
    queryAlbaran: TSQLQuery;
    busqueda: TDictionary<String,String>;
    ordenacion:Tdictionary<string,string>;
    clt:tcliente;
    //Marca:string;
    Alprincipio:boolean;
    refresco:boolean;
    cerrar:boolean;
    idgrid:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAlbaranes: TFormAlbaranes;

implementation
 uses vista_menu;
{$R *.fmx}

procedure TFormAlbaranes.botonabajoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
              ClientDataSet.Next;
end;

procedure TFormAlbaranes.botonarribaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
ClientDataSet.Prior;
end;

procedure TFormalbaranes.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormAlbaranes.botonbusquedaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
  var orden:tdictionary<string,string>;
  begin
   formbusquedaalb.ShowModal(procedure(ModalResult: TModalResult)
    begin
      if ModalResult = mrOK then
      begin
          //showmessage('aqui entra uno');
          alb1.copiardiccionario(formbusquedaalb.busqueda,busqueda);



          if  busqueda.Items['accion']='busqueda'then
              begin
                  if(not alb1.buscar(clientdataset,busqueda,true)) then
                  begin
                      alerta(self,'No se encontraton resultados para la busqueda seleccionada');
                      botonnext.Visible:=false;
                  end
                  else
                      botonnext.Visible:=true;

              end
          else
              begin
                      botonnext.visible:=false;

                        orden:=tdictionary<string,string>.create;
                        orden.add('column','Fecha');
                        orden.add('tipo','desc');
                        ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);
                        ordenacion.items['Fecha']:='asc';

                        //busqueda.Add('marca',marca);
                        Alb1.Listaralbaranes(queryalbaran, busqueda,orden);

                        DataSetProvider.DataSet:=queryalbaran;
                        clientdataset.Close;
                       clientdataset.Open;
                       alb1.inicializarGrid(gridalbaran,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
                       alb1.ajustatamaño(gridalbaran,ClientDataSet);

                      //ClientDataset1.Refresh;

                      //showmessage(inttostr(queryarticulo.RecordCount));



              end;

      end;
    end);



end;

procedure TFormalbaranes.botonCobroMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin


  If not fileExists(TPath.getPublicPath +'/NGC.txt') then
    begin
            if not alb1.albarancobrado(clientdataset.FieldByName('id').Asinteger) then
            begin

            alb1.MostrarCobrarAlbaran(clientdataset.FieldByName('id').Asinteger,self);
            idgrid:=clientdataset.FieldByName('id').AsString;
            refresco:=true;
            cerrar:=false;
            close;
            end
            else
            alerta(self,'No se puede cobrar albaranes ya cobrados o importes negativos');
    end
      else
        alerta(self,'Imposible Realizar Operación, No dispone de permisos');



end;

procedure TFormAlbaranes.botonderechaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
     gridalbaran.ScrollBy( gridalbaran.Columns[0].Width,0);
end;

procedure TFormalbaranes.botondesplegarMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin

 if  panelgrid.Align = talignlayout.altop then
      begin
      panelgrid.Align := talignlayout.alClient;
      end
      else
      begin
      panelgrid.Align := talignlayout.altop;
      panelgrid.Height:=275;
      end;



end;

procedure TFormalbaranes.BotonHistorialMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;
begin
{
carga.Visible:=true;
botonhistorial.Visible:=false;
botonnormal.Visible:=true;
botoncobro.Visible:=false;
marca:='H';
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');
if busqueda.Count <> 0 then
   busqueda.Clear;
busqueda.Add('marca',marca);
Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
DataSetProvider.DataSet:=queryalbaran;
  clientdataset.Close;
  application.ProcessMessages;
  application.ProcessMessages;
  clientdataset.Open;
  alb1.inicializarGrid(gridalbaran);
  alb1.sizecolgrid(gridalbaran,0,0);
  alb1.sizecolgrid(gridalbaran,1,50);
  alb1.sizecolgrid(gridalbaran,3,90);
  alb1.sizecolgrid(gridalbaran,5,200);

    ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);
    ordenacion.items['Fecha']:='asc';
 carga.Visible:=false;



 }
end;

procedure TFormAlbaranes.botonizquierdaMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
gridalbaran.ScrollBy( -1* gridalbaran.Columns[0].Width,0);
end;

procedure TFormAlbaranes.botonnextMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  if(not alb1.buscar(clientdataset,busqueda,false)) then
       alerta(self,'error inexperado');
end;

procedure TFormalbaranes.BotonNormalMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
 var orden:tdictionary<string,string>;
begin
{
carga.Visible:=true;
botonhistorial.Visible:=true;
botonnormal.Visible:=false;
botoncobro.Visible:=true;
marca:='N';
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');
if busqueda.count <> 0 then
   busqueda.Clear;
busqueda.Add('marca',marca);
Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
DataSetProvider.DataSet:=queryalbaran;
  clientdataset.Close;
   application.ProcessMessages;
  application.ProcessMessages;
  clientdataset.Open;
  alb1.inicializarGrid(gridalbaran);
  alb1.sizecolgrid(gridalbaran,0,0);
  alb1.sizecolgrid(gridalbaran,1,50);
  alb1.sizecolgrid(gridalbaran,3,90);
  alb1.sizecolgrid(gridalbaran,5,200);


     ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);
     ordenacion.items['Fecha']:='asc';


  carga.Visible:=false;




 }

end;

procedure TFormalbaranes.botonrefreshMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
var orden:tdictionary<string,string>;
begin
   carga.visible:=true;
   orden:=tdictionary<string,string>.create;
    orden.add('column','Fecha');
    orden.add('tipo','desc');

  if busqueda.Count <> 0 then
   busqueda.Clear;
//busqueda.Add('marca',marca);
Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
DataSetProvider.DataSet:=queryalbaran;

  ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);
  ordenacion.items['Fecha']:='asc';
    clientdataset.Close;
    application.ProcessMessages;
    application.ProcessMessages;
    clientdataset.Open;
    alb1.inicializarGrid(gridalbaran,ClientDataSet);
    alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);
    alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
    alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
    alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
    alb1.ajustatamaño(gridalbaran,ClientDataSet);
    carga.visible:=false;








end;

procedure TFormalbaranes.BotonVerMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Single);
begin
{
if clientdataset.RecordCount <> 0 then
begin
  if marca='N' then
    begin
      alb1.MostrarRectificarAlbaran(clientdataset.FieldByName('id').Asinteger,self);
      idgrid:=clientdataset.FieldByName('id').AsString;
      refresco:=true;
    end
  else
    begin
      alb1.MostrarVerAlbaran(clientdataset.FieldByName('id').Asinteger,self);

    end;



cerrar:=false;
close;
end
else
  alerta(self,'Imposible realizar la operación');
}
end;

procedure TFormAlbaranes.Button1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
{
  if clientdataset.RecordCount <> 0 then
begin

      alb1.imprimir(clientdataset.FieldByName('id').Asinteger,self);


end
else
  alerta(self,'Imposible realizar la operación');

 }



end;

procedure TFormalbaranes.FormActivate(Sender: TObject);
begin

   //application.ProcessMessages;


end;

procedure TFormalbaranes.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if cerrar then
  begin
  clientdataset.close;
  alprincipio:=false;
  refresco:=false;
  //marca:='N';
  //botonhistorial.Visible:=true;
  //botonnormal.Visible:=false;
  //botoncobro.Visible:=true;

  form2.Activate;
  end;
  cerrar:=true;
  Action:= TCloseAction.cahide;


end;

procedure TFormalbaranes.FormCreate(Sender: TObject);
begin
cerrar:=true;
alb1:=talbaran.create;
alb1.InicializarBotonera(panelbotones,3,10);
clt:=tcliente.create;
busqueda:=tdictionary<string,string>.create;
//marca:='N';
Alprincipio:=false;
refresco:=false;

//queryalbaran:=TSQLQuery.Create(application);
//busqueda:=tdictionary<string,string>.create;


end;

procedure TFormalbaranes.FormShow(Sender: TObject);
var orden:tdictionary<string,string>;
begin

    carga.visible:=true;
    if clientdataset.active=true then
    begin
      if refresco or alprincipio  then
      begin

              orden:=tdictionary<string,string>.create;
              orden.add('column','Fecha');
              orden.add('tipo','desc');
              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;
              //Busqueda.Add('marca',marca);
              Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
              DataSetProvider.DataSet:=queryalbaran;
              ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);
              ordenacion.items['Fecha']:='asc';


                 //ClientDataSet.refresh;
                 if alprincipio then
                 begin
                        clientdataset.Close;
                       application.ProcessMessages;

                       application.ProcessMessages;
                       clientdataset.Open;
                       alb1.inicializarGrid(gridalbaran,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
                       alb1.ajustatamaño(gridalbaran,ClientDataSet);
                      alprincipio:=false;
                 end
                 else if refresco then
                      begin
                       clientdataset.Close;
                       application.ProcessMessages;

                       application.ProcessMessages;
                       clientdataset.Open;
                       alb1.inicializarGrid(gridalbaran,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
                       alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
                       alb1.ajustatamaño(gridalbaran,ClientDataSet);
                        while not clientdataset.eof do
                        begin
                          if clientdataset.FieldByName('id').AsString = idgrid then
                          begin
                          break;
                          end
                          else
                          clientdataset.Next;

                        end;
                        if clientdataset.Eof then
                           clientdataset.First;
                      refresco:=false;
                        {codigo para refresco}
                      end;
                end;

    end
    else
      begin

        application.ProcessMessages;

        application.ProcessMessages;
              orden:=tdictionary<string,string>.create;
              orden.add('column','Fecha');
              orden.add('tipo','desc');
              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;
              //Busqueda.Add('marca',marca);
              Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
              DataSetProvider.DataSet:=queryalbaran;
        ClientDataSet.active:=true;
        alb1.inicializarGrid(gridalbaran,ClientDataSet);

        alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);
        alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
        alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
        alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
        alb1.ajustatamaño(gridalbaran,ClientDataSet);
        ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);
        ordenacion.items['Fecha']:='asc';

      end;

    carga.visible:=false;

end;

procedure TFormAlbaranes.gridalbaranCellDblClick(const Column: TColumn;
  const Row: Integer);
  var orden:tdictionary<string,string>;
    tipo:string;
begin
carga.visible:=true;
orden:=tdictionary<string,string>.create;
tipo:=ordenacion.items[column.header];
orden.add('column',column.header);
orden.add('tipo',tipo);
ordenacion:=alb1.inicializarordenaciongrid(gridalbaran);

if tipo='asc' then
   ordenacion.items[column.header]:='desc'
else
   ordenacion.items[column.header]:='asc';

if not botonnext.visible then
begin



Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
DataSetProvider.DataSet:=queryalbaran;
   clientdataset.Close;
   application.ProcessMessages;
  application.ProcessMessages;
   clientdataset.Open;
   alb1.inicializarGrid(gridalbaran,ClientDataSet);
   alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);
   alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
   alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
   alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
   alb1.ajustatamaño(gridalbaran,ClientDataSet);

  end
else
begin

              if busqueda.Count <> 0 then
              begin
                  busqueda.Clear;
              end;
              //Busqueda.Add('marca',marca);
              Alb1.Listaralbaranes(queryalbaran, busqueda,orden);
              DataSetProvider.DataSet:=queryalbaran;

              clientdataset.Close;
              application.ProcessMessages;
              application.ProcessMessages;
              clientdataset.Open;
              alb1.inicializarGrid(gridalbaran,ClientDataSet);
              alb1.sizecolgrid(gridalbaran,0,0,ClientDataSet);

              alb1.sizecolgrid(gridalbaran,1,50,ClientDataSet);
              alb1.sizecolgrid(gridalbaran,3,90,ClientDataSet);
              alb1.sizecolgrid(gridalbaran,5,200,ClientDataSet);
              alb1.ajustatamaño(gridalbaran,ClientDataSet);
              botonnext.Visible:=false;



end;




   carga.visible:=false;

end;

procedure TFormAlbaranes.gridalbaranDrawColumnCell(Sender: TObject;
  const Canvas: TCanvas; const Column: TColumn; const [Ref] Bounds: TRectF;
  const Row: Integer; const [Ref] Value: TValue; const State: TGridDrawStates);
    var
  T, T2: TRectF;
begin
  if Row = gridalbaran.Selected then
  begin
    with Canvas do
    begin
        Fill.Kind := TBrushKind.Solid;
        Fill.Color := talphacolors.Deepskyblue;
    end;

    T := Bounds;
    if TStringGrid(Sender).ColumnCount - 1  = Column.Index then
      T.Right := Self.Width;
    Canvas.FillRect(T, 0, 0, [], 0.5);
  end;

end;

procedure TFormAlbaranes.gridalbaranResize(Sender: TObject);
begin
  alb1.ajustatamaño(gridalbaran,ClientDataSet);
end;

procedure TFormAlbaranes.PanelBotonesResize(Sender: TObject);
begin
   alb1.InicializarBotonera(panelbotones,3,10);
end;

end.

