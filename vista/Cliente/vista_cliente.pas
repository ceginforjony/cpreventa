unit vista_cliente;

interface
              {$ZEROBASEDSTRINGS ON}
uses
 System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,Generics.Collections,
  FMX.Edit,carga_inicial, FMX.Objects, FMX.StdCtrls, FMX.Layouts,cliente,funciones,
  FMX.Controls.Presentation;

type
  TFormcliente = class(TForm)
    ToolBar1: TToolBar;
    botoncancelar: TButton;
    LabelTituloClt: TLabel;
    ScrollBox1: TScrollBox;
    Panel1: TPanel;
    Panel4: TPanel;
    labelCliente: TLabel;
    editcliente: TEdit;
    Panel3: TPanel;
    LabelTlf: TLabel;
    EditTlf: TEdit;
    Panel5: TPanel;
    LabelCodPostal: TLabel;
    editCodPostal: TEdit;
    Panel6: TPanel;
    Labelpoblacion: TLabel;
    EditPoblacion: TEdit;
    Panel7: TPanel;
    Labelprovincia: TLabel;
    Editprovincia: TEdit;
    Panel8: TPanel;
    Labeldireccion: TLabel;
    Editdireccion: TEdit;
    Panel9: TPanel;
    Labelncomer: TLabel;
    Editncomer: TEdit;
    Panel2: TPanel;
    Label1: TLabel;
    Editpenfac: TEdit;
    Panel10: TPanel;
    Label2: TLabel;
    Editpenalb: TEdit;
    Panel11: TPanel;
    Label3: TLabel;
    Editcontac: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
  private
  cliente: tcliente;
  DatosCliente: tdictionary<string,string>;

      { Private declarations }
  public
  codigoClt:string
    { Public declarations }
  end;
  var
  formcliente:tFormcliente;


implementation

{$R *.fmx}

procedure TFormcliente.botoncancelarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormcliente.FormClose(Sender: TObject; var Action: TCloseAction);
begin
cliente.MostrarListaCLientes;
action:=tcloseaction.caHide;
end;

procedure TFormcliente.FormCreate(Sender: TObject);
begin
    cliente:=tcliente.create;
end;

procedure TFormcliente.FormShow(Sender: TObject);
begin

    datoscliente := cliente.consultarCliente(codigoClt);
    labeltituloclt.text:= 'Cliente: '+codigoClt;
    editcliente.text:= datoscliente.items['cliente'];
    editncomer.text:= datoscliente.items['ncomercial'];
    editdireccion.text:= datoscliente.items['direccion'];
    editprovincia.text:= datoscliente.items['provincia'];
    editpoblacion.text:= datoscliente.items['poblacion'];
    editcodpostal.text:= datoscliente.items['codpostal'];
    edittlf.text:= datoscliente.items['telefono'];
    editcontac.text:= datoscliente.items['contacto'];
    //editpenalb.text:= datoscliente.items['pen_alb'];
    //editpenfac.text:= datoscliente.items['pen_fac'];
    editpenalb.text:= formatearnumero(2,datoscliente.items['pen_alb']);
    editpenfac.text:= formatearnumero(2,datoscliente.items['pen_fac']);

end;

end.
