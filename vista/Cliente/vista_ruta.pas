unit vista_ruta;

interface
         {$ZEROBASEDSTRINGS ON}
uses
   System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, carga_inicial,
  System.Rtti, FMX.StdCtrls, FMX.Layouts, FMX.Grid,Data.DbxSqlite, Data.DB, Data.SqlExpr,
  Data.Bind.EngExt, Fmx.Bind.DBEngExt, Fmx.Bind.Grid, System.Bindings.Outputs,
  Fmx.Bind.Editors, Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,Generics.Collections,
  Data.FMTBcd, System.Actions, FMX.ActnList, FMX.Gestures, FMX.Objects,
  Datasnap.Provider, Datasnap.DBClient,funciones,FMX.ListBox,cliente,comunicacion,
  FMX.Controls.Presentation;


type
  TFormruta = class(TForm)
    ToolBarArticulo: TToolBar;
    LabelArticulos: TLabel;
    botonAtras: TButton;
    Panel1: TPanel;
    Panel3: TPanel;
    Labelruta: TLabel;
    Panel2: TPanel;
    botonAsignar: TButton;
    IcoAceptar: TImage;
    LabelAceptar: TLabel;
    BotonReinicio: TButton;
    IcoLotes: TImage;
    LabelLotes: TLabel;
    ComboBoxRuta: TComboBox;
    LabelProceso: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BotonReinicioMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAsignarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel2Resize(Sender: TObject);
  private

   cliente:tcliente;
   queryrutas:TSQLQuery;
   nomruta:tdictionary<string,string>;
    { Private declarations }
  public

    { Public declarations }
  end;

var
  Formruta: TFormruta;

implementation
  uses vista_menu;
{$R *.fmx}

procedure TFormruta.botonAsignarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
  var comunicacion:tcomunicacion;
begin

  ruta:=nomruta.items[comboboxruta.Selected.Text];
  comunicacion:=tcomunicacion.create();
  if ruta<>'-1' then
    begin

      form2.labelrutaarchivo.Text:='Ruta: '+comboboxruta.Selected.Text;
      form2.labelrutaventa.Text:='Ruta: '+comboboxruta.Selected.Text;
      form2.labelrutautilidades.Text:='Ruta: '+comboboxruta.Selected.Text;
    end
    else
    begin
      form2.labelrutaarchivo.Text:='';
      form2.labelrutaventa.Text:='';
      form2.labelrutautilidades.Text:='';

    end;

  labelproceso.text:='Reiniciando tablas de clientes';
  application.ProcessMessages;
  comunicacion.reinicia_tablas_clientes();
  labelproceso.text:='';
  application.ProcessMessages;




  alerta(self,'La ruta ha sido establecida');

end;

procedure TFormruta.botonAtrasMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
close;
end;

procedure TFormruta.BotonReinicioMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Single);
begin
  comboboxruta.ItemIndex:=0;
  ruta:='-1';
  form2.labelrutaarchivo.Text:='';
  form2.labelrutaventa.Text:='';
  form2.labelrutautilidades.Text:='';
  alerta(self,'La ruta ha sido reiniciada');





end;

procedure TFormruta.FormClose(Sender: TObject; var Action: TCloseAction);
begin
form2.activate;
action:=tcloseaction.caHide;
end;

procedure TFormruta.FormCreate(Sender: TObject);
begin
application.ProcessMessages;
form1.Close;
cliente:=tcliente.create;
cliente.InicializarBotonera(panel2,2,20);
nomruta:=tdictionary<string,string>.create;
//form2.Activate;
end;

procedure TFormruta.FormShow(Sender: TObject);
var i,j:integer;
begin
i:=1;
//freeandnil(queryrutas);
queryrutas:=cliente.obtenerrutas();
if nomruta.count<>0 then
   nomruta.clear;
if comboboxruta.count<>0 then
   comboboxruta.clear;

comboboxruta.items.add('NINGUNA');
comboboxruta.ItemIndex:=0;
nomruta.add('NINGUNA','-1');
j:=0;
while not queryrutas.eof  do
begin

if nomruta.ContainsKey(queryrutas.fieldbyname('nombre').asstring)  then
begin
 comboboxruta.items.add(queryrutas.fieldbyname('nombre').asstring+inttostr(j+1));
 nomruta.add(queryrutas.fieldbyname('nombre').asstring+inttostr(j+1),queryrutas.fieldbyname('codigo').AsString);
 if ruta=queryrutas.fieldbyname('codigo').asstring then
    comboboxruta.ItemIndex:=i;
 j:=j+1;
end
else
begin
 comboboxruta.items.add(queryrutas.fieldbyname('nombre').asstring);
 nomruta.add(queryrutas.fieldbyname('nombre').asstring,queryrutas.fieldbyname('codigo').AsString);
 if ruta=queryrutas.fieldbyname('codigo').asstring then
    comboboxruta.ItemIndex:=i;
end;

 i:=i+1;
 queryrutas.next;
end;

if  comboboxruta.ItemIndex=0 then
    ruta:='-1';










end;

procedure TFormruta.Panel2Resize(Sender: TObject);
begin
cliente.InicializarBotonera(panel2,2,20);
end;

end.
